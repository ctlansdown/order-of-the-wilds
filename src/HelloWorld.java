
/**

 * 
 */
import static org.lwjgl.demo.vulkan.VKUtil.translateVulkanResult;
import static org.lwjgl.glfw.Callbacks.glfwFreeCallbacks;
import static org.lwjgl.glfw.GLFW.GLFW_FALSE;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_ESCAPE;
import static org.lwjgl.glfw.GLFW.GLFW_RELEASE;
import static org.lwjgl.glfw.GLFW.GLFW_RESIZABLE;
import static org.lwjgl.glfw.GLFW.GLFW_TRUE;
import static org.lwjgl.glfw.GLFW.GLFW_VISIBLE;
import static org.lwjgl.glfw.GLFW.glfwCreateWindow;
import static org.lwjgl.glfw.GLFW.glfwDefaultWindowHints;
import static org.lwjgl.glfw.GLFW.glfwDestroyWindow;
import static org.lwjgl.glfw.GLFW.glfwInit;
import static org.lwjgl.glfw.GLFW.glfwMakeContextCurrent;
import static org.lwjgl.glfw.GLFW.glfwPollEvents;
import static org.lwjgl.glfw.GLFW.glfwSetErrorCallback;
import static org.lwjgl.glfw.GLFW.glfwSetKeyCallback;
import static org.lwjgl.glfw.GLFW.glfwSetWindowShouldClose;
import static org.lwjgl.glfw.GLFW.glfwShowWindow;
import static org.lwjgl.glfw.GLFW.glfwSwapBuffers;
import static org.lwjgl.glfw.GLFW.glfwSwapInterval;
import static org.lwjgl.glfw.GLFW.glfwTerminate;
import static org.lwjgl.glfw.GLFW.glfwWindowHint;
import static org.lwjgl.glfw.GLFW.glfwWindowShouldClose;
import static org.lwjgl.glfw.GLFWVulkan.glfwVulkanSupported;
import static org.lwjgl.opengl.GL11.GL_COLOR_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.GL_DEPTH_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.glClear;
import static org.lwjgl.opengl.GL11.glClearColor;
import static org.lwjgl.system.MemoryUtil.*;
import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.glfw.GLFWVulkan.*;
import static org.lwjgl.vulkan.EXTDebugReport.*;
import static org.lwjgl.vulkan.KHRSwapchain.*;
import static org.lwjgl.vulkan.KHRSurface.*;
import static org.lwjgl.vulkan.VK10.*;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.LongBuffer;
import java.nio.ShortBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import org.joml.Matrix4f;
import org.joml.Vector2d;
import org.joml.Vector3d;
import org.joml.Vector3f;
import org.lwjgl.PointerBuffer;
import org.lwjgl.Version;
import org.lwjgl.glfw.GLFWCursorPosCallback;
import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.opengl.GL;
import org.lwjgl.system.MemoryUtil;
import org.lwjgl.vulkan.VkApplicationInfo;
import org.lwjgl.vulkan.VkAttachmentDescription;
import org.lwjgl.vulkan.VkAttachmentReference;
import org.lwjgl.vulkan.VkBufferCopy;
import org.lwjgl.vulkan.VkBufferCreateInfo;
import org.lwjgl.vulkan.VkClearColorValue;
import org.lwjgl.vulkan.VkClearValue;
import org.lwjgl.vulkan.VkCommandBuffer;
import org.lwjgl.vulkan.VkCommandBufferAllocateInfo;
import org.lwjgl.vulkan.VkCommandBufferBeginInfo;
import org.lwjgl.vulkan.VkCommandPoolCreateInfo;
import org.lwjgl.vulkan.VkDebugReportCallbackCreateInfoEXT;
import org.lwjgl.vulkan.VkDebugReportCallbackEXT;
import org.lwjgl.vulkan.VkDescriptorBufferInfo;
import org.lwjgl.vulkan.VkDescriptorPoolCreateInfo;
import org.lwjgl.vulkan.VkDescriptorPoolSize;
import org.lwjgl.vulkan.VkDescriptorSetAllocateInfo;
import org.lwjgl.vulkan.VkDescriptorSetLayoutBinding;
import org.lwjgl.vulkan.VkDescriptorSetLayoutCreateInfo;
import org.lwjgl.vulkan.VkDevice;
import org.lwjgl.vulkan.VkDeviceCreateInfo;
import org.lwjgl.vulkan.VkDeviceQueueCreateInfo;
import org.lwjgl.vulkan.VkExtent2D;
import org.lwjgl.vulkan.VkFormatProperties;
import org.lwjgl.vulkan.VkFramebufferCreateInfo;
import org.lwjgl.vulkan.VkGraphicsPipelineCreateInfo;
import org.lwjgl.vulkan.VkImageCreateInfo;
import org.lwjgl.vulkan.VkImageMemoryBarrier;
import org.lwjgl.vulkan.VkImageSubresourceRange;
import org.lwjgl.vulkan.VkImageViewCreateInfo;
import org.lwjgl.vulkan.VkInstance;
import org.lwjgl.vulkan.VkInstanceCreateInfo;
import org.lwjgl.vulkan.VkMemoryAllocateInfo;
import org.lwjgl.vulkan.VkMemoryRequirements;
import org.lwjgl.vulkan.VkOffset2D;
import org.lwjgl.vulkan.VkPhysicalDevice;
import org.lwjgl.vulkan.VkPhysicalDeviceFeatures;
import org.lwjgl.vulkan.VkPhysicalDeviceMemoryProperties;
import org.lwjgl.vulkan.VkPhysicalDeviceProperties;
import org.lwjgl.vulkan.VkPipelineColorBlendAttachmentState;
import org.lwjgl.vulkan.VkPipelineColorBlendStateCreateInfo;
import org.lwjgl.vulkan.VkPipelineDepthStencilStateCreateInfo;
import org.lwjgl.vulkan.VkPipelineInputAssemblyStateCreateInfo;
import org.lwjgl.vulkan.VkPipelineLayoutCreateInfo;
import org.lwjgl.vulkan.VkPipelineMultisampleStateCreateInfo;
import org.lwjgl.vulkan.VkPipelineRasterizationStateCreateInfo;
import org.lwjgl.vulkan.VkPipelineShaderStageCreateInfo;
import org.lwjgl.vulkan.VkPipelineVertexInputStateCreateInfo;
import org.lwjgl.vulkan.VkPipelineViewportStateCreateInfo;
import org.lwjgl.vulkan.VkPresentInfoKHR;
import org.lwjgl.vulkan.VkQueue;
import org.lwjgl.vulkan.VkQueueFamilyProperties;
import org.lwjgl.vulkan.VkRect2D;
import org.lwjgl.vulkan.VkRenderPassBeginInfo;
import org.lwjgl.vulkan.VkRenderPassCreateInfo;
import org.lwjgl.vulkan.VkSemaphoreCreateInfo;
import org.lwjgl.vulkan.VkShaderModuleCreateInfo;
import org.lwjgl.vulkan.VkSubmitInfo;
import org.lwjgl.vulkan.VkSubpassDependency;
import org.lwjgl.vulkan.VkSubpassDescription;
import org.lwjgl.vulkan.VkSurfaceCapabilitiesKHR;
import org.lwjgl.vulkan.VkSurfaceFormatKHR;
import org.lwjgl.vulkan.VkSwapchainCreateInfoKHR;
import org.lwjgl.vulkan.VkVertexInputAttributeDescription;
import org.lwjgl.vulkan.VkVertexInputBindingDescription;
import org.lwjgl.vulkan.VkVertexInputBindingDescription.Buffer;
import org.lwjgl.vulkan.VkViewport;
import org.lwjgl.vulkan.VkWriteDescriptorSet;

/**
 *
 * @author Christopher Lansdown
 */
@SuppressWarnings("unused")
public class HelloWorld {
	private static ByteBuffer[] layers = { memUTF8("VK_LAYER_LUNARG_standard_validation"), };
	private static final boolean validation = Boolean.parseBoolean(System.getProperty("vulkan.validation", "false"));

	// The window handle
	private long window;
	private VkPhysicalDevice physicalDevice;
	private VkInstance instance;
	private VkDevice device;
	private int graphicsQueueFamilyIndex;
	private VkQueue graphicsQueue;
	/** the surface for drawing to the window */
	private long surface;
	private VkPhysicalDeviceMemoryProperties memoryProperties;
	private int swapchainImageFormat;
	private int swapchainColorSpace;
	private int presentationMode;
	private long swapChain = VK_NULL_HANDLE;
	private LongBuffer swapChainImages = null;
	private long[] swapChainImageViews;
	private int swapchainWidth;
	private int swapchainHeight;
	private long pipelineLayout;
	private long renderPass;
	private long graphicsPipeline;
	private long[] framebuffers;
	private long graphicsCommandPool;
	private VkCommandBuffer[] graphicsCommandBuffers;
	private VkExtent2D swapchainExtent;
	private long renderFinishedSemaphor;
	private long imageAvailableSemaphor;
	private Vertices terrainVertices;
	private long descriptorSetLayout;
	private UniformBuffer uniformBuffer;
	/**
	 * The horizontal camera angle in radians
	 */
	private volatile double horizontalCameraAngle = 0;
	/**
	 * The vertical camera angle in radians
	 */
	private volatile double verticalCameraAngle = -3.1415926535 / 4;

	private volatile double cameraX = 0, cameraY = 00, cameraZ = 200;
	private long descriptorPool;
	private long descriptorSet;
	private float[][] terrainHeightmaps;
	private ImageInfo depthBufferImage;

	public void run() {
		System.out.println("Hello LWJGL " + Version.getVersion() + "!");

		try {
			init();
			try {
				Thread.sleep(100);
			} catch(Exception | Error ex) {
				ex.printStackTrace();
			}
			loop();

			// Free the window callbacks and destroy the window
			glfwFreeCallbacks(window);
			glfwDestroyWindow(window);
		} finally {
			// Terminate GLFW and free the error callback
			glfwTerminate();
			glfwSetErrorCallback(null).free();
		}
	}

	private void drawFrame() {
		// System.out.println("drawFrame()");
		int image_index;
		try (TemporaryIntBuffer buf = new TemporaryIntBuffer()) {
			int ret = vkAcquireNextImageKHR(device, swapChain, -1L, imageAvailableSemaphor, VK_NULL_HANDLE, buf.buf);
			errorIfNecessary(ret, "Unable to aquire next swap chain image: ");
			image_index = buf.value();
		}

		/** Update the camera position */
		uniformBuffer.update();

		// System.out.println(" image index is " + image_index);
		VkSubmitInfo submitInfo = VkSubmitInfo.calloc();
		VkPresentInfoKHR presentInfo = VkPresentInfoKHR.calloc();
		try (TemporaryLongBuffer img_sem_buf = new TemporaryLongBuffer(imageAvailableSemaphor);
		     TemporaryPointerBuffer cmd_buf = new TemporaryPointerBuffer(graphicsCommandBuffers[image_index]);
		     TemporaryIntBuffer wait_dst_mask = new TemporaryIntBuffer(VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT);
		     TemporaryLongBuffer render_sem_buf = new TemporaryLongBuffer(renderFinishedSemaphor);
		     TemporaryLongBuffer swp_chn = new TemporaryLongBuffer(swapChain);
		     TemporaryIntBuffer image_index_buf = new TemporaryIntBuffer(image_index)) {

			submitInfo.sType(VK_STRUCTURE_TYPE_SUBMIT_INFO);
			submitInfo.pWaitSemaphores(img_sem_buf.buf);
			submitInfo.pWaitDstStageMask(wait_dst_mask.buf);
			submitInfo.pCommandBuffers(cmd_buf.buf);
			submitInfo.pSignalSemaphores(render_sem_buf.buf);
			int ret = vkQueueSubmit(graphicsQueue, submitInfo, VK_NULL_HANDLE);
			errorIfNecessary(ret, "Unable to submit commands");

			presentInfo.sType(VK_STRUCTURE_TYPE_PRESENT_INFO_KHR);
			presentInfo.pWaitSemaphores(render_sem_buf.buf);
			presentInfo.swapchainCount(1);
			presentInfo.pSwapchains(swp_chn.buf);
			presentInfo.pImageIndices(image_index_buf.buf);

			ret = vkQueuePresentKHR(graphicsQueue, presentInfo);
			errorIfNecessary(ret, "Unable to present frame: ");

			vkQueueWaitIdle(graphicsQueue);
			// System.out.println(" drawFrame finished");
		} finally {
			submitInfo.free();
			presentInfo.free();
		}
	}

	private void init() {
		// Setup an error callback. The default implementation
		// will print the error message in System.err.

		// Initialize GLFW. Most GLFW functions will not work before doing this.
		if(!glfwInit())
			throw new IllegalStateException("Unable to initialize GLFW");

		if(!glfwVulkanSupported())
			throw new IllegalStateException("Vulkan is not supported!");

		/* Get the required instance extensions */
		PointerBuffer requiredExtensions = glfwGetRequiredInstanceExtensions();
		if(requiredExtensions == null)
			throw new AssertionError("Unable to get required Vulkan extensions");

		System.out.println("About to create the vulkan instance:");

		GLFWErrorCallback.createPrint(System.err).set();
		instance = createVulkanInstance(requiredExtensions);
		final VkDebugReportCallbackEXT debugCallback = new VkDebugReportCallbackEXT() {
			@Override
			public int invoke(int flags, int objectType, long object, long location, int messageCode, long pLayerPrefix, long pMessage, long pUserData) {
				System.err.println("ERROR OCCURED: " + VkDebugReportCallbackEXT.getString(pMessage));
				return 0;
			}
		};

		System.out.println("About to create the window:");
		createWindow(debugCallback);

		final GLFWCursorPosCallback cursorCallback = new GLFWCursorPosCallback() {
			boolean noFirstPosition = true;
			double lastXPos = 0, lastYPos = 0;

			@Override
			public void invoke(long window, double xpos, double ypos) {
				if(noFirstPosition) {
					noFirstPosition = false;
					lastXPos = xpos;
					lastYPos = ypos;
				}
				double delta_x = xpos - lastXPos;
				double delta_y = ypos - lastYPos;

				horizontalCameraAngle = (horizontalCameraAngle + delta_x * (3.1415926535 / 3600));
				while(horizontalCameraAngle > 2 * 3.1415926535)
					horizontalCameraAngle -= 2 * 3.1415926535;
				while(horizontalCameraAngle < -2 * 3.1415926535)
					horizontalCameraAngle += 2 * 3.1415926535;
				verticalCameraAngle = (verticalCameraAngle - delta_y * (3.1415926535 / 3600));
				while(verticalCameraAngle > 2 * 3.1415926535)
					verticalCameraAngle -= 2 * 3.1415926535;
				while(verticalCameraAngle < -2 * 3.1415926535)
					verticalCameraAngle += 2 * 3.1415926535;
				lastXPos = xpos;
				lastYPos = ypos;
			}
		};
		glfwSetCursorPosCallback(window, cursorCallback);
		glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

		System.out.println("===========================");
		System.out.println("creating the physical device");
		physicalDevice = getPhysicalDevice(surface);
		memoryProperties = VkPhysicalDeviceMemoryProperties.calloc();
		vkGetPhysicalDeviceMemoryProperties(physicalDevice, memoryProperties);
		device = createLogicalDevice();
		createGraphicsCommandPool();
		createDeviceQueue();
		createSwapChain();
		createImageViews();
		createDepthResources();
		createTerrain();
		createUniformBuffer();
		createDescriptorPool();
		createDescriptorSetLayout();
		createDescriptorSet();
		createGraphicsPipelines();
		createSwapchainFrameBuffers();
		createGraphicsCommandBuffers();
		recordGraphicsCommandBuffers();
		createSemaphores();

		System.out.println("===========================");
		System.out.println("showing the window");
		// Make the window visible
		glfwShowWindow(window);

		System.out.println("init() finished init");
	}

	/**
	 *
	 */
	private void createDepthResources() {
		int[] formats = { VK_FORMAT_D32_SFLOAT, VK_FORMAT_D32_SFLOAT_S8_UINT, VK_FORMAT_D24_UNORM_S8_UINT };
		int format = findSupportedFormat(formats, VK_IMAGE_TILING_OPTIMAL, VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT);
		depthBufferImage = createImage(swapchainWidth, swapchainHeight, format, VK_IMAGE_TILING_OPTIMAL, VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT,
		                               VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);
		createImageView(depthBufferImage, VK_IMAGE_ASPECT_DEPTH_BIT);
		transitionImageLayout(depthBufferImage, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL);
	}

	/**
	 *
	 */
	private void createDescriptorSet() {
		VkDescriptorSetAllocateInfo allocInfo = VkDescriptorSetAllocateInfo.calloc();
		VkDescriptorBufferInfo.Buffer bufferInfo = VkDescriptorBufferInfo.calloc(1);
		VkWriteDescriptorSet.Buffer descriptorWrite = VkWriteDescriptorSet.calloc(1);
		try (TemporaryLongBuffer dsl_buf = new TemporaryLongBuffer(descriptorSetLayout); TemporaryLongBuffer buf = new TemporaryLongBuffer()) {
			allocInfo.sType(VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO);
			allocInfo.descriptorPool(descriptorPool);
			allocInfo.pSetLayouts(dsl_buf.buf);

			int ret = vkAllocateDescriptorSets(device, allocInfo, buf.buf);
			errorIfNecessary(ret, "Unable to allocate the buffer set: ");
			descriptorSet = buf.value();

			bufferInfo.buffer(uniformBuffer.bufferInfo.buffer);
			bufferInfo.offset(0);
			bufferInfo.range(uniformBuffer.size());

			descriptorWrite.sType(VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET);
			descriptorWrite.dstSet(descriptorSet);
			descriptorWrite.dstBinding(0);
			descriptorWrite.dstArrayElement(0);
			descriptorWrite.descriptorType(VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER);
			descriptorWrite.pBufferInfo(bufferInfo);
			descriptorWrite.pImageInfo(null);
			descriptorWrite.pTexelBufferView(null);

			vkUpdateDescriptorSets(device, descriptorWrite, null);
		} finally {
			descriptorWrite.free();
			bufferInfo.free();
			allocInfo.free();
		}
	}

	/**
	 *
	 */
	private void createUniformBuffer() {
		uniformBuffer = new UniformBuffer();
		uniformBuffer.init();
	}

	private class UniformBuffer {
		public BufferInfo bufferInfo;
		public ByteBuffer buffer;

		public UniformBuffer() {
			buffer = memAlloc(size());
			bufferInfo = createBuffer(size(), VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);

		}

		public void init() {
			Matrix4f model = new Matrix4f();
			Matrix4f view = new Matrix4f();
			Matrix4f projection = new Matrix4f();
			projection.scale(1f, -1f, 1f).perspective((float) Math.toRadians(45), (float) swapchainWidth / swapchainHeight, 1.0f, Float.POSITIVE_INFINITY, true);

			System.out.println("The projection matrix is:\n" + projection);
			
			try (TemporaryPointerBuffer pb = new TemporaryPointerBuffer()) {
				vkMapMemory(device, bufferInfo.bufferMemory, 0, size(), 0, pb.buf);
				ByteBuffer bb = MemoryUtil.memByteBuffer(pb.value(), size());
				model.get(bb);
				view.get(1 * 16 * 4, bb);
				projection.get(2 * 16 * 4, bb);
				vkUnmapMemory(device, bufferInfo.bufferMemory);
			}
		}

		public int size() {
			return 3 * 16 * 4;
		}

		/**
		 * Updates the uniform buffer on the basis of camera position and angle This will probably be refactored at some point.
		 */
		public void update() {
			Matrix4f view = new Matrix4f();

			float x = (float) cameraX;
			float y = (float) cameraY;
			float z = (float) cameraZ;

			float dir_x = (float) (Math.cos(verticalCameraAngle) * Math.sin(horizontalCameraAngle));
			float dir_y = (float) (Math.cos(verticalCameraAngle) * Math.cos(horizontalCameraAngle));
			float dir_z = (float) (Math.sin(verticalCameraAngle));

			float look_x = x + dir_x;
			float look_y = y + dir_y;
			float look_z = z + dir_z;

			Vector3f v1 = new Vector3f(dir_x, dir_y, dir_z);
			Vector3f v2 = new Vector3f((float) (Math.cos(verticalCameraAngle) * Math.sin(horizontalCameraAngle - 3.1415926535 / 2)),
			                           (float) (Math.cos(verticalCameraAngle) * Math.cos(horizontalCameraAngle - 3.1415926535 / 2)),
			                           (float) (Math.sin(verticalCameraAngle)));
			v2.cross(v1);

			float up_x = (float) (Math.cos(verticalCameraAngle + 3.1415926535 / 2) * Math.sin(horizontalCameraAngle));
			float up_y = (float) (Math.cos(verticalCameraAngle + 3.1415926535 / 2) * Math.cos(horizontalCameraAngle));
			float up_z = (float) (Math.sin(verticalCameraAngle + 3.1415926535 / 2));

			// System.out.printf("\r%.4f/%.4f: (%.2f,%.2f,%.2f) looking at (%.2f,%.2f, %.2f) up: (%.4f, %.4f, %.4f)", horizontalCameraAngle, verticalCameraAngle, x, y, z,
			// look_x, look_y, look_z, up_x, up_y, up_z);
			view.setLookAt(x, y, z, look_x, look_y, look_z, up_x, up_y, up_z);

			try (TemporaryPointerBuffer pb = new TemporaryPointerBuffer()) {
				vkMapMemory(device, bufferInfo.bufferMemory, 0, size(), 0, pb.buf);
				ByteBuffer bb = MemoryUtil.memByteBuffer(pb.value(), size());
				view.get(1 * 16 * 4, bb);
				vkUnmapMemory(device, bufferInfo.bufferMemory);
			}
		}
	}

	private void createDescriptorPool() {
		VkDescriptorPoolSize.Buffer poolSize = VkDescriptorPoolSize.calloc(1);
		VkDescriptorPoolCreateInfo poolInfo = VkDescriptorPoolCreateInfo.calloc();
		try {
			poolSize.type(VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER);
			poolSize.descriptorCount(1);

			poolInfo.sType(VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO);
			poolInfo.pPoolSizes(poolSize);
			poolInfo.maxSets(1);

			try (TemporaryLongBuffer pbuf = new TemporaryLongBuffer()) {
				int ret = vkCreateDescriptorPool(device, poolInfo, null, pbuf.buf);
				errorIfNecessary(ret, "Unable to allocate descriptor pool: ");
				descriptorPool = pbuf.value();
			}
		} finally {
			poolSize.free();
			poolInfo.free();
		}
	}

	/**
	 *
	 */
	private void createDescriptorSetLayout() {
		VkDescriptorSetLayoutBinding.Buffer bindingInfo = VkDescriptorSetLayoutBinding.calloc(1);
		VkDescriptorSetLayoutCreateInfo layoutInfo = VkDescriptorSetLayoutCreateInfo.calloc();
		try {
			bindingInfo.binding(0);
			bindingInfo.descriptorType(VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER);
			bindingInfo.descriptorCount(1);
			bindingInfo.stageFlags(VK_SHADER_STAGE_VERTEX_BIT);

			layoutInfo.sType(VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO);
			layoutInfo.pBindings(bindingInfo);
			try (TemporaryLongBuffer pbuf = new TemporaryLongBuffer()) {
				int err = vkCreateDescriptorSetLayout(device, layoutInfo, null, pbuf.buf);
				errorIfNecessary(err, "Unable to create descriptor set: ");
				descriptorSetLayout = pbuf.value();
			}

		} finally {
			bindingInfo.free();
			layoutInfo.free();
		}
	}

	/**
	 *
	 */
	private void createSemaphores() {
		VkSemaphoreCreateInfo semaphoreInfo = VkSemaphoreCreateInfo.calloc();
		try {
			semaphoreInfo.sType(VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO);
			try (TemporaryLongBuffer buf = new TemporaryLongBuffer()) {
				int ret = vkCreateSemaphore(device, semaphoreInfo, null, buf.buf);
				errorIfNecessary(ret, "Unable to create semaphor: ");
				renderFinishedSemaphor = buf.value();
			}
			try (TemporaryLongBuffer buf = new TemporaryLongBuffer()) {
				int ret = vkCreateSemaphore(device, semaphoreInfo, null, buf.buf);
				errorIfNecessary(ret, "Unable to create semaphor: ");
				imageAvailableSemaphor = buf.value();
			}
		} finally {
			semaphoreInfo.free();
		}
	}

	/**
	 *
	 */
	private void recordGraphicsCommandBuffers() {
		for(int index = 0; index < graphicsCommandBuffers.length; index++) {
			VkCommandBuffer cb = graphicsCommandBuffers[index];
			VkCommandBufferBeginInfo commandBeginInfo = VkCommandBufferBeginInfo.calloc();
			VkRenderPassBeginInfo beginRenderPassInfo = VkRenderPassBeginInfo.calloc();
			VkOffset2D offset = VkOffset2D.calloc();
			VkClearValue.Buffer clearValue = VkClearValue.calloc(2);
			try {
				commandBeginInfo.sType(VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO);
				commandBeginInfo.flags(VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT);
				commandBeginInfo.pInheritanceInfo(null);
				int ret = vkBeginCommandBuffer(cb, commandBeginInfo);
				errorIfNecessary(ret, "Unable to begin recording in command buffers: ");

				beginRenderPassInfo.sType(VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO);
				beginRenderPassInfo.renderPass(renderPass);
				beginRenderPassInfo.framebuffer(framebuffers[index]);
				offset.x(0).y(0);
				beginRenderPassInfo.renderArea().offset(offset);
				beginRenderPassInfo.renderArea().extent(swapchainExtent);
				clearValue.get(0).color().float32(0, 0).float32(1, 0f).float32(2, 0).float32(3, 1);
				clearValue.get(1).depthStencil().depth(1.0f).stencil(0);
				beginRenderPassInfo.pClearValues(clearValue);
				vkCmdBeginRenderPass(cb, beginRenderPassInfo, VK_SUBPASS_CONTENTS_INLINE);

				vkCmdBindPipeline(cb, VK_PIPELINE_BIND_POINT_GRAPHICS, graphicsPipeline);

				try (TemporaryLongBuffer vertices = new TemporaryLongBuffer(terrainVertices.vertexBuffer);
				     TemporaryLongBuffer offsets = new TemporaryLongBuffer(0)) {
					vkCmdBindVertexBuffers(cb, 0, vertices.buf, offsets.buf);
				}
				vkCmdBindIndexBuffer(cb, terrainVertices.indexBuffer, 0, VK_INDEX_TYPE_UINT32);

				try (TemporaryLongBuffer ds = new TemporaryLongBuffer(descriptorSet)) {
					vkCmdBindDescriptorSets(cb, VK_PIPELINE_BIND_POINT_GRAPHICS, pipelineLayout, 0, ds.buf, null);
				}

				// vkCmdDraw(cb, 2 * 3, 1, 0, 0);
				vkCmdDrawIndexed(cb, terrainVertices.indexCount, 1, 0, 0, 0);

				ret = vkEndCommandBuffer(cb);
				errorIfNecessary(ret, "Failed to record command buffer: ");
			} finally {
				clearValue.free();
				offset.free();
				beginRenderPassInfo.free();
				commandBeginInfo.free();
			}
		}
	}

	/**
	 *
	 */
	private void createGraphicsCommandPool() {
		VkCommandPoolCreateInfo info = VkCommandPoolCreateInfo.calloc();
		try (TemporaryLongBuffer buf = new TemporaryLongBuffer()) {
			info.sType(VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO);
			info.queueFamilyIndex(graphicsQueueFamilyIndex);
			info.flags(0);
			int ret = vkCreateCommandPool(device, info, null, buf.buf);
			errorIfNecessary(ret, "Unable to create command pool: ");
			graphicsCommandPool = buf.value();
		} finally {
			info.free();
		}
	}

	/**
	 *
	 */
	private void createGraphicsCommandBuffers() {
		graphicsCommandBuffers = new VkCommandBuffer[framebuffers.length];
		VkCommandBufferAllocateInfo info = VkCommandBufferAllocateInfo.calloc();
		PointerBuffer buffer = memAllocPointer(graphicsCommandBuffers.length);
		try {
			info.sType(VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO);
			info.commandPool(graphicsCommandPool);
			info.level(VK_COMMAND_BUFFER_LEVEL_PRIMARY);
			info.commandBufferCount(graphicsCommandBuffers.length);
			int ret = vkAllocateCommandBuffers(device, info, buffer);
			errorIfNecessary(ret, "Unable to allocate command buffers: ");
			for(int i = 0; i < graphicsCommandBuffers.length; i++)
				graphicsCommandBuffers[i] = new VkCommandBuffer(buffer.get(i), device);
		} finally {
			memFree(buffer);
			info.free();
		}
	}

	/**
	 *
	 */
	private void createSwapchainFrameBuffers() {
		framebuffers = new long[swapChainImageViews.length];
		for(int i = 0; i < swapChainImageViews.length; i++) {
			VkFramebufferCreateInfo framebufferInfo = VkFramebufferCreateInfo.calloc();
			LongBuffer attachments = memAllocLong(2);
			try {
				attachments.put(swapChainImageViews[i]);
				attachments.put(depthBufferImage.imageView);
				attachments.flip();

				framebufferInfo.sType(VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO);
				framebufferInfo.renderPass(renderPass);
				framebufferInfo.pAttachments(attachments);
				framebufferInfo.width(swapchainWidth);
				framebufferInfo.height(swapchainHeight);
				framebufferInfo.layers(1);

				try (TemporaryLongBuffer buf = new TemporaryLongBuffer()) {
					int ret = vkCreateFramebuffer(device, framebufferInfo, null, buf.buf);
					errorIfNecessary(ret, "Unable to allocate framebuffer: ");
					framebuffers[i] = buf.value();
				}
			} finally {
				framebufferInfo.free();
				memFree(attachments);
			}
		}

	}

	/**
	 *
	 */
	private void createGraphicsPipelines() {
		try {
			createSimpleGraphicsPipeline();
		} catch(IOException ex) {
			System.out.println("Unable to create the simple graphics pipeline");
			ex.printStackTrace();
		}
	}

	/**
	 * @throws IOException
	 *
	 */
	private void createSimpleGraphicsPipeline() throws IOException {
		System.out.println("Loading Vertex Shader program.");
		long vertShaderModule = loadShader("shaders/vert.spv");
		System.out.println("Loading Fragment Shader program.");
		long fragShaderModule = loadShader("shaders/frag.spv");

		System.out.println("Allocating all the structures we'll need.");
		VkPipelineShaderStageCreateInfo vertexCreateInfo = VkPipelineShaderStageCreateInfo.calloc();
		VkPipelineShaderStageCreateInfo fragmentCreateInfo = VkPipelineShaderStageCreateInfo.calloc();
		VkPipelineShaderStageCreateInfo.Buffer shaderStages = VkPipelineShaderStageCreateInfo.calloc(2);
		VkPipelineVertexInputStateCreateInfo vertexInputInfo = VkPipelineVertexInputStateCreateInfo.calloc();
		VkPipelineInputAssemblyStateCreateInfo inputAssemblyState = VkPipelineInputAssemblyStateCreateInfo.calloc();
		VkViewport.Buffer viewport = VkViewport.calloc(1);
		VkRect2D.Buffer scissors = VkRect2D.calloc(1);
		VkPipelineViewportStateCreateInfo viewportStateInfo = VkPipelineViewportStateCreateInfo.calloc();
		VkPipelineRasterizationStateCreateInfo rasterizer = VkPipelineRasterizationStateCreateInfo.calloc();
		VkPipelineMultisampleStateCreateInfo multisample = VkPipelineMultisampleStateCreateInfo.calloc();
		VkPipelineColorBlendAttachmentState.Buffer colorBlendAttachment = VkPipelineColorBlendAttachmentState.calloc(1);
		VkPipelineColorBlendStateCreateInfo colorBlending = VkPipelineColorBlendStateCreateInfo.calloc();
		VkPipelineLayoutCreateInfo pipelineLayoutInfo = VkPipelineLayoutCreateInfo.calloc();
		VkAttachmentDescription.Buffer attachmentDescription = VkAttachmentDescription.calloc(1);
		// VkAttachmentReference.Buffer attachmentReference = VkAttachmentReference.calloc(1);
		VkAttachmentReference.Buffer attachmentReference = VkAttachmentReference.calloc(1);
		VkAttachmentReference depthAttachmentReference = VkAttachmentReference.calloc();
		VkSubpassDescription.Buffer subpass = VkSubpassDescription.calloc(1);
		VkSubpassDependency.Buffer subpassDependency = VkSubpassDependency.calloc(1);
		VkRenderPassCreateInfo renderPassInfo = VkRenderPassCreateInfo.calloc();
		VkPipelineDepthStencilStateCreateInfo depthStencil = VkPipelineDepthStencilStateCreateInfo.calloc();
		VkGraphicsPipelineCreateInfo.Buffer pipelineInfo = VkGraphicsPipelineCreateInfo.calloc(1);
		System.out.println("Structures allocated, now creating the pipeline info.");
		try {
			vertexCreateInfo.sType(VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO);
			vertexCreateInfo.stage(VK_SHADER_STAGE_VERTEX_BIT);
			vertexCreateInfo.module(vertShaderModule);
			vertexCreateInfo.pName(memUTF8("main"));
			shaderStages.get(0).set(vertexCreateInfo);

			fragmentCreateInfo.sType(VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO);
			fragmentCreateInfo.stage(VK_SHADER_STAGE_FRAGMENT_BIT);
			fragmentCreateInfo.module(fragShaderModule);
			fragmentCreateInfo.pName(memUTF8("main"));
			shaderStages.get(1).set(fragmentCreateInfo);

			vertexInputInfo.sType(VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO);
			vertexInputInfo.pVertexBindingDescriptions(terrainVertices.vertexBindingDescription);
			vertexInputInfo.pVertexAttributeDescriptions(terrainVertices.inputAttributeDescription);

			inputAssemblyState.sType(VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO);
			inputAssemblyState.topology(VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST);
			inputAssemblyState.primitiveRestartEnable(false);

			viewport.x(0);
			viewport.y(0);
			viewport.width(swapchainWidth);
			viewport.height(swapchainHeight);
			viewport.minDepth(0);
			viewport.maxDepth(1.0f);

			scissors.extent().width(swapchainWidth);
			scissors.extent().height(swapchainHeight);

			viewportStateInfo.sType(VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO);
			viewportStateInfo.viewportCount(1);
			viewportStateInfo.pViewports(viewport);
			viewportStateInfo.scissorCount(1);
			viewportStateInfo.pScissors(scissors);

			rasterizer.sType(VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO);
			rasterizer.depthClampEnable(false);
			rasterizer.rasterizerDiscardEnable(false);
			rasterizer.polygonMode(VK_POLYGON_MODE_FILL);
			rasterizer.lineWidth(1.0f);
			rasterizer.cullMode(VK_CULL_MODE_BACK_BIT);
			rasterizer.frontFace(VK_FRONT_FACE_COUNTER_CLOCKWISE);
			rasterizer.depthBiasEnable(false);
			// rasterizer.depthBiasConstantFactor(0);
			// rasterizer.depthBiasClamp(0);
			// rasterizer.depthBiasSlopeFactor(0);

			multisample.sType(VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO);
			multisample.sampleShadingEnable(false);
			multisample.rasterizationSamples(VK_SAMPLE_COUNT_1_BIT);
			multisample.minSampleShading(1.0f);
			multisample.pSampleMask(null);
			multisample.alphaToCoverageEnable(false);
			multisample.alphaToOneEnable(false);

			colorBlendAttachment.colorWriteMask(VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT);
			colorBlendAttachment.blendEnable(false);
			colorBlendAttachment.srcColorBlendFactor(VK_BLEND_FACTOR_SRC_ALPHA);
			colorBlendAttachment.dstColorBlendFactor(VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA);
			colorBlendAttachment.colorBlendOp(VK_BLEND_OP_ADD);
			colorBlendAttachment.srcAlphaBlendFactor(VK_BLEND_FACTOR_ONE);
			colorBlendAttachment.dstAlphaBlendFactor(VK_BLEND_FACTOR_ZERO);
			colorBlendAttachment.alphaBlendOp(VK_BLEND_OP_ADD);

			colorBlending.sType(VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO);
			colorBlending.logicOpEnable(false);
			colorBlending.logicOp(VK_LOGIC_OP_COPY);
			colorBlending.pAttachments(colorBlendAttachment);
			colorBlending.blendConstants(0, 0);
			colorBlending.blendConstants(1, 0);
			colorBlending.blendConstants(2, 0);
			colorBlending.blendConstants(3, 0);

			System.out.println("Creating the pipeline layout info.");
			try (TemporaryLongBuffer buf = new TemporaryLongBuffer(); TemporaryLongBuffer descriptors_buf = new TemporaryLongBuffer(descriptorSetLayout)) {
				pipelineLayoutInfo.sType(VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO);
				pipelineLayoutInfo.pNext(NULL);
				pipelineLayoutInfo.pSetLayouts(descriptors_buf.buf);
				int ret = vkCreatePipelineLayout(device, pipelineLayoutInfo, null, buf.buf);
				errorIfNecessary(ret, "Error creating pipeline layout");
				pipelineLayout = buf.value();
			}

			attachmentDescription.get(0).format(swapchainImageFormat);
			attachmentDescription.get(0).samples(VK_SAMPLE_COUNT_1_BIT);
			attachmentDescription.get(0).loadOp(VK_ATTACHMENT_LOAD_OP_CLEAR);
			attachmentDescription.get(0).storeOp(VK_ATTACHMENT_STORE_OP_STORE);
			attachmentDescription.get(0).stencilLoadOp(VK_ATTACHMENT_LOAD_OP_DONT_CARE);
			attachmentDescription.get(0).stencilStoreOp(VK_ATTACHMENT_LOAD_OP_DONT_CARE);
			attachmentDescription.get(0).initialLayout(VK_IMAGE_LAYOUT_UNDEFINED);
			attachmentDescription.get(0).finalLayout(VK_IMAGE_LAYOUT_PRESENT_SRC_KHR);

			attachmentReference.get(0).attachment(0);
			attachmentReference.get(0).layout(VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL);

			attachmentDescription.get(1).format(depthBufferImage.format);
			attachmentDescription.get(1).samples(VK_SAMPLE_COUNT_1_BIT);
			attachmentDescription.get(1).loadOp(VK_ATTACHMENT_LOAD_OP_CLEAR);
			attachmentDescription.get(1).storeOp(VK_ATTACHMENT_STORE_OP_DONT_CARE);
			attachmentDescription.get(1).stencilLoadOp(VK_ATTACHMENT_LOAD_OP_DONT_CARE);
			attachmentDescription.get(1).stencilStoreOp(VK_ATTACHMENT_STORE_OP_DONT_CARE);
			attachmentDescription.get(1).initialLayout(VK_IMAGE_LAYOUT_UNDEFINED);
			attachmentDescription.get(1).finalLayout(VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL);

			depthAttachmentReference.attachment(1);
			depthAttachmentReference.layout(VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL);

			subpass.pipelineBindPoint(VK_PIPELINE_BIND_POINT_GRAPHICS);
			subpass.colorAttachmentCount(1);
			subpass.pColorAttachments(attachmentReference);
			subpass.pDepthStencilAttachment(depthAttachmentReference);

			subpassDependency.srcSubpass(VK_SUBPASS_EXTERNAL);
			subpassDependency.dstSubpass(0);
			subpassDependency.srcStageMask(VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT);
			subpassDependency.srcAccessMask(0);
			subpassDependency.dstStageMask(VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT);
			subpassDependency.dstAccessMask(VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT);

			renderPassInfo.sType(VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO);
			renderPassInfo.pNext(NULL);
			renderPassInfo.pAttachments(attachmentDescription);
			renderPassInfo.pSubpasses(subpass);
			renderPassInfo.pDependencies(subpassDependency);

			System.out.println("creating the render pass");
			try (TemporaryLongBuffer buf = new TemporaryLongBuffer()) {
				int ret = vkCreateRenderPass(device, renderPassInfo, null, buf.buf);
				errorIfNecessary(ret, "Unable to create render pass: ");
				renderPass = buf.value();
			}

			/* Depth Buffer */
			depthStencil.sType(VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO);
			depthStencil.depthTestEnable(true);
			depthStencil.depthWriteEnable(true);
			depthStencil.depthCompareOp(VK_COMPARE_OP_GREATER);
			// depthStencil.depthCompareOp(VK_COMPARE_OP_LESS);
			depthStencil.depthBoundsTestEnable(false);
			depthStencil.minDepthBounds(0f);
			depthStencil.maxDepthBounds(1f);
			depthStencil.stencilTestEnable(false);

			/* Render Pipeline */
			pipelineInfo.sType(VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO);
			pipelineInfo.pStages(shaderStages);
			pipelineInfo.pVertexInputState(vertexInputInfo);
			pipelineInfo.pInputAssemblyState(inputAssemblyState);
			pipelineInfo.pViewportState(viewportStateInfo);
			pipelineInfo.pRasterizationState(rasterizer);
			pipelineInfo.pMultisampleState(multisample);
			pipelineInfo.pDepthStencilState(depthStencil);
			// pipelineInfo.pDepthStencilState(null);
			pipelineInfo.pColorBlendState(colorBlending);
			pipelineInfo.pDynamicState(null);
			pipelineInfo.layout(pipelineLayout);
			pipelineInfo.renderPass(renderPass);
			pipelineInfo.subpass(0);
			pipelineInfo.basePipelineHandle(VK_NULL_HANDLE);
			pipelineInfo.basePipelineIndex(-1);

			System.out.println("Creating the graphics pipeline.");
			try (TemporaryLongBuffer buf = new TemporaryLongBuffer()) {
				int ret = vkCreateGraphicsPipelines(device, VK_NULL_HANDLE, pipelineInfo, null, buf.buf);
				errorIfNecessary(ret, "Unable to create graphics pipeline");
				graphicsPipeline = buf.value();
			}
			System.out.println("Pipeline created!");
		} finally {
			System.out.println("Freeing memory!");
			pipelineInfo.free();
			depthStencil.free();
			renderPassInfo.free();
			subpass.free();
			depthAttachmentReference.free();
			attachmentReference.free();
			attachmentDescription.free();
			pipelineLayoutInfo.free();
			colorBlending.free();
			colorBlendAttachment.free();
			multisample.free();
			rasterizer.free();
			vertexCreateInfo.free();
			shaderStages.free();
			fragmentCreateInfo.free();
			vertexInputInfo.free();
			inputAssemblyState.free();
			viewport.free();
			System.out.println("Destroying the shader modules.");
			vkDestroyShaderModule(device, vertShaderModule, null);
			vkDestroyShaderModule(device, fragShaderModule, null);
			System.out.println("All memory cleared!");
		}
	}

	private long loadShader(String name) throws IOException {
		ByteBuffer code = readFile(name);
		VkShaderModuleCreateInfo info = VkShaderModuleCreateInfo.calloc();
		try (TemporaryLongBuffer b = new TemporaryLongBuffer()) {
			info.sType(VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO);
			info.pCode(code);
			int ret = vkCreateShaderModule(device, info, null, b.buf);
			errorIfNecessary(ret, "Unable to create shader module for " + name + ": ");
			return b.value();
		} finally {
			info.free();
		}
	}

	/**
	 * Reads a file. NOTE: you have to call memFree on the buffer returned.
	 *
	 * @param name
	 * @return
	 * @throws IOException
	 */
	private ByteBuffer readFile(String name) throws IOException {
		File f = new File(name);
		ByteBuffer b = memAlloc((int) f.length());

		try (FileChannel fc = FileChannel.open(f.toPath())) {
			while(b.hasRemaining())
				fc.read(b);
			b.flip();
		}
		return b;
	}

	private void createImageViews() {
		swapChainImageViews = new long[swapChainImages.capacity()];
		System.out.println("There are " + swapChainImageViews.length + " swap chain images.");
		for(int i = 0; i < swapChainImageViews.length; i++) {
			VkImageViewCreateInfo info = VkImageViewCreateInfo.calloc();
			try {
				info.sType(VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO);
				info.image(swapChainImages.get(i));
				info.viewType(VK_IMAGE_VIEW_TYPE_2D);
				info.format(swapchainImageFormat);
				info.components().r(VK_COMPONENT_SWIZZLE_IDENTITY);
				info.components().g(VK_COMPONENT_SWIZZLE_IDENTITY);
				info.components().b(VK_COMPONENT_SWIZZLE_IDENTITY);
				info.components().a(VK_COMPONENT_SWIZZLE_IDENTITY);
				info.subresourceRange().aspectMask(VK_IMAGE_ASPECT_COLOR_BIT);
				info.subresourceRange().baseMipLevel(0);
				info.subresourceRange().levelCount(1);
				info.subresourceRange().baseArrayLayer(0);
				info.subresourceRange().layerCount(1);
				try (TemporaryLongBuffer b = new TemporaryLongBuffer()) {
					int ret = vkCreateImageView(device, info, null, b.buf);
					errorIfNecessary(ret, "Unable to create image view " + i);
					swapChainImageViews[i] = b.value();
				}
			} finally {
				info.free();
			}
		}

	}

	private void createSwapChain() {
		VkSurfaceFormatKHR.Buffer surfaceFormats = null;
		IntBuffer presentModes = null;
		VkSurfaceCapabilitiesKHR surfaceCapabilities = null;
		VkSwapchainCreateInfoKHR chainInfo = null;
		try {
			/* Get the Surface Formats and choose one */
			try (TemporaryIntBuffer b = new TemporaryIntBuffer()) {
				int ret = vkGetPhysicalDeviceSurfaceFormatsKHR(physicalDevice, surface, b.buf, null);
				if(ret != VK_SUCCESS)
					throw new AssertionError("Unable to get surface format count: " + translateVulkanResult(ret));
				int count = b.buf.get(0);
				surfaceFormats = VkSurfaceFormatKHR.calloc(count);
				ret = vkGetPhysicalDeviceSurfaceFormatsKHR(physicalDevice, surface, b.buf, surfaceFormats);
				if(ret != VK_SUCCESS)
					throw new AssertionError("Unable to get surface formats: " + translateVulkanResult(ret));

				/* Choose one */
				chooseColorSpaceAndColorFormat(surfaceFormats, count);
			}

			/* Get the presentation modes and choose one */
			try (TemporaryIntBuffer b = new TemporaryIntBuffer()) {
				int ret = vkGetPhysicalDeviceSurfacePresentModesKHR(physicalDevice, surface, b.buf, null);
				if(ret != VK_SUCCESS)
					throw new AssertionError("Unable to get surface presentation mode count: " + translateVulkanResult(ret));
				int mode_count = b.buf.get(0);
				presentModes = memAllocInt(mode_count);
				ret = vkGetPhysicalDeviceSurfacePresentModesKHR(physicalDevice, surface, b.buf, presentModes);
				if(ret != VK_SUCCESS)
					throw new AssertionError("Unable to get surface presentation modes: " + translateVulkanResult(ret));
				/* Choose One */
				presentationMode = choosePresentationMode(presentModes, mode_count);
			}

			/* Choose the Swap Extent */
			int width, height, image_count;
			{
				surfaceCapabilities = VkSurfaceCapabilitiesKHR.calloc();
				int ret = vkGetPhysicalDeviceSurfaceCapabilitiesKHR(physicalDevice, surface, surfaceCapabilities);
				errorIfNecessary(ret, "Unable to get surface capabilities: ");
				VkExtent2D extent = surfaceCapabilities.currentExtent();
				if(extent.width() != -1 && extent.height() != -1) {
					width = extent.width();
					height = extent.height();
				} else {
					width = surfaceCapabilities.maxImageExtent().width();
					height = surfaceCapabilities.maxImageExtent().height();
					System.out.println("!!!!! Somehow the surface extent is flexible. Going with " + width + "x" + height);
				}
				image_count = surfaceCapabilities.minImageCount();
				if(image_count < 2) {
					if(surfaceCapabilities.maxImageCount() == 0 || surfaceCapabilities.maxImageCount() >= 2) {
						image_count = 2;
					}
				}
				swapchainWidth = width;
				swapchainHeight = height;
				swapchainExtent = VkExtent2D.calloc();
				swapchainExtent.width(swapchainWidth);
				swapchainExtent.height(swapchainHeight);
			}

			/* Create the Swap Chain */
			chainInfo = VkSwapchainCreateInfoKHR.calloc();
			chainInfo.sType(VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR);
			chainInfo.surface(surface);
			chainInfo.minImageCount(image_count);
			chainInfo.imageFormat(swapchainImageFormat);
			chainInfo.imageColorSpace(swapchainColorSpace);
			chainInfo.imageExtent().width(width).height(height);
			chainInfo.imageArrayLayers(1);
			chainInfo.imageUsage(VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT);
			chainInfo.imageSharingMode(VK_SHARING_MODE_EXCLUSIVE);
			chainInfo.preTransform(surfaceCapabilities.currentTransform());
			chainInfo.compositeAlpha(VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR);
			chainInfo.presentMode(presentationMode);
			chainInfo.clipped(true);
			chainInfo.oldSwapchain(swapChain);
			try (TemporaryLongBuffer buf = new TemporaryLongBuffer()) {
				int ret = vkCreateSwapchainKHR(device, chainInfo, null, buf.buf);
				if(ret != VK_SUCCESS)
					throw new AssertionError("Unable to create SwapChain: " + translateVulkanResult(ret));
				swapChain = buf.value();
			}

			/* Get the Swapchain images */
			try (TemporaryIntBuffer buf = new TemporaryIntBuffer()) {
				int ret = vkGetSwapchainImagesKHR(device, swapChain, buf.buf, null);
				errorIfNecessary(ret, "Unable to get the swapchain image count: ");
				int count = buf.value();
				swapChainImages = memAllocLong(count);
				ret = vkGetSwapchainImagesKHR(device, swapChain, buf.buf, swapChainImages);
				errorIfNecessary(ret, "Unable to get SwapChain images: ");
			}
		} finally {
			if(surfaceFormats != null)
				surfaceFormats.free();
			if(presentModes != null)
				memFree(presentModes);
			if(surfaceCapabilities != null)
				surfaceCapabilities.free();
			if(chainInfo != null)
				chainInfo.free();
		}
	}

	/**
	 *
	 * @param ret
	 * @param string
	 */
	private void errorIfNecessary(int ret, String string) {
		if(ret != VK_SUCCESS)
			throw new AssertionError(string + translateVulkanResult(ret));
	}

	/**
	 *
	 * @param presentModes
	 * @param mode_count
	 */
	private int choosePresentationMode(IntBuffer presentModes, int count) {
		return VK_PRESENT_MODE_FIFO_KHR;
	}

	/**
	 * Picks a color format and color space for our swap chain.
	 *
	 * @param formats
	 */
	private void chooseColorSpaceAndColorFormat(VkSurfaceFormatKHR.Buffer formats, int formatCount) {
		if(formatCount == 1 && formats.get(0).format() == VK_FORMAT_UNDEFINED) {
			swapchainImageFormat = VK_FORMAT_B8G8R8A8_UNORM;
			swapchainColorSpace = VK_COLOR_SPACE_SRGB_NONLINEAR_KHR;
		} else {
			boolean assigned = false;
			for(int i = 0; i < formatCount; i++) {
				VkSurfaceFormatKHR f = formats.get(i);
				if(f.colorSpace() == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR && f.format() == VK_FORMAT_B8G8R8A8_UNORM) {
					assigned = true;
					swapchainImageFormat = VK_FORMAT_B8G8R8A8_UNORM;
					swapchainColorSpace = VK_COLOR_SPACE_SRGB_NONLINEAR_KHR;
				}
			}
			if(!assigned) {
				VkSurfaceFormatKHR f = formats.get(0);
				swapchainImageFormat = f.format();
				swapchainColorSpace = f.colorSpace();
			}
		}
	}

	private void createWindow(final VkDebugReportCallbackEXT debugCallback) {
		// Configure our window
		glfwDefaultWindowHints(); // optional, the current window hints are already the default
		glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
		glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE); // the window will stay hidden after creation
		// glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE); // the window will be resizable

		int WIDTH = 1920;
		int HEIGHT = 1080;

		// Create the window
		window = glfwCreateWindow(WIDTH, HEIGHT, "Hello World!", NULL, NULL);
		if(window == NULL)
			throw new RuntimeException("Failed to create the GLFW window");
		// Setup a key callback. It will be called every time a key is pressed, repeated or released.
		glfwSetKeyCallback(window, (window, key, scancode, action, mods) -> {
			if(key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE)
				glfwSetWindowShouldClose(window, true); // We will detect this in our rendering loop

			if(key == GLFW_KEY_W && action != GLFW_RELEASE) {
				Vector3f d = getLookingDirection(horizontalCameraAngle, verticalCameraAngle);
				System.out.printf("\r at (%.3f,%.3f,%.3f) looking at (%.3f,%.2f,%.3f)", cameraX, cameraY, cameraZ, d.x, d.y, d.z);
				cameraX += d.x / 1;
				cameraY += d.y / 1;
				cameraZ += d.z / 1;
			}
			if(key == GLFW_KEY_S && action != GLFW_RELEASE) {
				Vector3f d = getLookingDirection(horizontalCameraAngle, verticalCameraAngle);
				cameraX -= d.x / 10;
				cameraY -= d.y / 10;
				cameraZ -= d.z / 10;
			}

			if(key == GLFW_KEY_A && action != GLFW_RELEASE) {
				// Vector3f d = getLookingDirection(horizontalCameraAngle - 3.1415926535 / 2, verticalCameraAngle);
				cameraX += Math.sin(horizontalCameraAngle - 3.1415926535 / 2) / 10;
				cameraY += Math.cos(horizontalCameraAngle - 3.1415926535 / 2) / 10;
				// cameraZ += d.z/10;
			}
			if(key == GLFW_KEY_D && action != GLFW_RELEASE) {
				// Vector3f d = getLookingDirection(horizontalCameraAngle + 3.1415926535 / 2, verticalCameraAngle);
				cameraX += Math.sin(horizontalCameraAngle + 3.1415926535 / 2) / 10;
				cameraY += Math.cos(horizontalCameraAngle + 3.1415926535 / 2) / 10;
				// cameraZ += d.z/10;
			}

		});
		final long debugCallbackHandle = setupDebugging(instance, VK_DEBUG_REPORT_ERROR_BIT_EXT | VK_DEBUG_REPORT_WARNING_BIT_EXT, debugCallback);

		try (TemporaryLongBuffer pSurface = new TemporaryLongBuffer()) {
			int err = glfwCreateWindowSurface(instance, window, null, pSurface.buf);
			if(err != VK_SUCCESS)
				throw new AssertionError("Failed to create surface: " + err);
			surface = pSurface.buf.get(0);
		}

		// Enable v-sync
		// glfwSwapInterval(1);
	}

	private Vector3f getLookingDirection(double horizontalAngle, double verticalAngle) {
		Vector3f d = new Vector3f();

		d.x = (float) (Math.sin(horizontalAngle) * Math.cos(verticalAngle));
		d.y = (float) (Math.cos(horizontalAngle) * Math.cos(verticalAngle));
		d.z = (float) (Math.sin(verticalAngle));

		// System.out.printf("\r looking at %s", d);

		return d.normalize();
	}

	/**
	 * @return
	 *
	 */
	private VkDevice createLogicalDevice() {
		FloatBuffer queue_priority = memAllocFloat(1).put(0.0f);
		queue_priority.flip();
		VkDeviceQueueCreateInfo.Buffer queue_create_info = VkDeviceQueueCreateInfo.calloc(1);
		queue_create_info.sType(VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO);
		queue_create_info.queueFamilyIndex(graphicsQueueFamilyIndex);
		queue_create_info.pQueuePriorities(queue_priority);

		PointerBuffer extensions = memAllocPointer(1);
		ByteBuffer VK_KHR_SWAPCHAIN_EXTENSION = memUTF8(VK_KHR_SWAPCHAIN_EXTENSION_NAME);
		extensions.put(VK_KHR_SWAPCHAIN_EXTENSION);
		extensions.flip();
		PointerBuffer ppEnabledLayerNames = memAllocPointer(layers.length);
		if(validation)
			for(int i = 0; i < layers.length; i++)
				ppEnabledLayerNames.put(layers[i]);
		ppEnabledLayerNames.flip();

		VkDeviceCreateInfo device_create_info = VkDeviceCreateInfo.calloc();
		device_create_info.sType(VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO);
		device_create_info.pNext(NULL);
		device_create_info.pQueueCreateInfos(queue_create_info);
		device_create_info.ppEnabledExtensionNames(extensions);
		device_create_info.ppEnabledLayerNames(ppEnabledLayerNames);

		PointerBuffer pDevice = memAllocPointer(1);
		int err = vkCreateDevice(physicalDevice, device_create_info, null, pDevice);
		long device_id = pDevice.get(0);
		memFree(pDevice);
		if(err != VK_SUCCESS)
			throw new AssertionError("Failed to create VkDevice:" + err);
		VkDevice device = new VkDevice(device_id, physicalDevice, device_create_info);

		// Cleanup
		device_create_info.free();
		memFree(ppEnabledLayerNames);
		memFree(VK_KHR_SWAPCHAIN_EXTENSION);
		memFree(extensions);
		memFree(queue_priority);

		return device;
	}

	/**
	 *
	 */
	private void createDeviceQueue() {
		System.out.println("About to get the device queue (device == " + device + " graphics family index == " + graphicsQueueFamilyIndex + ")");
		try (TemporaryPointerBuffer pQueue = new TemporaryPointerBuffer()) {
			vkGetDeviceQueue(device, graphicsQueueFamilyIndex, 0, pQueue.buf);
			System.out.println("About to create the VkQueue");
			graphicsQueue = new VkQueue(pQueue.buf.get(0), device);
		}
	}

	/**
	 *
	 * @param requiredExtensions
	 * @return
	 */
	private VkInstance createVulkanInstance(PointerBuffer requiredExtensions) {
		/* The App Info */
		VkApplicationInfo appInfo = VkApplicationInfo.calloc();
		appInfo.sType(VK_STRUCTURE_TYPE_APPLICATION_INFO);
		appInfo.pApplicationName(memUTF8("Order of the Wilds"));
		appInfo.pEngineName(memUTF8(""));
		appInfo.apiVersion(VK_MAKE_VERSION(1, 0, 2));

		/* Extensions */
		ByteBuffer VK_EXT_DEBUG_REPORT_EXTENSION = memUTF8(VK_EXT_DEBUG_REPORT_EXTENSION_NAME);
		PointerBuffer ppEnabledExtensionNames = memAllocPointer(requiredExtensions.remaining() + 1);
		ppEnabledExtensionNames.put(requiredExtensions);
		ppEnabledExtensionNames.put(VK_EXT_DEBUG_REPORT_EXTENSION);
		ppEnabledExtensionNames.flip();

		/* Validation Layers */
		PointerBuffer ppEnabledLayerNames = memAllocPointer(layers.length);
		if(validation)
			for(int i = 0; i < layers.length; i++)
				ppEnabledLayerNames.put(layers[i]);
		ppEnabledLayerNames.flip();

		/* Creating the instance */
		VkInstanceCreateInfo createInfo = VkInstanceCreateInfo.calloc();
		createInfo.sType(VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO);
		createInfo.pNext(NULL);
		createInfo.pApplicationInfo(appInfo);
		createInfo.ppEnabledExtensionNames(ppEnabledExtensionNames);
		createInfo.ppEnabledLayerNames(ppEnabledLayerNames);

		PointerBuffer pInstance = memAllocPointer(1);
		int err = vkCreateInstance(createInfo, null, pInstance);
		long instance = pInstance.get(0);
		memFree(pInstance);

		if(err != VK_SUCCESS)
			throw new AssertionError("Failed to create VkInstnace: " + err);

		VkInstance ret = new VkInstance(instance, createInfo);
		createInfo.free();
		memFree(ppEnabledLayerNames);
		memFree(VK_EXT_DEBUG_REPORT_EXTENSION);
		memFree(ppEnabledExtensionNames);
		memFree(appInfo.pApplicationName());
		memFree(appInfo.pEngineName());
		appInfo.free();

		return ret;
	}

	private VkPhysicalDevice getPhysicalDevice(long surface) {
		IntBuffer physicalDeviceCount = memAllocInt(1);
		int err = vkEnumeratePhysicalDevices(instance, physicalDeviceCount, null);
		if(err != VK_SUCCESS)
			throw new AssertionError("Failed to get number of physical devices: " + err);
		PointerBuffer physicalDevices = memAllocPointer(physicalDeviceCount.get(0));
		err = vkEnumeratePhysicalDevices(instance, physicalDeviceCount, physicalDevices);

		// long physical_device = physicalDevices.get(0);
		VkPhysicalDevice physical_device = null;
		for(int i = 0; i < physicalDeviceCount.get(0); i++) {
			VkPhysicalDevice dev = new VkPhysicalDevice(physicalDevices.get(i), instance);
			if(isDeviceSuitable(dev, surface)) {
				System.out.println("    picking device " + i);
				physical_device = dev;
			}
		}

		memFree(physicalDeviceCount);
		memFree(physicalDevices);
		if(err != VK_SUCCESS)
			throw new AssertionError("Failed to get physical devices: " + err);

		graphicsQueueFamilyIndex = getGraphicsQueueFamily(physical_device, surface);
		return physical_device;
	}

	boolean isDeviceSuitable(VkPhysicalDevice dev, long surface) {
		VkPhysicalDeviceProperties properties = VkPhysicalDeviceProperties.calloc();
		vkGetPhysicalDeviceProperties(dev, properties);

		VkPhysicalDeviceFeatures features = VkPhysicalDeviceFeatures.calloc();
		vkGetPhysicalDeviceFeatures(dev, features);

		// todo: check for swap chain support

		try {
			if(properties.deviceType() == VK_PHYSICAL_DEVICE_TYPE_CPU)
				System.out.println("Vulkan device is a CPU");
			else if(properties.deviceType() == VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU)
				System.out.println("Vulkan device is an integrated GPU");
			else if(properties.deviceType() == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU)
				System.out.println("Vulkan device is a discrete GPU");
			else if(properties.deviceType() == VK_PHYSICAL_DEVICE_TYPE_VIRTUAL_GPU)
				System.out.println("Vulkan device is a virtual GPU");
			else
				System.out.println("Vulkan device is... \"other\"");

			if(features.geometryShader())
				System.out.println("    has a geometry shader");
			if(features.tessellationShader())
				System.out.println("    has a tesselation shader");
			if(features.shaderFloat64())
				System.out.println("    shader supports 64 bit floats");
			System.out.println("    max image dimension: " + properties.limits().maxImageDimension2D());
			return properties.deviceType() == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU && features.geometryShader() && getGraphicsQueueFamily(dev, surface) >= 0;
		} finally {
			properties.free();
			features.free();
		}
	}

	/**
	 * Returns -1 if no graphics queue family is supported, otherwise the index of it
	 *
	 * @param device
	 * @param surface
	 * @return
	 */
	private int getGraphicsQueueFamily(VkPhysicalDevice device, long surface) {
		IntBuffer count_buffer = memAllocInt(1);
		vkGetPhysicalDeviceQueueFamilyProperties(device, count_buffer, null);
		int count = count_buffer.get(0);

		VkQueueFamilyProperties.Buffer properties_buffer = VkQueueFamilyProperties.calloc(count);
		vkGetPhysicalDeviceQueueFamilyProperties(device, count_buffer, properties_buffer);

		try {
			for(int i = 0; i < count; i++) {
				if((properties_buffer.get(i).queueFlags() & VK_QUEUE_GRAPHICS_BIT) != 0) {
					int[] ret = { 0 };
					vkGetPhysicalDeviceSurfaceSupportKHR(device, i, surface, ret);
					if(ret[0] == 1)
						return i;
				}
			}
		} finally {
			memFree(count_buffer);
			properties_buffer.free();
		}
		System.out.println("!!!!! Unable to find graphics queue index.");
		return -1;
	}

	private void loop() {
		// Run the rendering loop until the user has attempted to close the window or has pressed the ESCAPE key.
		while(!glfwWindowShouldClose(window)) {
			// work goes here
			drawFrame();

			// Poll for window events. The key callback above will only be invoked during this call.
			glfwPollEvents();
		}
		vkDeviceWaitIdle(device);
	}

	public static void main(String[] args) {
		System.out.println("validation == " + validation);
		new HelloWorld().run();
	}

	private static long setupDebugging(VkInstance instance, int flags, VkDebugReportCallbackEXT callback) {
		// Again, a struct to create something, in this case the debug report callback
		VkDebugReportCallbackCreateInfoEXT dbgCreateInfo = VkDebugReportCallbackCreateInfoEXT.calloc()
		                                                                                     .sType(VK_STRUCTURE_TYPE_DEBUG_REPORT_CALLBACK_CREATE_INFO_EXT) // <- the
		                                                                                                                                                     // struct type
		                                                                                     .pNext(NULL) // <- must be NULL
		                                                                                     .pfnCallback(callback) // <- the actual function pointer (in
		                                                                                                            // LWJGL a Callback)
		                                                                                     .pUserData(NULL) // <- any user data provided to the debug
		                                                                                                      // report callback function
		                                                                                     .flags(flags); // <- indicates which kind of messages
		                                                                                                    // we want to receive
		LongBuffer pCallback = memAllocLong(1); // <- allocate a LongBuffer (for a non-dispatchable handle)
		// Actually create the debug report callback
		int err = vkCreateDebugReportCallbackEXT(instance, dbgCreateInfo, null, pCallback);
		long callbackHandle = pCallback.get(0);
		memFree(pCallback); // <- and free the LongBuffer
		dbgCreateInfo.free(); // <- and also the create-info struct
		if(err != VK_SUCCESS) {
			throw new AssertionError("Failed to create VkInstance: " + err);
		}
		return callbackHandle;
	}

	private int findMemoryType(int typeFilter, int properties) {
		VkPhysicalDeviceMemoryProperties memProperties = VkPhysicalDeviceMemoryProperties.calloc();
		try {
			vkGetPhysicalDeviceMemoryProperties(physicalDevice, memProperties);

			for(int i = 0; i < memProperties.memoryTypeCount(); i++) {
				if((typeFilter & (1 << i)) != 0 && ((memProperties.memoryTypes(i).propertyFlags() & properties) == properties))
					return i;
			}
		} finally {
			memProperties.free();
		}

		throw new AssertionError("Unable to find suitable memory type!");
	}

	private int findSupportedFormat(int[] candidates, int tiling, int features) {
		VkFormatProperties props = VkFormatProperties.calloc();
		try {
			for(int format : candidates) {
				vkGetPhysicalDeviceFormatProperties(physicalDevice, format, props);
				if(tiling == VK_IMAGE_TILING_LINEAR && (props.linearTilingFeatures() & features) == features)
					return format;
				else if(tiling == VK_IMAGE_TILING_OPTIMAL && (props.optimalTilingFeatures() & features) == features)
					return format;

			}
			throw new AssertionError("Unable to find a supported format");
		} finally {
			props.free();
		}
	}

	public static boolean hasStencilComponent(int format) {
		return format == VK_FORMAT_D32_SFLOAT_S8_UINT || format == VK_FORMAT_D24_UNORM_S8_UINT || format == VK_FORMAT_D16_UNORM_S8_UINT;
	}

	/**
	 * Creates the terrain vertex buffer & associated data
	 */
	private void createTerrain() {
		// createTerrainMesh();

		terrainHeightmaps = createTerrainHeigtmaps(1);
		ByteBuffer vertexData = heightmapToVertexData(terrainHeightmaps[0]);
		ByteBuffer indexData = createIndexDataForTerrainMap();
		int vertex_count = 1000 * 1000;
		int index_count = 999 * 999 * 2 * 3;

		// {
		// IntBuffer ib = indexData.asIntBuffer();
		// System.out.println("first triangle: ("+ib.get()+","+ib.get()+","+ib.get()+")");
		// }
//		{
//			FloatBuffer fb = vertexData.asFloatBuffer();
//			for(int i = 0; i < 3; i++)
//				System.out.println("  " + fb.get() + ", " + fb.get() + ", " + fb.get());
//
//			System.out.println("");
//
//			for(int i = 0; i < 3; i++)
//				System.out.println("  " + fb.get() + ", " + fb.get() + ", " + fb.get());
//		}
		//
		//
//		{
//			System.out.println("1001st triangle:");
//			int offset = 1001 * 9;
//			FloatBuffer fb = vertexData.asFloatBuffer();
//			for(int i = 0; i < 3; i++)
//				System.out.println("  " + fb.get(offset + i * 3) + ", " + fb.get(offset + i * 3 + 1) + ", " + fb.get(offset + i * 3 + 2));
//		}


		VkVertexInputBindingDescription.Buffer terrainVertexBindingDescription = VkVertexInputBindingDescription.calloc(1);
		terrainVertexBindingDescription.binding(0);
		terrainVertexBindingDescription.stride((3 + 3 + 3) * 4);
		terrainVertexBindingDescription.inputRate(VK_VERTEX_INPUT_RATE_VERTEX);

		VkVertexInputAttributeDescription.Buffer terrainVertexinputAttributes = VkVertexInputAttributeDescription.calloc(3);
		terrainVertexinputAttributes.get(0).binding(0).location(0).format(VK_FORMAT_R32G32B32_SFLOAT).offset(0);
		terrainVertexinputAttributes.get(1).binding(0).location(1).format(VK_FORMAT_R32G32B32_SFLOAT).offset(12);
		terrainVertexinputAttributes.get(2).binding(0).location(2).format(VK_FORMAT_R32G32B32_SFLOAT).offset(24);

		BufferInfo vertex_buffer_info = toVulkanBuffer(vertexData, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
		                                               VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, true);

		BufferInfo index_buffer_info = toVulkanBuffer(indexData, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT,
		                                              VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, true);

		terrainVertices = new Vertices();
		terrainVertices.vertexCount = vertex_count;
		terrainVertices.vertexData = vertexData;
		terrainVertices.vertexBindingDescription = terrainVertexBindingDescription;
		terrainVertices.inputAttributeDescription = terrainVertexinputAttributes;
		terrainVertices.vertexBuffer = vertex_buffer_info.buffer;
		terrainVertices.vertexBufferMemory = vertex_buffer_info.bufferMemory;
		terrainVertices.indexBuffer = index_buffer_info.buffer;
		terrainVertices.indexMemory = index_buffer_info.bufferMemory;
		terrainVertices.indexCount = index_count;
	}

	/**
	 * Right now we start with a 4km map, where we're at the center. Each square kilometer lives within its own array
	 */
	private void createTerrainMesh() {
		int kmCount = 1;

	}

	/**
	 *
	 * @return
	 */
	private ByteBuffer createIndexDataForTerrainMap() {
		ByteBuffer data = memAlloc(999 * 999 * 2 * 3 * 4);
		IntBuffer indices = data.asIntBuffer();

		for(int i = 0; i < 999; i++) {
			for(int j = 0; j < 999; j++) {
				indices.put(i * 1000 + j);
				indices.put(i * 1000 + j + 1);
				indices.put((i + 1) * 1000 + j);

				indices.put((i + 1) * 1000 + j);
				indices.put(i * 1000 + j + 1);
				indices.put((i + 1) * 1000 + j + 1);
			}
		}

		return data;
	}

	/**
	 *
	 * @param heightmap
	 * @param h
	 * @param g
	 * @param f
	 * @return
	 */
	private ByteBuffer heightmapToVertexData(float[] heightmap) {
		int vertex_size = (3 + 3 + 3) * 4;
		int stride = 1000 * 12;
		ByteBuffer data = memAlloc(1000 * 1000 * vertex_size);
		FloatBuffer vertices = data.asFloatBuffer();
		Vector3f v1 = new Vector3f(), v2 = new Vector3f();

		float max_height = Float.MIN_VALUE;
		float min_height = Float.MAX_VALUE;
		for(float h : heightmap) {
			max_height = Math.max(max_height, h);
			min_height = Math.min(min_height, h);
		}
		System.out.println("Maximum height: " + max_height);
		System.out.println("Minimum height: " + min_height);

		for(int row = 0; row < 1000; row++) {
			for(int col = 0; col < 1000; col++) {
				float height = heightmap[row * 1000 + col];
				/* Coordinates */
				vertices.put(col);
				vertices.put(row);
				vertices.put(height);

				/* Colors */
				vertices.put(Math.min(1.0f, Math.max(0, (height - min_height) / (5f * (max_height - min_height)))));
				vertices.put(Math.min(0.3f, Math.max(0.1f, (height - min_height) / (max_height - min_height))));
				vertices.put(Math.min(1.0f, Math.max(0, (height - min_height) / (8f * (max_height - min_height)))));

				/* Normals */
				if(row > 1 && row < 999) {
					if(col > 1 && col < 999) {
						/* Main Area */
						v1.set(1, 1, heightmap[1000 * (row - 1) + (col - 1)] - heightmap[1000 * (row + 1) + (col + 1)]);
						v2.set(-1, 1, heightmap[1000 * (row - 1) + (col + 1)] - heightmap[1000 * (row + 1) + (col - 1)]);
					} else {
						/* Right and left columns */
						v1.set(1, 1, heightmap[1000 * (row + 1) + (col)]);
						v2.set(-1, 1, heightmap[1000 * (row - 1) + (col)]);
					}
				} else {
					if(col > 1 && col < 999) {
						/* Top and bottom edges */
						v1.set(1, 0, heightmap[1000 * (row) + (col + 1)]);
						v2.set(-1, 0, heightmap[1000 * (row) + (col - 1)]);
					} else {
						/* Four corners */
						v1.set(0, 1, 0);
						v2.set(1, 0, 0);
					}
				}
				if(col == 2 && row == 2) {
					System.out.println("2,2 triangle: " + v2 + " X " + v1);
				}
				v1.cross(v2);
				v1.normalize();
				v1.get(vertices);
				vertices.position(vertices.position() + 3);

				if(col == 2 && row == 2)
					System.out.println("    cross-product is " + v1);
			}
		}

		return data;
	}

	private float[][] createTerrainHeigtmaps(int kmCount) {
		float[][] heightmaps = new float[kmCount][1000 * 1000];

		Random rand = new Random(1);

		/* Step 1: allocate the average height and standard deviation of the heightmap */
		for(int i = 0; i < kmCount; i++) {
			heightmaps[i][0] = 200 * rand.nextFloat();
			heightmaps[i][1] = rand.nextFloat();
		}

		/* Step 1: generate the local average heights */
		for(float[] map : heightmaps) {
			rand.setSeed((long) (1000000 * map[0]));
			float average = map[0];
			float variance = map[1];

			System.out.println("Average == " + average + ", variance == " + variance);

			for(int i = 0; i < 4; i++) {
				int offset = 1000 * 500 * (i / 2) + 500 * (i % 2);
				float average2 = average + 200 * variance * (rand.nextFloat() - 0.5f);
				System.out.println("    Average2 == " + average2);
				map[offset] = average2;
				for(int j = 0; j < 4; j++) {
					int offset2 = offset + 1000 * 250 * (j / 2) + 250 * (j % 2);
					float average3 = average2 + 100 * variance * (rand.nextFloat() - 0.5f);
					map[offset2] = average3;
				}
			}
		}

		/* Step 2: generate the individual grid point heights */
		for(float[] map : heightmaps) {
			rand.setSeed((long) (1000000 * map[0]));
			float average = map[0];
			float variance = map[1];

			for(int i = 0; i < 4; i++) {
				int offset = 1000 * 500 * (i / 2) + 500 * (i % 2);
				for(int j = 0; j < 4; j++) {
					int offset2 = offset + 1000 * 250 * (j / 2) + 250 * (j % 2);
					float local_base_height = map[offset2];
					System.out.println("    Local base height: " + local_base_height);
					for(int row = 0; row < 250; row++) {
						for(int col = 0; col < 250; col++) {
							try {
								map[offset2 + 1000 * row + col] = local_base_height + 5 * variance * (rand.nextFloat() - 0.5f);
							} catch(ArrayIndexOutOfBoundsException ex) {
								ex.printStackTrace();
								System.out.printf("i == %d, j == %d, row == %d, col == %d => offset2 == %d\n", i, j, row, col, offset2);
								System.exit(-1);
							}
						}
					}
				}
			}
		}

		return heightmaps;
	}

	private BufferInfo toVulkanBuffer(ByteBuffer data, int usage, int sharing, boolean useStaging) {
		BufferInfo buf;
		if(useStaging) {
			buf = createBuffer(data.capacity(), VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);
		} else {
			buf = createBuffer(data.capacity(), usage, sharing);
		}

		try (TemporaryPointerBuffer pbuf = new TemporaryPointerBuffer()) {
			int ret = vkMapMemory(device, buf.bufferMemory, 0, data.capacity(), 0, pbuf.buf);
			errorIfNecessary(ret, "Unable to map buffer memory");
			MemoryUtil.memCopy(memAddress(data), pbuf.value(), data.remaining());
			vkUnmapMemory(device, buf.bufferMemory);
		}

		if(useStaging) {
			BufferInfo real = createBuffer(data.capacity(), usage, sharing);
			copyBuffer(buf.buffer, real.buffer, data.capacity());
			buf.free(device);
			buf = real;
		}

		return buf;
	}

	private void copyBuffer(long srcBuffer, long dstBuffer, int size) {
		VkCommandBufferAllocateInfo allocInfo = VkCommandBufferAllocateInfo.calloc();
		VkCommandBufferBeginInfo beginInfo = VkCommandBufferBeginInfo.calloc();
		VkBufferCopy.Buffer copyInfo = VkBufferCopy.calloc(1);
		VkSubmitInfo submitInfo = VkSubmitInfo.calloc();
		try (TemporaryPointerBuffer buf = new TemporaryPointerBuffer()) {
			allocInfo.sType(VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO);
			allocInfo.level(VK_COMMAND_BUFFER_LEVEL_PRIMARY);
			allocInfo.commandPool(graphicsCommandPool);
			allocInfo.commandBufferCount(1);

			int ret = vkAllocateCommandBuffers(device, allocInfo, buf.buf);
			errorIfNecessary(ret, "Unable to allocate copy command buffer");
			VkCommandBuffer command_buffer = new VkCommandBuffer(buf.value(), device);

			beginInfo.sType(VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO);
			beginInfo.flags(VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT);
			vkBeginCommandBuffer(command_buffer, beginInfo);

			copyInfo.srcOffset(0);
			copyInfo.dstOffset(0);
			copyInfo.size(size);
			vkCmdCopyBuffer(command_buffer, srcBuffer, dstBuffer, copyInfo);
			vkEndCommandBuffer(command_buffer);

			try (TemporaryPointerBuffer pb = new TemporaryPointerBuffer(command_buffer)) {
				submitInfo.sType(VK_STRUCTURE_TYPE_SUBMIT_INFO);
				submitInfo.pCommandBuffers(pb.buf);
				vkQueueSubmit(graphicsQueue, submitInfo, VK_NULL_HANDLE);
				vkQueueWaitIdle(graphicsQueue);
				vkFreeCommandBuffers(device, ret, pb.buf);
			}
			;
		} finally {
			copyInfo.free();
			beginInfo.free();
			allocInfo.free();
		}
	}

	private BufferInfo createBuffer(int size, int usage, int sharingMode) {
		long vertex_buffer;
		long buffer_memory;
		VkBufferCreateInfo info = VkBufferCreateInfo.calloc();
		VkMemoryRequirements req = VkMemoryRequirements.calloc();
		VkMemoryAllocateInfo allocInfo = VkMemoryAllocateInfo.calloc();
		try (TemporaryLongBuffer buf = new TemporaryLongBuffer();
		     TemporaryLongBuffer mem_buf = new TemporaryLongBuffer();
		     TemporaryPointerBuffer pbuf = new TemporaryPointerBuffer()) {
			info.sType(VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO);
			info.size(size);
			info.usage(usage);
			info.sharingMode(sharingMode);
			int ret = vkCreateBuffer(device, info, null, buf.buf);
			errorIfNecessary(ret, "Unable to allocate buffer: ");
			vertex_buffer = buf.value();

			vkGetBufferMemoryRequirements(device, vertex_buffer, req);
			allocInfo.sType(VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO);
			allocInfo.allocationSize(req.size());
			allocInfo.memoryTypeIndex(findMemoryType(req.memoryTypeBits(), VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT));
			ret = vkAllocateMemory(device, allocInfo, null, mem_buf.buf);
			errorIfNecessary(ret, "Unable to allocate vertex buffer memory: ");
			buffer_memory = mem_buf.value();
			ret = vkBindBufferMemory(device, vertex_buffer, buffer_memory, 0);
			errorIfNecessary(ret, "Unable to bind vertex buffer to memory: ");
		} finally {
			info.free();
			req.free();
			allocInfo.free();
		}

		return new BufferInfo(vertex_buffer, buffer_memory);
	}

	private final static class BufferInfo {
		public BufferInfo(long buffer, long bufferMemory) {
			this.buffer = buffer;
			this.bufferMemory = bufferMemory;
		}

		/**
		 *
		 */
		public void free(VkDevice device) {
			vkDestroyBuffer(device, buffer, null);
			vkFreeMemory(device, bufferMemory, null);
		}

		public long buffer;
		public long bufferMemory;
	}

	private final static class Vertices {
		public int indexCount;
		public int vertexCount;
		int stride;
		public ByteBuffer vertexData;
		public VkVertexInputBindingDescription.Buffer vertexBindingDescription;
		public VkVertexInputAttributeDescription.Buffer inputAttributeDescription;
		long vertexBuffer;
		long vertexBufferMemory;
		public long indexMemory;
		public long indexBuffer;

		public void free(VkDevice device) {
			vkDestroyBuffer(device, vertexBuffer, null);
			vertexBindingDescription.free();
			inputAttributeDescription.free();
			memFree(vertexData);
		}
	}

	private final static class ImageInfo {

		public long image;
		public long memory;
		public long imageView;
		public int format;

		public void free(VkDevice device) {
			vkDestroyImageView(device, imageView, null);
			vkDestroyImage(device, image, null);
			vkFreeMemory(device, memory, null);
		}

	}

	private ImageInfo createImage(int width, int height, int format, int tiling, int usage, int properties) {
		ImageInfo image = new ImageInfo();

		VkImageCreateInfo imageInfo = VkImageCreateInfo.calloc();
		VkMemoryRequirements memReqs = VkMemoryRequirements.calloc();
		VkMemoryAllocateInfo allocInfo = VkMemoryAllocateInfo.calloc();
		try {
			imageInfo.sType(VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO);
			imageInfo.imageType(VK_IMAGE_TYPE_2D);
			imageInfo.extent().width(width);
			imageInfo.extent().height(height);
			imageInfo.extent().depth(1);
			imageInfo.mipLevels(1);
			imageInfo.arrayLayers(1);
			imageInfo.format(format);
			imageInfo.tiling(tiling);
			imageInfo.initialLayout(VK_IMAGE_LAYOUT_UNDEFINED);
			imageInfo.usage(usage);
			imageInfo.samples(VK_SAMPLE_COUNT_1_BIT);
			imageInfo.sharingMode(VK_SHARING_MODE_EXCLUSIVE);
			try (TemporaryLongBuffer buf = new TemporaryLongBuffer()) {
				int ret = vkCreateImage(device, imageInfo, null, buf.buf);
				errorIfNecessary(ret, "Unable to allocate image: ");
				image.image = buf.value();
				image.format = format;
			}

			vkGetImageMemoryRequirements(device, image.image, memReqs);
			System.out.println("Image memory requirements: " + memReqs.size());
			
			allocInfo.sType(VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO);
			allocInfo.allocationSize(memReqs.size());
			allocInfo.memoryTypeIndex(findMemoryType(memReqs.memoryTypeBits(), properties));
			try (TemporaryLongBuffer buf = new TemporaryLongBuffer()) {
				int ret = vkAllocateMemory(device, allocInfo, null, buf.buf);
				errorIfNecessary(ret, "Unable to allocate memory");
				image.memory = buf.value();
			}

			vkBindImageMemory(device, image.image, image.memory, 0);
		} finally {
			allocInfo.free();
			memReqs.free();
			imageInfo.free();
		}

		return image;
	}

	private long createImageView(ImageInfo image, int aspectFlags) {
		VkImageViewCreateInfo info = VkImageViewCreateInfo.calloc();
		try (TemporaryLongBuffer buf = new TemporaryLongBuffer()) {
			info.sType(VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO);
			info.image(image.image);
			info.viewType(VK_IMAGE_VIEW_TYPE_2D);
			info.format(image.format);
			info.subresourceRange().aspectMask(aspectFlags);
			info.subresourceRange().baseMipLevel(0);
			info.subresourceRange().levelCount(1);
			info.subresourceRange().baseArrayLayer(0);
			info.subresourceRange().layerCount(1);

			int ret = vkCreateImageView(device, info, null, buf.buf);
			errorIfNecessary(ret, "Unable to create Image View: ");
			image.imageView = buf.value();
			return image.imageView;
		} finally {
			info.free();
		}
	}

	/**
	 * Allocates a single-use command buffer. Must be completed with endSingleTimeCommands
	 *
	 * @return
	 */
	VkCommandBuffer beginSingleTimeCommands() {
		VkCommandBufferAllocateInfo allocInfo = VkCommandBufferAllocateInfo.calloc();
		VkCommandBufferBeginInfo beginInfo = VkCommandBufferBeginInfo.calloc();
		try (TemporaryPointerBuffer pbuf = new TemporaryPointerBuffer()) {

			allocInfo.sType(VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO);
			allocInfo.level(VK_COMMAND_BUFFER_LEVEL_PRIMARY);
			allocInfo.commandPool(graphicsCommandPool);
			allocInfo.commandBufferCount(1);

			vkAllocateCommandBuffers(device, allocInfo, pbuf.buf);
			VkCommandBuffer commandBuffer = new VkCommandBuffer(pbuf.value(), device);

			beginInfo.sType(VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO);
			beginInfo.flags(VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT);
			vkBeginCommandBuffer(commandBuffer, beginInfo);

			return commandBuffer;
		} finally {
			beginInfo.free();
			allocInfo.free();
		}
	}

	/**
	 * Completes a single-time buffer and de-allocates it.
	 *
	 * @param commandBuffer
	 */
	private void endSingleTimeCommands(VkCommandBuffer commandBuffer) {
		vkEndCommandBuffer(commandBuffer);

		VkSubmitInfo submitInfo = VkSubmitInfo.calloc();
		try (TemporaryPointerBuffer pbuf = new TemporaryPointerBuffer(commandBuffer)) {
			submitInfo.sType(VK_STRUCTURE_TYPE_SUBMIT_INFO);
			submitInfo.pCommandBuffers(pbuf.buf);
			vkQueueSubmit(graphicsQueue, submitInfo, VK_NULL_HANDLE);
			vkQueueWaitIdle(graphicsQueue);
			vkFreeCommandBuffers(device, graphicsCommandPool, pbuf.buf);
		} finally {
			submitInfo.free();
		}
	}

	private void transitionImageLayout(ImageInfo image, int oldLayout, int newLayout) {
		VkCommandBuffer cmd = beginSingleTimeCommands();

		VkImageMemoryBarrier.Buffer barrier = VkImageMemoryBarrier.calloc(1);
		try {
			barrier.sType(VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER);
			barrier.oldLayout(oldLayout);
			barrier.newLayout(newLayout);
			barrier.srcQueueFamilyIndex(VK_QUEUE_FAMILY_IGNORED);
			barrier.dstQueueFamilyIndex(VK_QUEUE_FAMILY_IGNORED);
			barrier.image(image.image);
			if(newLayout == VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL) {
				if(hasStencilComponent(image.format))
					barrier.subresourceRange().aspectMask(VK_IMAGE_ASPECT_DEPTH_BIT | VK_IMAGE_ASPECT_STENCIL_BIT);
				else
					barrier.subresourceRange().aspectMask(VK_IMAGE_ASPECT_DEPTH_BIT);
			} else {
				barrier.subresourceRange().aspectMask(VK_IMAGE_ASPECT_COLOR_BIT);
			}
			barrier.subresourceRange().baseMipLevel(0);
			barrier.subresourceRange().levelCount(1);
			barrier.subresourceRange().baseArrayLayer(0);
			barrier.subresourceRange().layerCount(1);

			int source_stage, destination_stage;
			if(oldLayout == VK_IMAGE_LAYOUT_UNDEFINED && newLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL) {
				barrier.srcAccessMask(0);
				barrier.dstAccessMask(VK_ACCESS_TRANSFER_WRITE_BIT);
				source_stage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
				destination_stage = VK_PIPELINE_STAGE_TRANSFER_BIT;
			} else if(oldLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL && newLayout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL) {
				barrier.srcAccessMask(VK_ACCESS_TRANSFER_WRITE_BIT);
				barrier.dstAccessMask(VK_ACCESS_SHADER_READ_BIT);
				source_stage = VK_PIPELINE_STAGE_TRANSFER_BIT;
				destination_stage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
			} else if(oldLayout == VK_IMAGE_LAYOUT_UNDEFINED && newLayout == VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL) {
				barrier.srcAccessMask(0);
				barrier.dstAccessMask(VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT);
				source_stage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
				destination_stage = VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
			} else {
				throw new IllegalArgumentException("Unsupported layout transition");
			}

			vkCmdPipelineBarrier(cmd, source_stage, destination_stage, 0, null, null, barrier);

			endSingleTimeCommands(cmd);
		} finally {
			barrier.free();
		}
	}

}

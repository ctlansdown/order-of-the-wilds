import static org.lwjgl.system.MemoryUtil.memAllocInt;
import static org.lwjgl.system.MemoryUtil.memFree;

import java.nio.IntBuffer;

/**
 * 
 */

/**
 *
 * @author Christopher Lansdown
 */
public class TemporaryIntBuffer implements AutoCloseable {

	IntBuffer buf;
	
	public TemporaryIntBuffer() {
		buf = memAllocInt(1);
	}
	
	/**
	 * Allocates a buffer and initializes it with the specified value
	 * @param val
	 */
	public TemporaryIntBuffer(int val) {
		buf = memAllocInt(1);
		buf.put(val);
		buf.flip();
	}
	
	
	
	@Override
	public void close() {
		memFree(buf);
	}

	/**
	 *
	 * @return
	 */
	public int value() {
		return buf.get(0);
	}
}

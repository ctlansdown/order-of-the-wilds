import java.nio.LongBuffer;

import static org.lwjgl.system.MemoryUtil.*;

/**
 * 
 */

/**
 *
 * @author Christopher Lansdown
 */
public class TemporaryLongBuffer implements AutoCloseable {

	public LongBuffer buf;
	
	public TemporaryLongBuffer() {
		buf = memAllocLong(1);
	}
	
	public TemporaryLongBuffer(long val) {
		buf = memAllocLong(1);
		buf.put(val);
		buf.flip();
	}


	public long value() {
		return buf.get(0);
	}
	
	@Override
	public void close() {
		memFree(buf);
	}

}

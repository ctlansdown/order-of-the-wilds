import static org.lwjgl.system.MemoryUtil.memAllocPointer;
import static org.lwjgl.system.MemoryUtil.memFree;

import org.lwjgl.PointerBuffer;
import org.lwjgl.system.Pointer;
import org.lwjgl.vulkan.VkCommandBuffer;

/**
 * A wrapper around a PointerBuffer so that we can auto-close it
 * @author Christopher Lansdown
 */
public class TemporaryPointerBuffer implements AutoCloseable {

	public PointerBuffer buf;
	
	public TemporaryPointerBuffer() {
		buf = memAllocPointer(1);
	}
	
	public TemporaryPointerBuffer(int val) {
		buf = memAllocPointer(1);
		buf.put(val);
		buf.flip();
	}
	
	public TemporaryPointerBuffer(Pointer val) {
		buf = memAllocPointer(1);
		buf.put(val);
		buf.flip();
	}

	
	@Override
	public void close() {
		memFree(buf);
	}

	public long value() {
		return buf.get(0);
	}
}

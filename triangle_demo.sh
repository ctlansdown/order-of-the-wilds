#! /bin/sh
LWJGL=~/lwjgl
CLASSPATH="bin/"
for file in `ls -1 $LWJGL/*.jar`:
do
	CLASSPATH="$CLASSPATH:$file"
done

java -Dorg.lwjgl.util.Debug=true -cp $CLASSPATH TriangleDemo

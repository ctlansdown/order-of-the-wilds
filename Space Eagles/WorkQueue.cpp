/*
 * WorkQueue.cpp
 *
 *  Created on: Jul 20, 2018
 *      Author: ctl
 */

#include "WorkQueue.h"
#include <boost/thread.hpp>
#include <thread>

Job::Job(std::vector<std::function<void()>>& tasks) : functions(tasks) {
    completionCount = tasks.size();
}

void Job::waitForCompletion() {
    boost::unique_lock<boost::mutex> lock(mut);
    while(completionCount > 0)
        cond.wait(lock);
}


int Job::getTaskCount() {
    return functions.size();
}

WorkQueue::WorkQueue() {
    unsigned int thread_count = std::thread::hardware_concurrency()-1;
    if(thread_count < 1)
        thread_count = 1;
    threadCount = thread_count;
    for(int i = 0; i < thread_count; i++) {
        WorkQueue::QueueThread t(*this);
        boost::thread thread(t);
    }
}

WorkQueue::WorkQueue(int thread_count)  {
    threadCount = thread_count;
    for(int i = 0; i < thread_count; i++) {
        WorkQueue::QueueThread t(*this);
        boost::thread thread(t);
    }
}

/**
 * Submit a job to the queue
 *
 * We grab the lock and add the tasks to the queue, then notify the threads in case they're asleep
 */
void WorkQueue::submit(Job& job) {
    {
        boost::unique_lock<boost::mutex> lock(mutex);
        for(auto task : job.getFunctions()) {
            queue.push_back(task);
        }
    }
    if(job.getTaskCount() == 1)
        cond.notify_one();
    else
        cond.notify_all();
}

void WorkQueue::submit(std::function<void()> f) {
    {
        boost::unique_lock<boost::mutex> lock(mutex);
        queue.push_back(f);
    }
    cond.notify_one();
}

void WorkQueue::QueueThread::operator ()() {
    while(true) {
        std::function<void()> function;
        {
            boost::unique_lock<boost::mutex> lock(workQueue.mutex);
            if(!workQueue.queue.empty()) {
                function = workQueue.queue.front();
                workQueue.queue.pop_front();
            } else {
                workQueue.cond.wait(lock);
                continue;
            }
        }
        printf("WorkQueue::QueueThread doing work.\n");
        function();
        printf("WorkQueue::QueueThread finished work.\n");
    }
}

int WorkQueue::getThreadCount() {
    return threadCount;
}

#include <vector>
#include <functional>

void test_work_queue() {
    WorkQueue wq(4);
    std::vector<std::function<void()>> tasks;
    for(int i = 0; i < 32; i++) {
        std::function<void()> func = [i] () {printf("Hello world %d\n", i); usleep(500000);};
        tasks.push_back(func);
    }
    Job job(tasks);
    wq.submit(job);

    job.waitForCompletion();
    printf("Done!\n");

    wq.submit(tasks);

    sleep(5);
    exit(0);
}

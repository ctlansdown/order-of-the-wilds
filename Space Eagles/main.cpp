#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <unistd.h>
#include <math.h>

#include <chrono>
#include <iostream>
#include <fstream>
#include <stdexcept>
#include <algorithm>
#include <chrono>
#include <vector>
#include <cstring>
#include <array>
#include <set>

#include "Terrain.h"
#include "TerrainGrid.h"
#include "VulkanBuffer.h"
#include "WorkQueue.h"
#include "VulkanUtils.h"

const uint32_t WIDTH=1920;
const uint32_t HEIGHT=1080;

#define PI 3.1415926535897932

static std::vector<char> readFile(const std::string& filename);
void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods);
void mouseCallback(GLFWwindow* window, double xpos, double ypos);

double horizontalCameraAngle = 0;
double verticalCameraAngle = -PI/4;
double speed = 0;
glm::vec3 cameraPosition(500.0f, 500.0f, 1000.0f);
bool takeScreenshot = false;

//#ifdef NDEBUG
//    const bool enableValidationLayers = false;
//#else
//    const bool enableValidationLayers = true;
//#endif // NDEBUG
const bool enableValidationLayers = false;
const std::vector<const char*> validationLayers = { "VK_LAYER_LUNARG_standard_validation" };
const std::vector<const char*> deviceExtensions = { VK_KHR_SWAPCHAIN_EXTENSION_NAME };

static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(VkDebugReportFlagsEXT flags, VkDebugReportObjectTypeEXT objType, uint64_t obj, size_t location, int32_t code, const char* layerPrefix, const char*msg, void* userData) {
    std::cerr << "validation layer: " << msg << std::endl;
    return VK_FALSE;
}


void err(VkResult result, const char* message) {
    if(result != VK_SUCCESS) // FIXME: find out the error and print it
        throw std::runtime_error(message);
}

/**
 * The work queue on which to execute computational tasks
 */
WorkQueue workQueue;

struct VulkanQueueIndices {
    uint32_t graphics = -1;
    uint32_t present = -1;

    bool isComplete() {
        return graphics >= 0 && present >= 0;
    }
};
VulkanQueueIndices getQueues(VkPhysicalDevice device, VkSurfaceKHR surface);

struct SwapChainSupportDetails {
    VkSurfaceCapabilitiesKHR capabilities;
    std::vector<VkSurfaceFormatKHR>  formats;
    std::vector<VkPresentModeKHR> presentModes;

    VkSurfaceFormatKHR chooseSwapSurfaceFormat() {
        if(formats.size() == 1 && formats[0].format == VK_FORMAT_UNDEFINED)
            return {VK_FORMAT_B8G8R8A8_UNORM, VK_COLOR_SPACE_SRGB_NONLINEAR_KHR};
        for(const VkSurfaceFormatKHR& fmt : formats) {
            if(fmt.format == VK_FORMAT_B8G8R8A8_UNORM && fmt.colorSpace == VK_COLORSPACE_SRGB_NONLINEAR_KHR)
                return fmt;
        }
        for(const VkSurfaceFormatKHR& fmt : formats) {
            if(fmt.colorSpace == VK_COLORSPACE_SRGB_NONLINEAR_KHR)
                return fmt;
        }
        return formats[0];
    }

    VkPresentModeKHR choosePresentMode() {
        return VK_PRESENT_MODE_FIFO_KHR;
    }

    VkExtent2D chooseSwapExtent() {
        if(capabilities.currentExtent.width != std::numeric_limits<uint32_t>::max()) {
            return capabilities.currentExtent;
        } else {
            VkExtent2D actual_extent = {WIDTH, HEIGHT};
            actual_extent.width = std::max(capabilities.minImageExtent.width, std::min(capabilities.maxImageExtent.width, WIDTH));
            actual_extent.height = std::max(capabilities.minImageExtent.height, std::min(capabilities.maxImageExtent.height, HEIGHT));
            return actual_extent;
        }
    }
};

struct UniformBufferObject {
    glm::mat4 model;
    glm::mat4 view;
    glm::mat4 proj;
};

class SpaceEagles {

    Terrain terrain;
    GLFWwindow* window;
    VkInstance instance;
    VkDebugReportCallbackEXT debugFunction;
    VkPhysicalDevice physicalDevice = VK_NULL_HANDLE;
    VkDevice device;
    // VkQueue graphicsQueue;
    // VkQueue presentQueue;
    QueueLibrary* queueLibrary;
    CommandPoolLibrary* commandPoolLibrary;
    VkSurfaceKHR surface;
    VkSwapchainKHR swapchain;
    VkFormat swapChainImageFormat;
    VkExtent2D swapChainExtent;
    std::vector<VkImage> swapChainImages;
    std::vector<VkImageView> swapChainImageViews;
    VkRenderPass renderPass;
    VkPipelineLayout pipelineLayout;
    VkDescriptorSetLayout descriptorSetLayout;
    VkPipeline graphicsPipeline;
    std::vector<VkFramebuffer> swapChainFrameBuffers;
    VkCommandPool commandPool;
    VkCommandPool transientCommandPool;
    std::vector<VkCommandBuffer> commandBuffers;
    VkSemaphore imageAvailableSemaphore;
    VkSemaphore renderFinishedSemaphore;
    VkDescriptorPool descriptorPool;
    std::vector<VkDescriptorSet> descriptorSets;

    /* Terrain */
    // vector<Vertex>* vertices;
    // vector<uint32_t>* indices;
    VulkanBuffer terrainBuffer;
    VulkanBuffer terrainIndexBuffer;
    std::vector<VulkanBuffer> uniformBuffers;



    VkImage depthImage;
    VkDeviceMemory depthImageMemory;
    VkImageView depthImageView;

    public:
    SpaceEagles(uint64_t seed) : terrain(seed) {
        initVulkan();
        printf("Initialization completed.\n==========================================================================================================================\n");
    }

    void initVulkan() {
        initWindow();
        createVulkanInstance();
        setupDebugCallback();
        createSurface();
        selectPhysicalDevice();
        queueLibrary = new QueueLibrary(physicalDevice, surface);
        createLogicalDevice();
        commandPoolLibrary = new CommandPoolLibrary(queueLibrary, device);
        commandPoolLibrary->createCommandPools(workQueue.getThreadCount(), true);
        createCommandPool();
        createSwapChain();
        createDepthResources();
        createImageViews();
        createRenderPass();
        createDescriptorSetLayout();
        createGraphicsPipeline();
        createFrameBuffers();
        createTerrain();
        createUniformBuffers();
        createDescriptorPool();
        createDescriptorSet();
        recordCommandBuffers();
        initSemaphores();
    }

    void mainLoop() {
        while(!glfwWindowShouldClose(window)) {
            glfwPollEvents();
            if(takeScreenshot) {
                takeScreenshot = false;
                screenshot();
            }
            drawFrame();
            {
                PresentQueueLoan loan(queueLibrary);
                vkQueueWaitIdle(*loan.queue);
            }
            // usleep(100);
        }
    }

    void updateUniformBuffer(int imageIndex) {
        static auto startTime = std::chrono::high_resolution_clock::now();
        static auto lastTime = startTime;
        auto currentTime = std::chrono::high_resolution_clock::now();
        float time = std::chrono::duration<float, std::chrono::seconds::period>(currentTime - startTime).count();
        double time_diff = std::chrono::duration<float, std::chrono::seconds::period>(currentTime - lastTime).count();
        lastTime = currentTime;
        UniformBufferObject ubo = {};
        /* Model */
        // ubo.model = glm::rotate(glm::mat4(1.0f), time * glm::radians(90.0f), glm::vec3(0.0f, 0.0f, 1.0f));
        ubo.model = glm::mat4(1.0f);

        /* View */
        glm::vec3 look(0.0f, 0.0f, 0.0f);

        look[0] = cos(verticalCameraAngle)*sin(horizontalCameraAngle);
        look[1] = cos(verticalCameraAngle) * cos(horizontalCameraAngle);
        look[2] = sin(verticalCameraAngle);

        // printf("look: %.3f / %.3f => %f,%f,%f\n", horizontalCameraAngle, verticalCameraAngle, look[0], look[1], look[2]);

        glm::vec3 up(0.0f, 0.0f, 1.0f);

        /* Do the movement for the amount of time which has elapsed (This is game logic) */
        for(int i = 0; i < 3; i++)
            cameraPosition[i] += time_diff*speed*look[i];
        float min_altitude = terrain.getHeight((double)cameraPosition[0], (double)cameraPosition[1]);
        if(cameraPosition[2] < min_altitude + 5)
            cameraPosition[2] = min_altitude + 5;
        printf("\rcamera position: (%.1f,%.1f,%.1f) looking at (%.2f,%.2f,%.2f) (min altitude = %f)                   ", cameraPosition[0], cameraPosition[1], cameraPosition[2], look[0], look[1], look[2], min_altitude);

        // printf("time == %.3f, speed == %.3f, product == %.3f\n", time_diff, speed, time_diff*speed);

        ubo.view = glm::lookAt(cameraPosition, cameraPosition + look, up);


        /* Projection */
        ubo.proj = glm::perspective(glm::radians(45.0f), swapChainExtent.width / (float)swapChainExtent.height, 0.1f, 10000.0f);
        ubo.proj[1][1] *= -1;

        /* Copy the data over */
        void* data;
        vkMapMemory(device, uniformBuffers[imageIndex].memory, 0, sizeof(ubo), 0, &data);
        memcpy(data, &ubo, sizeof(ubo));
        vkUnmapMemory(device, uniformBuffers[imageIndex].memory);
    }

    void cleanup() {
        vkDestroySemaphore(device, imageAvailableSemaphore, nullptr);
        vkDestroySemaphore(device, renderFinishedSemaphore, nullptr);
        for(auto framebuffer : swapChainFrameBuffers)
            vkDestroyFramebuffer(device, framebuffer, nullptr);
        vkDestroyDescriptorPool(device, descriptorPool, nullptr);
        vkDestroyPipeline(device, graphicsPipeline, nullptr);
        vkDestroyPipelineLayout(device, pipelineLayout, nullptr);
        vkDestroyRenderPass(device, renderPass, nullptr);
        for(auto imageView : swapChainImageViews)
            vkDestroyImageView(device, imageView, nullptr);
        vkDestroySwapchainKHR(device, swapchain, nullptr);
        vkDestroyCommandPool(device, commandPool, nullptr);
        vkDestroySurfaceKHR(instance, surface, nullptr);
        vkDestroyDevice(device, nullptr);
        vkDestroyInstance(instance, nullptr);
        glfwDestroyWindow(window);
        glfwTerminate();
    }

    void drawFrame() {
        uint32_t imageIndex;
        vkAcquireNextImageKHR(device, swapchain, std::numeric_limits<uint64_t>::max(), imageAvailableSemaphore, VK_NULL_HANDLE, &imageIndex);

        updateUniformBuffer(imageIndex);

        VkSubmitInfo submitInfo = {};
        submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;

        VkSemaphore waitSemaphores[] = { imageAvailableSemaphore };
        VkPipelineStageFlags waitStages[] = {VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT};
        submitInfo.waitSemaphoreCount = 1;
        submitInfo.pWaitSemaphores = waitSemaphores;
        submitInfo.pWaitDstStageMask = waitStages;
        submitInfo.commandBufferCount = 1;
        submitInfo.pCommandBuffers = &commandBuffers[imageIndex];

        VkSemaphore signalSemaphores[] = { renderFinishedSemaphore };
        submitInfo.signalSemaphoreCount = 1;
        submitInfo.pSignalSemaphores = signalSemaphores;

        {
            GraphicsQueueLoan loan(queueLibrary);
            err(vkQueueSubmit(*loan.queue, 1, &submitInfo, VK_NULL_HANDLE), "\n\n\nUnable to submit draw command buffer!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
        }

        VkPresentInfoKHR presentInfo = {};
        presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
        presentInfo.waitSemaphoreCount = 1;
        presentInfo.pWaitSemaphores = signalSemaphores;

        VkSwapchainKHR swapChains[] = { swapchain };
        presentInfo.swapchainCount = 1;
        presentInfo.pSwapchains = swapChains;
        presentInfo.pImageIndices = &imageIndex;
        presentInfo.pResults = nullptr;

        {
            PresentQueueLoan loan(queueLibrary);
            vkQueuePresentKHR(*loan.queue, &presentInfo);
        }
    }

    void screenshot() {
        printf("screenshot()\n");
    }

    void createUniformBuffers() {
        VkDeviceSize bufferSize = sizeof(UniformBufferObject);
        uniformBuffers.resize(swapChainImages.size());
        for(int i = 0; i < swapChainImages.size(); i++)
            createVulkanBuffer(bufferSize, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
                               uniformBuffers[i].buffer, uniformBuffers[i].memory);
    }

    void createDescriptorPool() {
        VkDescriptorPoolSize pool_size = {};
        pool_size.type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
        pool_size.descriptorCount = static_cast<uint32_t>(swapChainImages.size());

        VkDescriptorPoolCreateInfo pool_info = {};
        pool_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
        pool_info.poolSizeCount = 1;
        pool_info.pPoolSizes = &pool_size;
        pool_info.maxSets = static_cast<uint32_t>(swapChainImages.size());

        err(vkCreateDescriptorPool(device, &pool_info, nullptr, &descriptorPool), "Unable to create descriptor pool");
    }

    void createDescriptorSet() {
        std::vector<VkDescriptorSetLayout> layouts(swapChainImages.size(), descriptorSetLayout);
        VkDescriptorSetAllocateInfo alloc_info = {};
        alloc_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
        alloc_info.descriptorPool = descriptorPool;
        alloc_info.descriptorSetCount = static_cast<uint32_t>(swapChainImages.size());
        alloc_info.pSetLayouts = layouts.data();

        descriptorSets.resize(swapChainImages.size());
        err(vkAllocateDescriptorSets(device, &alloc_info, &descriptorSets[0]), "Unable to allocate descriptor sets");

        for(size_t i = 0; i < swapChainImages.size(); i++) {
            VkDescriptorBufferInfo bufferInfo = {};
            bufferInfo.buffer = uniformBuffers[i].buffer;
            bufferInfo.offset = 0;
            bufferInfo.range = sizeof(UniformBufferObject);

            VkWriteDescriptorSet descriptorWrite = {};
            descriptorWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
            descriptorWrite.dstSet = descriptorSets[i];
            descriptorWrite.dstBinding = 0;
            descriptorWrite.dstArrayElement = 0;
            descriptorWrite.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
            descriptorWrite.descriptorCount = 1;
            descriptorWrite.pBufferInfo = &bufferInfo;
            descriptorWrite.pImageInfo = nullptr;
            descriptorWrite.pTexelBufferView = nullptr;

            vkUpdateDescriptorSets(device, 1, &descriptorWrite, 0, nullptr);
        }
    }

    VkFormat findSupportedFormat(const std::vector<VkFormat>& candidates, VkImageTiling tiling, VkFormatFeatureFlags features) {
        for(VkFormat format : candidates) {
            VkFormatProperties props;
            vkGetPhysicalDeviceFormatProperties(physicalDevice, format, &props);
            if(tiling == VK_IMAGE_TILING_LINEAR && (props.linearTilingFeatures & features) == features)
                return format;
            else if(tiling == VK_IMAGE_TILING_OPTIMAL && (props.optimalTilingFeatures & features) == features)
                return format;
        }
        throw std::runtime_error("failed to find a supported format.");
    }

    VkFormat findDepthFormat() {
        return findSupportedFormat({VK_FORMAT_D32_SFLOAT, VK_FORMAT_D32_SFLOAT_S8_UINT, VK_FORMAT_D24_UNORM_S8_UINT}, VK_IMAGE_TILING_OPTIMAL, VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT);
    }

    bool hasStencilComponent(VkFormat format) {
        return format == VK_FORMAT_D32_SFLOAT_S8_UINT || format == VK_FORMAT_D24_UNORM_S8_UINT;
    }

    void createDepthResources() {
        VkFormat depthFormat = findDepthFormat();

        createImage(swapChainExtent.width, swapChainExtent.height, depthFormat, VK_IMAGE_TILING_OPTIMAL, VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT,
                    VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, depthImage, depthImageMemory);
        depthImageView = createImageView(depthImage, depthFormat, VK_IMAGE_ASPECT_DEPTH_BIT);
        transitionImageLayout(depthImage, depthFormat, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL);

    }

    void createTerrain() {
        VulkanJobGroup jobGroup(*commandPoolLibrary, VK_QUEUE_TRANSFER_BIT, true);

        printf("createTerrain()\n");
        terrainGrid.distance = 1;
        terrainGrid.count = (2 * terrainGrid.distance + 1) * (2 * terrainGrid.distance + 1);
        terrainGrid.seamCount = (2 * terrainGrid.distance + 1) * (2 * terrainGrid.distance) * 2;
        terrainGrid.tiles.resize(terrainGrid.count);
        terrainGrid.indices.resize(terrainGrid.count);
        terrainGrid.seams.resize(terrainGrid.seamCount);
        printf("distance == %d, count == %d, seamCount == %d\n", terrainGrid.distance, terrainGrid.count, terrainGrid.seamCount);

        createVulkanBuffer(sizeof(Vertex) * 1000000 * terrainGrid.count, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
                VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, terrainBuffer.buffer, terrainBuffer.memory);
        printf("created the terrain buffer\n");
        // note: the *4 is for 4 sides
        uint32_t indicesCount = terrainGrid.count * 4 * terrain.getTileIndexBufferElementCount();
        printf("index element count == %d\n", indicesCount);
        uint32_t seam_size = terrain.getTileIndexBufferSeamElementCount() * 4;
        uint32_t seamIndicesCount = terrain.getTileIndexBufferSeamElementCount() * terrainGrid.seamCount;
        terrainGrid.indicesCount = indicesCount + seamIndicesCount;
        createVulkanBuffer(indicesCount * 4 + seamIndicesCount * 4, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT,
                VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, terrainIndexBuffer.buffer, terrainIndexBuffer.memory);
        printf("created the index buffer\n");

        /* Initialize the tiles */
        int pos = 0;
        int tileIndex = 0;
        uint32_t index_buffer_offset = 0;
        printf("generating the data: distance == %d\n", terrainGrid.distance);
        vector<function<void()>> tasks;
        for(int i = -terrainGrid.distance; i <= terrainGrid.distance; i++) {
            for(int j = -terrainGrid.distance; j <= terrainGrid.distance; j++) {
                function<void()> f = [=, &jobGroup] () {
                    printf("Creating the VulkanJob\n");
                    VulkanJob job(jobGroup);
                    printf("VulkanJob created.\n");
                    auto vertices = terrain.getVertexData(i * 1000, j * 1000);
                    // stageDataAndCopyInto(vertices->data(), vertices->size() * sizeof(Vertex), tileIndex * vertices->size() * sizeof(Vertex), terrainBuffer);
                    job.stageDataAndCopyInto(vertices->data(), vertices->size() * sizeof(Vertex), tileIndex * vertices->size() * sizeof(Vertex), terrainBuffer);
                    auto indices = terrain.getIndexData(i * 1000, j * 1000);
                    for(int i = 0; i < indices->size(); i++)
                        (*indices)[i] += pos;
                    // stageDataAndCopyInto(indices->data(), indices->size() * 4, index_buffer_offset, terrainIndexBuffer);
                    job.stageDataAndCopyInto(indices->data(), indices->size() * 4, index_buffer_offset, terrainIndexBuffer);

                    job.submitBuffers();

                    terrainGrid.tiles[tileIndex].length = vertices->size() * sizeof(Vertex);
                    terrainGrid.tiles[tileIndex].lowerLeft = point(i * 1000, j * 1000);
                    terrainGrid.tiles[tileIndex].bufferOffset = tileIndex * vertices->size() * sizeof(Vertex);
                    terrainGrid.tiles[tileIndex].vertexOffset = pos;

                    terrainGrid.indices[tileIndex].length = indices->size() * 4;
                    terrainGrid.indices[tileIndex].lowerLeft = point(i * 1000, j * 1000);
                    terrainGrid.indices[tileIndex].bufferOffset = index_buffer_offset;
                };
                // f();
                tasks.push_back(f);

                tileIndex++;
                pos += 1000000;
                index_buffer_offset += terrain.getTileIndexBufferElementCount() * 4;
            }
        }
        Job job(tasks);
        workQueue.submit(job);
        job.waitForCompletion();
        tasks.clear();
        printf("finished generating the data.\n");

        /* Initialize the seams */

        // TODO: make these able to be run at the same time and put them onto the work queue

        int seam_index = 0;
        int grid_length = 2 * terrainGrid.distance + 1;
        for(int i = 0; i < terrainGrid.count; i++) {
            auto& tile = terrainGrid.tiles[i];
            // Seam to the right
            if(tile.lowerLeft.x < 1000) {
                function<void()> f = [=, &jobGroup] () {
                    VulkanJob job(jobGroup);
                    vector<uint32_t> seam;
                    terrainGrid.seams[seam_index].lowerLeft = tile.lowerLeft;
                    terrainGrid.seams[seam_index].length = seam_size;
                    terrainGrid.seams[seam_index].bufferOffset = index_buffer_offset;
                    int right_index = terrainGrid.pointToGridIndex(tile.lowerLeft.x + 1000, tile.lowerLeft.y);
                    auto& right = terrainGrid.tiles[right_index];
                    terrain.generateSeamData(tile.lowerLeft.x, tile.lowerLeft.y, tile.vertexOffset, right.lowerLeft.x, right.lowerLeft.y, right.vertexOffset, seam);
                    job.stageDataAndCopyInto(seam.data(), seam_size, index_buffer_offset, terrainIndexBuffer);
                };
                //f();
                tasks.push_back(f);
                index_buffer_offset += seam_size;
                seam_index++;
            }

            // Seam to the bottom
            if(tile.lowerLeft.y > -1000) {
                function<void()> f = [=, &jobGroup] () {
                    VulkanJob job(jobGroup);
                    vector<uint32_t> seam;
                    terrainGrid.seams[seam_index].lowerLeft = tile.lowerLeft;
                    terrainGrid.seams[seam_index].length = seam_size;
                    terrainGrid.seams[seam_index].bufferOffset = index_buffer_offset;
                    int bottom_index = terrainGrid.pointToGridIndex(tile.lowerLeft.x, tile.lowerLeft.y - 1000);
                    auto& bottom = terrainGrid.tiles[bottom_index];
                    terrain.generateSeamData(tile.lowerLeft.x, tile.lowerLeft.y, tile.vertexOffset, bottom.lowerLeft.x, bottom.lowerLeft.y, bottom.vertexOffset,
                            seam);
                    job.stageDataAndCopyInto(seam.data(), seam_size, index_buffer_offset, terrainIndexBuffer);
                };
                //f();
                tasks.push_back(f);
                index_buffer_offset += seam_size;
                seam_index++;
            }
        }

        Job job2(tasks);
        workQueue.submit(job2);
        job2.waitForCompletion();

        printf("fencing the commands.\n");
        jobGroup.fenceCommands();
        printf("freeing the resources.\n");
        jobGroup.freeResources();
    }

    void createImage(uint32_t width, uint32_t height, VkFormat format, VkImageTiling tiling, VkImageUsageFlags usage, VkMemoryPropertyFlags properties,
                     VkImage& image, VkDeviceMemory& imageMemory) {
        if(width == 0)
            throw std::invalid_argument("image width must be greater than zero!");
        if(height == 0)
            throw std::invalid_argument("image height must be greater than zero!");
        VkImageCreateInfo info = {};
        info.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
        info.imageType = VK_IMAGE_TYPE_2D;
        info.extent.width = width;
        info.extent.height = height;
        info.extent.depth = 1;
        info.mipLevels = 1;
        info.arrayLayers = 1;
        info.format = format;
        info.tiling = tiling;
        info.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
        info.usage = usage;
        info.samples = VK_SAMPLE_COUNT_1_BIT;
        info.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

        err(vkCreateImage(device, &info, nullptr, &image), "Unable to create image.");

        VkMemoryRequirements memRequirements;
        vkGetImageMemoryRequirements(device, image, &memRequirements);

        VkMemoryAllocateInfo allocInfo = {};
        allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
        allocInfo.allocationSize = memRequirements.size;
        allocInfo.memoryTypeIndex = findMemoryType(memRequirements.memoryTypeBits, properties);

        err(vkAllocateMemory(device, &allocInfo, nullptr, &imageMemory), "Unable to allocate image memory.");

        vkBindImageMemory(device, image, imageMemory, 0);
    }

    void transitionImageLayout(VkImage image, VkFormat format, VkImageLayout oldLayout, VkImageLayout newLayout) {
        VkCommandBuffer command_buffer = beginSingleTimeCommands();

        VkImageMemoryBarrier barrier = {};
        barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
        barrier.oldLayout = oldLayout;
        barrier.newLayout = newLayout;
        barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
        barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
        barrier.image = image;
        if(newLayout == VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL) {
            barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT;
            if(hasStencilComponent(format))
                barrier.subresourceRange.aspectMask |= VK_IMAGE_ASPECT_STENCIL_BIT;
        } else {
            barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        }
        barrier.subresourceRange.baseMipLevel = 0;
        barrier.subresourceRange.levelCount = 1;
        barrier.subresourceRange.baseArrayLayer = 0;
        barrier.subresourceRange.layerCount = 1;

        VkPipelineStageFlags sourceStage;
        VkPipelineStageFlags destinationStage;

        if(oldLayout == VK_IMAGE_LAYOUT_UNDEFINED && newLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL) {
            barrier.srcAccessMask = 0;
            barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;

            sourceStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
            destinationStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
        } else if(oldLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL && newLayout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL) {
            barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
            barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

            sourceStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
            destinationStage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
        } else if(oldLayout == VK_IMAGE_LAYOUT_UNDEFINED && newLayout == VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL) {
            barrier.srcAccessMask = 0;
            barrier.dstAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;

            sourceStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
            destinationStage = VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
        } else {
            throw std::invalid_argument("Unsupported layout transition");
        }

        vkCmdPipelineBarrier(command_buffer, sourceStage, destinationStage, 0, 0, nullptr, 0, nullptr, 1, &barrier);

        endSingleTimeCommands(command_buffer);
    }

    VkCommandBuffer beginSingleTimeCommands() {
        VkCommandBufferAllocateInfo info = {};
        info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
        info.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
        info.commandPool = transientCommandPool;
        info.commandBufferCount = 1;

        VkCommandBuffer commandBuffer;
        vkAllocateCommandBuffers(device, &info, &commandBuffer);

        VkCommandBufferBeginInfo begin = {};
        begin.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
        begin.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

        vkBeginCommandBuffer(commandBuffer, &begin);
        return commandBuffer;
    }

    void endSingleTimeCommands(VkCommandBuffer commandBuffer) {
        vkEndCommandBuffer(commandBuffer);

        VkSubmitInfo submitInfo = {};
        submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
        submitInfo.commandBufferCount = 1;
        submitInfo.pCommandBuffers = &commandBuffer;

        {
            GraphicsQueueLoan loan(queueLibrary);
            vkQueueSubmit(*loan.queue, 1, &submitInfo, VK_NULL_HANDLE);
            vkQueueWaitIdle(*loan.queue);
        }
        vkFreeCommandBuffers(device, transientCommandPool, 1, &commandBuffer);
    }


    void createDescriptorSetLayout() {
        VkDescriptorSetLayoutBinding uboLayoutBinding = {};
        uboLayoutBinding.binding = 0;
        uboLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
        uboLayoutBinding.descriptorCount = 1;
        uboLayoutBinding.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
        uboLayoutBinding.pImmutableSamplers = nullptr;

        VkDescriptorSetLayoutCreateInfo layoutInfo = {};
        layoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
        layoutInfo.bindingCount = 1;
        layoutInfo.pBindings = &uboLayoutBinding;

        err(vkCreateDescriptorSetLayout(device, &layoutInfo, nullptr, &descriptorSetLayout), "Unable to create the descriptor set layout.");


    }

    void createVulkanBuffer(VkDeviceSize size, VkBufferUsageFlags usage, VkMemoryPropertyFlags properties, VkBuffer& buffer, VkDeviceMemory& bufferMemory) {
        VkBufferCreateInfo bufferInfo = {};
        bufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
        bufferInfo.size = size;
        bufferInfo.usage = usage;
        bufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
        err(vkCreateBuffer(device, &bufferInfo, nullptr, &buffer), "Failed to create vertex buffer.");

        VkMemoryRequirements memRequirements;
        vkGetBufferMemoryRequirements(device, buffer, &memRequirements);

        VkMemoryAllocateInfo allocInfo = {};
        allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
        allocInfo.allocationSize = memRequirements.size;
        allocInfo.memoryTypeIndex = findMemoryType(memRequirements.memoryTypeBits, properties);

        err(vkAllocateMemory(device, &allocInfo, nullptr, &bufferMemory), "Unable to allocate device memory");
        // printf("allocated memory.\n");
        vkBindBufferMemory(device, buffer, bufferMemory, 0);
        // printf("bound the memory.\n");
    }

    void copyBuffer(VkBuffer srcBuffer, VkBuffer dstBuffer, VkDeviceSize targetOffset, VkDeviceSize size) {
        VkCommandBuffer commandBuffer = beginSingleTimeCommands();

        VkBufferCopy copyRegion = {};
        copyRegion.srcOffset = 0;
        copyRegion.dstOffset = targetOffset;
        copyRegion.size = size;
        vkCmdCopyBuffer(commandBuffer, srcBuffer, dstBuffer, 1, &copyRegion);

        endSingleTimeCommands(commandBuffer);
    }


    void stageDataAndCopyInto(void* data, uint32_t size, uint32_t offset, VulkanBuffer& destination) {
        VkBuffer stagingBuffer;
        VkDeviceMemory stagingBufferMemory;
        createVulkanBuffer(size, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, stagingBuffer, stagingBufferMemory);
        void* mapped_memory;
        vkMapMemory(device, stagingBufferMemory, 0, size, 0, &mapped_memory);
        memcpy(mapped_memory, data, (size_t)size);
        vkUnmapMemory(device, stagingBufferMemory);

        copyBuffer(stagingBuffer, destination.buffer, offset, size);

        vkDestroyBuffer(device, stagingBuffer, nullptr);
        vkFreeMemory(device, stagingBufferMemory, nullptr);
    }

    /**
     * Creates a vertex buffer of the specified size, sticking into buffer
     * @param size the size (in bytes) of the buffer
     * @param buffer the buffer into which to store the vertex buffer
     * @param bufferMemory
     */
    void createBuffer(void* data, int size, int usage, VulkanBuffer& buffer) {
        printf("createVertexBuffer: size == %d\n", size);
        createVulkanBuffer(size, usage, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, buffer.buffer, buffer.memory);
        stageDataAndCopyInto(data, size, 0, buffer);
    }


    uint32_t findMemoryType(uint32_t typeFilter, VkMemoryPropertyFlags properties) {
        VkPhysicalDeviceMemoryProperties memProperties;
        vkGetPhysicalDeviceMemoryProperties(physicalDevice, &memProperties);
        for(uint32_t i = 0; i < memProperties.memoryTypeCount; i++)
            if((typeFilter & (1<< i)) && (memProperties.memoryTypes[i].propertyFlags & properties) == properties)
                return i;
        throw std::runtime_error("failed to find suitable memory type!");
    }

    void initSemaphores() {
        VkSemaphoreCreateInfo semaphoreInfo = {};
        semaphoreInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
        err(vkCreateSemaphore(device, &semaphoreInfo, nullptr, &imageAvailableSemaphore), "Unable to create semaphore");
        err(vkCreateSemaphore(device, &semaphoreInfo, nullptr, &renderFinishedSemaphore), "Unable to create semaphore");
    }

    void recordCommandBuffers() {
        commandBuffers.resize(swapChainFrameBuffers.size());
        VkCommandBufferAllocateInfo allocInfo = {};
        allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
        allocInfo.commandPool = commandPool;
        allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
        allocInfo.commandBufferCount = (uint32_t)commandBuffers.size();
        err(vkAllocateCommandBuffers(device, &allocInfo, commandBuffers.data()), "Unable to allocate Command Buffers");

        for(size_t i = 0; i < commandBuffers.size(); i++) {
            VkCommandBufferBeginInfo beginInfo = {};
            beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
            beginInfo.flags = VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT;
            beginInfo.pInheritanceInfo = nullptr;
            vkBeginCommandBuffer(commandBuffers[i], &beginInfo);

            VkRenderPassBeginInfo renderPassInfo = {};
            renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
            renderPassInfo.renderPass = renderPass;
            renderPassInfo.framebuffer = swapChainFrameBuffers[i];
            renderPassInfo.renderArea.offset = {0, 0};
            renderPassInfo.renderArea.extent = swapChainExtent;

            std::array<VkClearValue, 2> clearValues = {};
            clearValues[0].color = {0.0f, 0.0f, 0.0f, 1.0f};
            clearValues[1].depthStencil = {1.0f, 0};
            renderPassInfo.clearValueCount = static_cast<uint32_t>(clearValues.size());
            renderPassInfo.pClearValues = clearValues.data();
            vkCmdBeginRenderPass(commandBuffers[i], &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);

            vkCmdBindPipeline(commandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, graphicsPipeline);

            VkBuffer vertexBuffers[] = {terrainBuffer.buffer};
            VkDeviceSize offsets[] = {0};
            vkCmdBindVertexBuffers(commandBuffers[i], 0, 1, vertexBuffers, offsets);
            vkCmdBindIndexBuffer(commandBuffers[i], terrainIndexBuffer.buffer, 0, VK_INDEX_TYPE_UINT32);
            vkCmdBindDescriptorSets(commandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, pipelineLayout, 0, 1, &descriptorSets[i], 0, nullptr);

            printf("recording commands: indicesCount == %d\n", terrainGrid.indicesCount);
            vkCmdDrawIndexed(commandBuffers[i], terrainGrid.indicesCount, 1, 0, 0, 0);

            vkCmdEndRenderPass(commandBuffers[i]);

            err(vkEndCommandBuffer(commandBuffers[i]), "Couldn't end the command buffers.");
        }

    }

    void createCommandPool() {
        VulkanQueueIndices indices = getQueues(physicalDevice, surface);
        VkCommandPoolCreateInfo poolInfo = {};
        poolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
        poolInfo.queueFamilyIndex = indices.graphics;
        poolInfo.flags = 0;

        err(vkCreateCommandPool(device, &poolInfo, nullptr, &commandPool), "Unable to create Command Pool");

        poolInfo.flags = VK_COMMAND_POOL_CREATE_TRANSIENT_BIT;
        err(vkCreateCommandPool(device, &poolInfo, nullptr, &transientCommandPool), "Unable to create the transient command pool.");
    }

    void createFrameBuffers() {
        swapChainFrameBuffers.resize(swapChainImageViews.size());
        for(size_t i = 0; i < swapChainImageViews.size(); i++) {
            VkImageView attachments[] = { swapChainImageViews[i], depthImageView };

            VkFramebufferCreateInfo framebufferInfo = {};
            framebufferInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
            framebufferInfo.renderPass = renderPass;
            framebufferInfo.attachmentCount = 2;
            framebufferInfo.pAttachments = attachments;
            framebufferInfo.width = swapChainExtent.width;
            framebufferInfo.height = swapChainExtent.height;
            framebufferInfo.layers = 1;
            err(vkCreateFramebuffer(device, &framebufferInfo, nullptr, &swapChainFrameBuffers[i]), "Unable to create framebuffer");
        }
    }

    void createRenderPass() {
        VkAttachmentDescription colorAttachment = {};
        colorAttachment.format = swapChainImageFormat;
        colorAttachment.samples = VK_SAMPLE_COUNT_1_BIT;
        colorAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
        colorAttachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
        colorAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        colorAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        colorAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
        colorAttachment.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

        VkAttachmentReference colorAttachmentRef = {};
        colorAttachmentRef.attachment = 0;
        colorAttachmentRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

        VkAttachmentDescription depthAttachment = {};
        depthAttachment.format = findDepthFormat();
        depthAttachment.samples = VK_SAMPLE_COUNT_1_BIT;
        depthAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
        depthAttachment.storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        depthAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        depthAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        depthAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
        depthAttachment.finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

        VkAttachmentReference depthAttachmentRef = {};
        depthAttachmentRef.attachment = 1;
        depthAttachmentRef.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

        VkSubpassDescription subpass = {};
        subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
        subpass.colorAttachmentCount = 1;
        subpass.pColorAttachments = &colorAttachmentRef;
        subpass.pDepthStencilAttachment = &depthAttachmentRef;

        VkSubpassDependency dependency = {};
        dependency.srcSubpass = VK_SUBPASS_EXTERNAL;
        dependency.dstSubpass = 0;
        dependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
        dependency.srcAccessMask = 0;
        dependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
        dependency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;


        std::array<VkAttachmentDescription, 2> attachments = {colorAttachment, depthAttachment};
        VkRenderPassCreateInfo renderPassInfo = {};
        renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
        renderPassInfo.attachmentCount = static_cast<uint32_t>(attachments.size());
        renderPassInfo.pAttachments = attachments.data();
        renderPassInfo.subpassCount = 1;
        renderPassInfo.pSubpasses = &subpass;
        renderPassInfo.dependencyCount = 1;
        renderPassInfo.pDependencies = &dependency;

        err(vkCreateRenderPass(device, &renderPassInfo, nullptr, &renderPass), "Unable to create render pass.");
    }

    void createGraphicsPipeline() {
        auto vertShaderCode = readFile("shaders/vert.spv");
        auto fragShaderCode = readFile("shaders/frag.spv");
        VkShaderModule vertShaderModule = createShaderModule(vertShaderCode);
        VkShaderModule fragShaderModule = createShaderModule(fragShaderCode);

        VkPipelineShaderStageCreateInfo vertShaderStageInfo = {};
        vertShaderStageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
        vertShaderStageInfo.stage = VK_SHADER_STAGE_VERTEX_BIT;
        vertShaderStageInfo.module = vertShaderModule;
        vertShaderStageInfo.pName = "main";

        VkPipelineShaderStageCreateInfo fragShaderStageInfo = {};
        fragShaderStageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
        fragShaderStageInfo.stage = VK_SHADER_STAGE_FRAGMENT_BIT;
        fragShaderStageInfo.module = fragShaderModule;
        fragShaderStageInfo.pName = "main";

        VkPipelineShaderStageCreateInfo shaderStages[] = {vertShaderStageInfo, fragShaderStageInfo};

        VkPipelineVertexInputStateCreateInfo vertexInputInfo = {};
        vertexInputInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
        auto bindingDescription = Vertex::getBindingDescription();
        vertexInputInfo.vertexBindingDescriptionCount = 1;
        vertexInputInfo.pVertexBindingDescriptions = &bindingDescription;
        auto attributeDescriptions = Vertex::getAttributeDescriptions();
        vertexInputInfo.vertexAttributeDescriptionCount = static_cast<uint32_t>(attributeDescriptions.size());
        vertexInputInfo.pVertexAttributeDescriptions = attributeDescriptions.data();

        VkPipelineInputAssemblyStateCreateInfo inputAssembly = {};
        inputAssembly.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
        inputAssembly.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
        inputAssembly.primitiveRestartEnable = VK_FALSE;

        VkViewport viewport = {};
        viewport.x = 0;
        viewport.y = 0;
        viewport.width = (float)swapChainExtent.width;
        viewport.height = (float)swapChainExtent.height;
        viewport.minDepth = 0;
        viewport.maxDepth = 1.0f;

        VkRect2D scissor = {};
        scissor.offset = {0, 0};
        scissor.extent = swapChainExtent;

        VkPipelineViewportStateCreateInfo viewportState = {};
        viewportState.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
        viewportState.viewportCount = 1;
        viewportState.pViewports = &viewport;
        viewportState.scissorCount = 1;
        viewportState.pScissors = &scissor;

        VkPipelineRasterizationStateCreateInfo rasterizer = {};
        rasterizer.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
        rasterizer.depthClampEnable = VK_FALSE;
        rasterizer.rasterizerDiscardEnable = VK_FALSE;
        rasterizer.polygonMode = VK_POLYGON_MODE_FILL;
        rasterizer.lineWidth = 1.0f;
        rasterizer.cullMode = VK_CULL_MODE_BACK_BIT;
        rasterizer.frontFace = VK_FRONT_FACE_CLOCKWISE;
        rasterizer.depthBiasEnable = VK_FALSE;
        rasterizer.depthBiasConstantFactor = 0.0f;
        rasterizer.depthBiasClamp = 0.0f;
        rasterizer.depthBiasSlopeFactor = 0.0f;

        VkPipelineMultisampleStateCreateInfo multisampling = {};
        multisampling.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
        multisampling.sampleShadingEnable = VK_FALSE;
        multisampling.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
        multisampling.minSampleShading = 1.0f;
        multisampling.pSampleMask = nullptr;
        multisampling.alphaToCoverageEnable = VK_FALSE;
        multisampling.alphaToOneEnable = VK_FALSE;

        VkPipelineColorBlendAttachmentState colorBlendAttachment = {};
        colorBlendAttachment.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
        colorBlendAttachment.blendEnable = VK_FALSE;
        colorBlendAttachment.srcColorBlendFactor = VK_BLEND_FACTOR_ONE;
        colorBlendAttachment.dstColorBlendFactor = VK_BLEND_FACTOR_ZERO;
        colorBlendAttachment.colorBlendOp = VK_BLEND_OP_ADD;
        colorBlendAttachment.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
        colorBlendAttachment.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
        colorBlendAttachment.alphaBlendOp = VK_BLEND_OP_ADD;


        VkPipelineColorBlendStateCreateInfo colorBlending = {};
        colorBlending.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
        colorBlending.logicOpEnable = VK_FALSE;
        colorBlending.logicOp = VK_LOGIC_OP_COPY;
        colorBlending.attachmentCount = 1;
        colorBlending.pAttachments = &colorBlendAttachment;
        colorBlending.blendConstants[0] = 0.0f;
        colorBlending.blendConstants[1] = 0.0f;
        colorBlending.blendConstants[2] = 0.0f;
        colorBlending.blendConstants[3] = 0.0f;


        VkPipelineLayoutCreateInfo pipelineLayoutInfo = {};
        pipelineLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
        pipelineLayoutInfo.setLayoutCount = 1;
        pipelineLayoutInfo.pSetLayouts = &descriptorSetLayout;
        pipelineLayoutInfo.pushConstantRangeCount = 0;
        pipelineLayoutInfo.pPushConstantRanges = 0;

        err(vkCreatePipelineLayout(device, &pipelineLayoutInfo, nullptr, &pipelineLayout), "Can't create the pipeline layout.");

        VkPipelineDepthStencilStateCreateInfo depthStencil = {};
        depthStencil.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
        depthStencil.depthTestEnable = VK_TRUE;
        depthStencil.depthWriteEnable = VK_TRUE;
        depthStencil.depthCompareOp = VK_COMPARE_OP_LESS;
        depthStencil.depthBoundsTestEnable = VK_FALSE;
        depthStencil.minDepthBounds = 0.0f;
        depthStencil.maxDepthBounds = 1.0f;
        depthStencil.stencilTestEnable = VK_FALSE;
        depthStencil.front = {};
        depthStencil.back = {};

        VkGraphicsPipelineCreateInfo pipelineInfo = {};
        pipelineInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
        pipelineInfo.stageCount = 2;
        pipelineInfo.pStages = shaderStages;
        pipelineInfo.pVertexInputState = &vertexInputInfo;
        pipelineInfo.pInputAssemblyState = &inputAssembly;
        pipelineInfo.pViewportState = &viewportState;
        pipelineInfo.pRasterizationState = &rasterizer;
        pipelineInfo.pMultisampleState = &multisampling;
        pipelineInfo.pDepthStencilState = &depthStencil;
        pipelineInfo.pColorBlendState = &colorBlending;
        pipelineInfo.pDynamicState = nullptr;
        pipelineInfo.layout = pipelineLayout;
        pipelineInfo.renderPass = renderPass;
        pipelineInfo.subpass = 0;
        pipelineInfo.basePipelineHandle = VK_NULL_HANDLE;
        pipelineInfo.basePipelineIndex = -1;

        err(vkCreateGraphicsPipelines(device, VK_NULL_HANDLE, 1, &pipelineInfo, nullptr, &graphicsPipeline), "Unable to create Graphics pipeline.");

        vkDestroyShaderModule(device, fragShaderModule, nullptr);
        vkDestroyShaderModule(device, vertShaderModule, nullptr);
    }

    VkShaderModule createShaderModule(const std::vector<char>& code) {
        VkShaderModuleCreateInfo createInfo = {};
        createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
        createInfo.codeSize = code.size();
        createInfo.pCode = reinterpret_cast<const uint32_t*>(code.data());
        VkShaderModule shaderModule;
        err(vkCreateShaderModule(device, &createInfo, nullptr, &shaderModule), "Cannot create shader module");
        return shaderModule;
    }


    VkImageView createImageView(VkImage image, VkFormat format, VkImageAspectFlags aspectFlags) {
        VkImageViewCreateInfo createInfo = {};
        createInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
        createInfo.image = image;
        createInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
        createInfo.format = format;
        createInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
        createInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
        createInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
        createInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
        createInfo.subresourceRange.aspectMask = aspectFlags;
        createInfo.subresourceRange.baseMipLevel = 0;
        createInfo.subresourceRange.levelCount = 1;
        createInfo.subresourceRange.baseArrayLayer = 0;
        createInfo.subresourceRange.layerCount = 1;

        VkImageView imageView;
        err(vkCreateImageView(device, &createInfo, nullptr, &imageView), "failed to create swapchain image view");

        return imageView;
    }

    void createImageViews() {
        swapChainImageViews.resize(swapChainImages.size());
        for(size_t i = 0; i < swapChainImages.size(); i++) {
            swapChainImageViews[i] = createImageView(swapChainImages[i], swapChainImageFormat, VK_IMAGE_ASPECT_COLOR_BIT);
        }
    }

    void createSwapChain() {

        SwapChainSupportDetails support = querySwapChainSupport(physicalDevice);
        VkSurfaceFormatKHR format = support.chooseSwapSurfaceFormat();
        swapChainImageFormat = format.format;
        swapChainExtent = support.chooseSwapExtent();


        uint32_t image_count = std::max(support.capabilities.minImageCount, static_cast<uint32_t>(2));
        if(support.capabilities.maxImageCount > 0 && image_count > support.capabilities.maxImageCount)
            image_count = support.capabilities.maxImageCount;

        VkSwapchainCreateInfoKHR createInfo = {};
        createInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
        createInfo.surface = surface;
        createInfo.minImageCount = image_count;
        createInfo.imageFormat = format.format;
        createInfo.imageColorSpace = format.colorSpace;
        createInfo.imageExtent = swapChainExtent;
        createInfo.imageArrayLayers = 1;
        createInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;

        VulkanQueueIndices indices = getQueues(physicalDevice, surface);
        uint32_t queueFamilyIndices[] = {indices.graphics, indices.present};
        if(indices.graphics == indices.present) {
            createInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
            createInfo.queueFamilyIndexCount = 0;
            createInfo.pQueueFamilyIndices = nullptr;
        } else {
            createInfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
            createInfo.queueFamilyIndexCount = 2;
            createInfo.pQueueFamilyIndices = queueFamilyIndices;
        }

        createInfo.preTransform = support.capabilities.currentTransform;
        createInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
        createInfo.presentMode = support.choosePresentMode();
        createInfo.clipped = VK_TRUE;
        createInfo.oldSwapchain = VK_NULL_HANDLE;

        err(vkCreateSwapchainKHR(device, &createInfo, nullptr, &swapchain), "Couldn't create the swap chain");

        /* Get the Swap Chain Images */
        vkGetSwapchainImagesKHR(device, swapchain, &image_count, nullptr);
        swapChainImages.resize(image_count);
        vkGetSwapchainImagesKHR(device, swapchain, &image_count, swapChainImages.data());
    }

    void createSurface() {
        err(glfwCreateWindowSurface(instance, window, nullptr, &surface), "Couldn't create window surface");
    }

    SwapChainSupportDetails querySwapChainSupport(VkPhysicalDevice device) {
        SwapChainSupportDetails details;

        vkGetPhysicalDeviceSurfaceCapabilitiesKHR(device, surface, &details.capabilities);

        uint32_t format_count;
        vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &format_count, nullptr);
        if(format_count != 0) {
            details.formats.resize(format_count);
            vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &format_count, details.formats.data());
        }

        uint32_t present_mode_count = 0;
        vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface, &present_mode_count, nullptr);
        if(present_mode_count != 0) {
            details.presentModes.resize(present_mode_count);
            vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface, &present_mode_count, details.presentModes.data());
        }

        return details;
    }

    void createLogicalDevice() {
        std::vector<VkDeviceQueueCreateInfo> queueCreateInfos = queueLibrary->createQueueInfos(1, VK_QUEUE_TRANSFER_BIT | VK_QUEUE_GRAPHICS_BIT, true);

        VkPhysicalDeviceFeatures deviceFeatures = {};

        VkDeviceCreateInfo createInfo = {};
        createInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
        createInfo.queueCreateInfoCount = static_cast<uint32_t>(queueCreateInfos.size());
        createInfo.pQueueCreateInfos = queueCreateInfos.data();
        createInfo.pEnabledFeatures = &deviceFeatures;
        createInfo.enabledExtensionCount = static_cast<uint32_t>(deviceExtensions.size());
        createInfo.ppEnabledExtensionNames = deviceExtensions.data();
        if(enableValidationLayers) {
            createInfo.enabledLayerCount = static_cast<uint32_t>(validationLayers.size());
            createInfo.ppEnabledExtensionNames = validationLayers.data();
        } else {
            createInfo.enabledLayerCount = 0;
        }
        err(vkCreateDevice(physicalDevice, &createInfo, nullptr, &device), "Can't create logical device: ");

        queueLibrary->addCreatedQueues(device);
    }

    void createVulkanInstance() {
        VkApplicationInfo appInfo = {};
        appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
        appInfo.pApplicationName = "Space Eagles";
        appInfo.applicationVersion = VK_MAKE_VERSION(1,0,0);
        appInfo.pEngineName ="No Engine";
        appInfo.engineVersion = VK_MAKE_VERSION(1,0,0);
        appInfo.apiVersion = VK_API_VERSION_1_0;

        VkInstanceCreateInfo createInfo = {};
        createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
        createInfo.pApplicationInfo = &appInfo;
        auto extensions = getRequiredExtensions();
        createInfo.enabledExtensionCount = static_cast<uint32_t>(extensions.size());
        createInfo.ppEnabledExtensionNames = extensions.data();
        if(enableValidationLayers) {
            createInfo.enabledLayerCount = validationLayers.size();
            createInfo.ppEnabledLayerNames = validationLayers.data();
        } else {
            createInfo.enabledLayerCount = 0;
        }

        err(vkCreateInstance(&createInfo, nullptr, &instance), "can't create instance:");
    }

    void selectPhysicalDevice() {
        uint32_t count = 0;
        vkEnumeratePhysicalDevices(instance, &count, nullptr);
        if(count == 0)
            throw std::runtime_error("There are no GPUs with Vulkan support.");
        std::vector<VkPhysicalDevice> devices(count);
        vkEnumeratePhysicalDevices(instance, &count, devices.data());
        for(const auto& device : devices) {
            if(isDeviceSuitable(device)) {
                physicalDevice = device;
                return;
            }
        }

        throw std::runtime_error("Could not find a suitable GPU!");
    }

    bool isDeviceSuitable(VkPhysicalDevice device) {
        VkPhysicalDeviceProperties properties;
        vkGetPhysicalDeviceProperties(device, &properties);

        VkPhysicalDeviceFeatures features;
        vkGetPhysicalDeviceFeatures(device, &features);

        VulkanQueueIndices queues = getQueues(device, surface);

        return (properties.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU ||
            properties.deviceType == VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU) &&
            features.geometryShader && queues.isComplete();
    }


    void initWindow(){
        glfwInit();
        glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
        glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);
        window = glfwCreateWindow(WIDTH, HEIGHT, "Space Eagles", nullptr, nullptr);
        glfwSetKeyCallback(window, keyCallback);
        glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
        glfwSetCursorPosCallback(window, mouseCallback);
    }

    std::vector<const char*> getRequiredExtensions() {
        std::vector<const char*> extensions;

        uint32_t glfwExtensionsCount = 0;
        const char** glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionsCount);

        for(int i = 0; i < glfwExtensionsCount; i++)
            extensions.push_back(glfwExtensions[i]);
        if(enableValidationLayers)
            extensions.push_back(VK_EXT_DEBUG_REPORT_EXTENSION_NAME);

        return extensions;
    }

    void setupDebugCallback() {
        if(!enableValidationLayers)
            return;

        VkDebugReportCallbackCreateInfoEXT createInfo = {};
        createInfo.sType = VK_STRUCTURE_TYPE_DEBUG_REPORT_CALLBACK_CREATE_INFO_EXT;
        createInfo.flags = VK_DEBUG_REPORT_ERROR_BIT_EXT | VK_DEBUG_REPORT_WARNING_BIT_EXT;
        createInfo.pfnCallback = debugCallback;

        auto func = (PFN_vkCreateDebugReportCallbackEXT) vkGetInstanceProcAddr(instance, "vkCreateDebugReportCallbackEXT");
        if(func != nullptr) {
            err(func(instance, &createInfo, nullptr, &debugFunction), "Can't install debug callback");
        } else {
            std::cerr << "Can't install debug callback hook";
        }
    }

};

void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods) {
    if(key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE) {
        glfwSetWindowShouldClose(window, true);
    }
    if(key == GLFW_KEY_HOME && action == GLFW_RELEASE) {
        glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
        glfwSetCursorPosCallback(window, nullptr);
    }
    if(key == GLFW_KEY_F2) {
        takeScreenshot = true;
    }

    if(key == GLFW_KEY_W && action != GLFW_RELEASE) {
        speed += 1.0;
        if(speed > 250)
            speed = 250;
    }
    if(key == GLFW_KEY_S && action != GLFW_RELEASE) {
        speed -= 0.5;
        if(speed < 0)
            speed = 0;
    }
    if(key == GLFW_KEY_BACKSPACE && action != GLFW_RELEASE) {
        speed = 0;
    }

}


void mouseCallback(GLFWwindow* window, double xpos, double ypos) {
    static bool noFirstPosition = true;
    static double lastXPos, lastYPos;
    if(noFirstPosition) {
        lastXPos = xpos;
        lastYPos = ypos;
        noFirstPosition = false;
    }
    double delta_x = xpos - lastXPos;
    double delta_y = ypos - lastYPos;


    horizontalCameraAngle = (horizontalCameraAngle + delta_x * (PI / 3600));
    while(horizontalCameraAngle > 2 * PI)
            horizontalCameraAngle -= 2 * PI;
    while(horizontalCameraAngle < -2 * PI)
            horizontalCameraAngle += 2 * PI;
    verticalCameraAngle = (verticalCameraAngle - delta_y * (PI / 3600));
    while(verticalCameraAngle > 2 * PI)
            verticalCameraAngle -= 2 * PI;
    while(verticalCameraAngle < -2 * PI)
            verticalCameraAngle += 2 * PI;

    // printf("(%.3f,%.3f)\n", delta_x, delta_y);
    lastXPos = xpos;
    lastYPos = ypos;
}

VulkanQueueIndices getQueues(VkPhysicalDevice device, VkSurfaceKHR surface) {
    VulkanQueueIndices queues;
    uint32_t queueFamilyCount = 0;
    vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, nullptr);
    std::vector<VkQueueFamilyProperties> queueFamilies(queueFamilyCount);
    vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, queueFamilies.data());
    VkBool32 presentSupport = false;

    int i = 0;
    for(const auto& fam : queueFamilies) {
        vkGetPhysicalDeviceSurfaceSupportKHR(device, i, surface, &presentSupport);
        if(fam.queueCount > 0 && (fam.queueFlags & VK_QUEUE_GRAPHICS_BIT)) {
            queues.graphics = i;
        }
        if(fam.queueCount > 0 && presentSupport)
            queues.present = i;

        if(queues.isComplete())
            break;
        i++;
    }
    return queues;
}

static std::vector<char> readFile(const std::string& filename) {
    std::ifstream file(filename, std::ios::ate | std::ios::binary);
    if(!file.is_open()) {
        throw std::runtime_error("failed to open file!");
    }

    size_t fileSize = (size_t) file.tellg();
    std::vector<char> buffer(fileSize);

    file.seekg(0);
    file.read(buffer.data(), fileSize);
    file.close();

    return buffer;
}



int main(int argc, char **argv) {
    SpaceEagles app(1);

    try {
        app.mainLoop();
        app.cleanup();
    } catch(const std::runtime_error& e) {
        std::cerr << e.what() << std::endl;
        return -1;
    }

    return 0;
}




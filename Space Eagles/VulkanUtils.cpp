
#include <stdio.h>

#include <VulkanUtils.h>
#include <VulkanBuffer.h>

// TODO: this is a mess which probably needs to get deleted and folded into VulkanJob/VulkanJobGroup

inline void err(VkResult r, const char* message) {
    if(r != VK_SUCCESS)
        fprintf(stderr, "%s\n", message);
}

VkCommandBuffer beginSingleTimeCommands() {
    VkCommandBufferAllocateInfo info = {};
    info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    info.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    // info.commandPool = transientCommandPool;
    info.commandBufferCount = 1;

    VkCommandBuffer commandBuffer;
    // vkAllocateCommandBuffers(device, &info, &commandBuffer);

    VkCommandBufferBeginInfo begin = {};
    begin.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    begin.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

    vkBeginCommandBuffer(commandBuffer, &begin);
    return commandBuffer;
}

void endSingleTimeCommands(VkCommandBuffer commandBuffer) {
    vkEndCommandBuffer(commandBuffer);

    VkSubmitInfo submitInfo = {};
    submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submitInfo.commandBufferCount = 1;
    submitInfo.pCommandBuffers = &commandBuffer;

//    {
//        GraphicsQueueLoan loan(queueLibrary);
//        vkQueueSubmit(*loan.queue, 1, &submitInfo, VK_NULL_HANDLE);
//        vkQueueWaitIdle(*loan.queue);
//    }
//    vkFreeCommandBuffers(device, transientCommandPool, 1, &commandBuffer);
}


uint32_t findMemoryType(VkPhysicalDevice physicalDevice, uint32_t typeFilter, VkMemoryPropertyFlags properties) {
    VkPhysicalDeviceMemoryProperties memProperties;
    vkGetPhysicalDeviceMemoryProperties(physicalDevice, &memProperties);
    for(uint32_t i = 0; i < memProperties.memoryTypeCount; i++)
        if((typeFilter & (1 << i)) && (memProperties.memoryTypes[i].propertyFlags & properties) == properties)
            return i;
    throw std::runtime_error("failed to find suitable memory type!");
}


void createVulkanBuffer(VkPhysicalDevice physicalDevice, VkDevice device, VkDeviceSize size, VkBufferUsageFlags usage, VkMemoryPropertyFlags properties,
        VkBuffer& buffer, VkDeviceMemory& bufferMemory) {
    VkBufferCreateInfo bufferInfo = { };
    bufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    bufferInfo.size = size;
    bufferInfo.usage = usage;
    bufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    err(vkCreateBuffer(device, &bufferInfo, nullptr, &buffer), "Failed to create vertex buffer.");

    VkMemoryRequirements memRequirements;
    vkGetBufferMemoryRequirements(device, buffer, &memRequirements);

    VkMemoryAllocateInfo allocInfo = { };
    allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    allocInfo.allocationSize = memRequirements.size;
    allocInfo.memoryTypeIndex = findMemoryType(physicalDevice, memRequirements.memoryTypeBits, properties);

    err(vkAllocateMemory(device, &allocInfo, nullptr, &bufferMemory), "Unable to allocate device memory");
    // printf("allocated memory.\n");
    vkBindBufferMemory(device, buffer, bufferMemory, 0);
    // printf("bound the memory.\n");
}

void copyBuffer(VkBuffer srcBuffer, VkBuffer dstBuffer, VkDeviceSize targetOffset, VkDeviceSize size) {
    VkCommandBuffer commandBuffer = beginSingleTimeCommands();

    VkBufferCopy copyRegion = { };
    copyRegion.srcOffset = 0;
    copyRegion.dstOffset = targetOffset;
    copyRegion.size = size;
    vkCmdCopyBuffer(commandBuffer, srcBuffer, dstBuffer, 1, &copyRegion);

    endSingleTimeCommands(commandBuffer);
}

void stageDataAndCopyInto(VkPhysicalDevice physicalDevice, VkDevice device, void* data, uint32_t size, uint32_t offset, VulkanBuffer& destination) {
    VkBuffer stagingBuffer;
    VkDeviceMemory stagingBufferMemory;
    createVulkanBuffer(physicalDevice, device, size, VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
            VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, stagingBuffer, stagingBufferMemory);
    void* mapped_memory;
    vkMapMemory(device, stagingBufferMemory, 0, size, 0, &mapped_memory);
    memcpy(mapped_memory, data, (size_t) size);
    vkUnmapMemory(device, stagingBufferMemory);

    copyBuffer(stagingBuffer, destination.buffer, offset, size);

    vkDestroyBuffer(device, stagingBuffer, nullptr);
    vkFreeMemory(device, stagingBufferMemory, nullptr);
}

/**
 * Creates a vertex buffer of the specified size, sticking into buffer
 * @param size the size (in bytes) of the buffer
 * @param buffer the buffer into which to store the vertex buffer
 * @param bufferMemory
 */
void createBuffer(VkPhysicalDevice physicalDevice, VkDevice device, void* data, int size, int usage, VulkanBuffer& buffer) {
    printf("createVertexBuffer: size == %d\n", size);
    createVulkanBuffer(physicalDevice, device, size, usage, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, buffer.buffer, buffer.memory);
    stageDataAndCopyInto(physicalDevice, device, data, size, 0, buffer);
}



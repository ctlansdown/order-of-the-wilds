#include "Terrain.h"

#include "boost/thread.hpp"

#include <stdlib.h>
#include <random>
#include <vector>
#include <unordered_map>

typedef boost::unique_lock<boost::mutex> Lock;

vector<unordered_map<point, terrain_description>>& Terrain::getDescriptions(point p) {
    Lock lock(description_mutex);
    return *descriptions[p];
}

vector<float>* Terrain::getHeightmapVector(point p) {
    Lock lock(heightmap_mutex);
    if(heightmap.count(p) == 0) {
        printf("Allocating new heightmap for (%ld,%ld)\n", p.x, p.y);
        heightmap[p] = new vector<float>();
    }
    return heightmap[p];
}

bool Terrain::haveHeightmapData(point p) {
    printf("Terrain::haveHeightmapData(%ld,%ld)\n", p.x, p.y);
    Lock lock(heightmap_mutex);
    printf("Terrain::haveHeightmapData(%ld,%ld) returning\n", p.x, p.y);
    return heightmap.count(p) != 0;
}

vector<Vertex>* Terrain::getVertexVector(point p) {
    Lock lock(vertex_mutex);
    if(vertexmap.count(p) == 0)
        vertexmap[p] = new vector<Vertex>();
    return vertexmap[p];
}
vector<uint32_t>* Terrain::getIndicesVector(point p) {
    Lock lock(index_mutex);
    if(vertexIndicesMap.count(p) == 0)
        vertexIndicesMap[p] = new vector<uint32_t>;
    return vertexIndicesMap[p];
}

bool Terrain::haveVertexData(point p) {
    Lock lock(vertex_mutex);
    return vertexmap.count(p) != 0;
}

bool Terrain::haveIndexData(point p) {
    Lock lock(index_mutex);
    return vertexIndicesMap.count(p) != 0;
}


/**
 *
 * NOTE: this method is thread-safe because it locks the datastructure with the description_mutex
 */
void Terrain::generateDescriptions(double x, double y, uint64_t seed) {
    Lock lock(description_mutex);

    printf("generateDescriptions(%f, %f, %lud)\n", x, y, seed);
    point location = {(int64_t)x, (int64_t)y};

    // Don't do anything if we've already generated the descriptions
    if(descriptions.count(location) > 0)
        return;

    descriptions[location] = new vector<unordered_map<point, terrain_description>>();
    auto& description = *descriptions[location];
    description.resize(3);

    std::mt19937 mt((unsigned)seed ^ (seed >> 32));
    std::uniform_real_distribution<double> uniform(0.0, 1.0);
    std::uniform_int_distribution<int> type(0,2);

    terrain_description desc = {};
    desc.type = type(mt);
    desc.dominance = uniform(mt);
    desc.averageHeight = uniform(mt)*TYPE_HEIGHTS[desc.type];
    desc.variance = uniform(mt)*TYPE_VARIANCE[desc.type];

    printf("        (%f,%f): t %d, d %f, h %f, var %f\n", x, y, desc.type, desc.dominance, desc.averageHeight, desc.variance);

    description[2][location] = desc;
}

void Terrain::generate_zoom_level(double x, double y, int level, std::uniform_real_distribution<double>& uniform, std::mt19937& mt,
                                  vector<unordered_map<point, terrain_description>>& my_descriptions, terrain_description& my_description) {
    double mid_x = floor(x/1000)*1000.0 + 500.0;
    double mid_y = floor(y/1000)*1000.0 + 500.0;
    // printf("generate_zoom_level(%f, %f, %d, etc, etc) (%f,%f)\n", x, y, level, mid_x, mid_y);
    for (int i = 0; i < 10; i++) {
        for (int j = 0; j < 10; j++) {
            int scale = 1;
            for(int i = 0; i < level; i++)
                scale *= 10;
            int64_t gx = x + i*scale;
            int64_t gy = y + j*scale;
            int64_t nx = (gx - mid_x < 0) ? x - 1000 : x + 1000;
            int64_t ny = (gy - mid_y < 0) ? y - 1000 : y + 1000;
            point neighbor_point((nx/1000)*1000, (ny/1000)*1000);
            auto& neighbor = getDescriptions(neighbor_point)[2][neighbor_point];
            terrain_description desc = { };
            desc.type = my_description.type;

            // the distance factor scales between 1.0 (for the center of our block) and 0.5 (for an edge)
            float distance_factor = 1.0 - max(abs(gx-mid_x), abs(gy-mid_y))/1000.0;

            desc.averageHeight = (my_description.averageHeight * my_description.dominance * distance_factor
                    + neighbor.averageHeight * neighbor.dominance * (1 - distance_factor)) / (my_description.dominance*distance_factor + neighbor.dominance*(1-distance_factor));
            desc.averageHeight += (uniform(mt) - 0.5) * my_description.variance * 10;
            desc.variance = my_description.variance * (1-(uniform(mt)*uniform(mt)));
            my_descriptions[level-1][point(gx, gy)] = desc;
            // printf("(%ld,%ld) => h=%f (h=%f, v=%f; (%ld,%ld): h=%f, d=%f) (%f)\n", gx, gy, desc.averageHeight, my_description.averageHeight, my_description.variance,
            //        (nx/1000)*1000, (ny/1000)*1000, neighbor.averageHeight, neighbor.dominance, distance_factor);
            if(level > 1)
                generate_zoom_level(gx, gy, level-1, uniform, mt, my_descriptions, desc);
        }
    }

}

void Terrain::generateTerrain(double x, double y, uint64_t seed) {
    printf("generateTerrain(%f, %f, %ld): starting\n", x, y, seed);
    std::mt19937 mt((unsigned)seed ^ (seed >> 32));
    std::uniform_real_distribution<double> uniform(0.0, 1.0);
    point id_point((int64_t) (x), (int64_t) (y));
//    descriptions.emplace_back();
//    descriptions.emplace_back();
//    descriptions.emplace_back();

    /* Start by calculating the nearby descriptions */
    for(int i = -1; i <= 1; i++) {
        for(int j = -1; j <= 1; j++) {
            int gx = x + i*1000;
            int gy = y + j*1000;
            generateDescriptions(gx, gy, calculateSeed(gx, gy));
        }
    }

    printf("generateTerrain(%f, %f, %ld): generated descriptions\n", x, y, seed);

    auto& my_descriptions = getDescriptions(id_point);

    printf("generateTerrain(%f, %f, %ld): got my description\n", x, y, seed);

    /* Next, work out the coarse-grained descriptions */
    auto& my_description = my_descriptions[2][id_point];
    generate_zoom_level(x, y, 2, uniform, mt, my_descriptions, my_description);

    printf("generateTerrain(%f, %f, %ld): generated zoom levels\n", x, y, seed);

    /* Generate the basic terrain from the descriptions */
    auto& heights = *getHeightmapVector(id_point);
    heights.resize(1000*1000, 0.0f);
    for(int k = 0; k < 100; k++) {
        for(int l = 0; l < 100; l++) {
            auto& d = my_descriptions[0][point((int64_t)x + 10*k, (int64_t)y + 10*l)];
            for(int i = 0; i < 10; i++) {
                for(int j = 0; j < 10; j++)
                    heights[1000*(10*l +j) + 10*k + i] = d.averageHeight + uniform(mt)*my_description.variance/2;;
            }
        }
    }
    printf("generateTerrain(%f, %f, %ld): done\n", x, y, seed);
}



void Terrain::generateVertices(int64_t x, int64_t y, vector<Vertex>& vertices, vector<uint32_t>& indices) {
    printf("generateVertices(%ld, %ld, etc): starting.\n", x, y);
    point id_point(x,y);
    float max_height = -1000000;
    float min_height = 1000000;
    auto& map = *getHeightmapVector(id_point);
    for(float h : map) {
        if(h > max_height)
            max_height = h;
        if(h < min_height)
            min_height = h;
    }
    float height_range = max_height - min_height;
    printf("Max height is %f, min_height is %f\n", max_height, min_height);


//    for(int i = 0; i < 200; i++) {
//        for(int j = 0; j < 30; j++)
//            printf("%0.1f ", map[1000*i + j]);
//        printf("\n");
//    }
//    exit(0);

    vertices.resize(map.size());
    for(int i = 0; i < map.size(); i++) {
        int row = i / 1000;
        int col = i % 1000;

        /* Coordinates */
        vertices[i].pos[0] = x + col;
        vertices[i].pos[1] = y + row;
        vertices[i].pos[2] = map[i];

        /* Color */
        vertices[i].color[0] = (1 - (map[i]/height_range))/3.0;
        vertices[i].color[1] = map[i]/(2*height_range) + 0.2;
        vertices[i].color[2] = 0.0;


        /* Normal */
        glm::vec3 v1, v2;
        if(row > 0 && row < 999) {
            if(col > 0 && col < 999) {
                v1[0] = 1;
                v1[1] = 1;
                v1[2] = map[1000 * (row - 1) + (col - 1)] - map[1000 * (row + 1) + (col + 1)];

                v2[0] = -1;
                v2[1] = 1;
                v2[2] = map[1000 * (row - 1) + (col + 1)] - map[1000 * (row + 1) + (col - 1)];
            } else {
                v1[0] = 1;
                v1[1] = 1;
                v1[2] = map[1000 * (row + 1) + (col)];

                v2[0] = -1;
                v2[1] = 1;
                v2[2] = map[1000 * (row - 1) + (col)];
            }
        } else {
            if(col > 0 && col < 999) {
                /* Top and bottom edges */
                v1[0] = 1;
                v1[1] = 1;
                v1[2] = map[1000 * (row) + (col + 1)];

                v2[0] = -1;
                v2[1] = 1;
                v2[2] = map[1000 * (row) + (col - 1)];
            } else {
                /* four corners */
                v1[0] = 1;
                v1[1] = 1;
                v1[2] = 0;

                v2[0] = -1;
                v2[1] = 1;
                v2[2] = 0;
            }
        }
        vertices[i].normal = glm::cross(v1, v2);
        glm::normalize(vertices[i].normal);
    }

    int side_length = (int)sqrt(map.size());
    indices.reserve((side_length-1)*(side_length-1)*2*3);
    for(int i = 0; i < side_length-1; i++) {
        for(int j = 0; j < side_length-1; j++) {
            indices.push_back(i * side_length + j + 1);
            indices.push_back(i * side_length + j);
            indices.push_back((i + 1) * side_length + j);

            indices.push_back(i * side_length + j + 1);
            indices.push_back((i + 1) * side_length + j);
            indices.push_back((i + 1) * side_length + j + 1);
        }
    }
    printf("generateVertices(%ld, %ld, etc): done.\n", x, y);
}

vector<Vertex>* Terrain::getVertexData(int64_t x, int64_t y) {
    printf("getVertexData(%ld, %ld)\n", x, y);
    // step 1: make sure that the terrain is generated
    point id_point(x,y);
    if(!haveHeightmapData(id_point)) {
        generateTerrain(x, y);
    }

    // step 2: ensure that the terrain heights have been translated to vertices
    if(!haveVertexData(id_point)) {
        auto* vertices = getVertexVector(id_point);
        auto* indices = getIndicesVector(id_point);
        generateVertices(x, y, *vertices, *indices);
    }

    return getVertexVector(id_point);
}

vector<uint32_t>* Terrain::getIndexData(int64_t x, int64_t y){
    // step 1: make sure that the terrain is generated
    point id_point(x,y);
    if(!haveHeightmapData(id_point)) {
        generateTerrain(x, y);
    }

    // step 2: ensure that the terrain heights have been translated to vertices
    if(!haveVertexData(id_point)) {
        auto* vertices = getVertexVector(id_point);
        auto* indices = getIndicesVector(id_point);
        generateVertices(x, y, *vertices, *indices);
    }

    return getIndicesVector(id_point);
}

#define RIGHT 0
#define LEFT 1
#define TOP 2
#define BOTTOM 3

void Terrain::generateSeamData(int64_t x1, int64_t y1, uint32_t offset1, int64_t x2, int64_t y2, uint32_t offset2, vector<uint32_t>& seam) {
    seam.resize(getTileIndexBufferSeamElementCount());



    int which;

    if(x1 < x2)
        which = RIGHT;
    else if(x2 < x1)
        which = LEFT;
    else if(y1 < y2)
        which = TOP;
    else
        which = BOTTOM;

    printf("generateSeamData(%ld, %ld, %u,        \t%ld, %ld, %u,        \tseam)  \t(%d)\n", x1, y1, offset1, x2, y2, offset2, which);

    if(which == RIGHT) {
        for(int i = 0; i < 999; i++) {
            int base = i*6;
            seam[base + 1] = i*1000 + 999 + offset1;
            seam[base + 0] = i*1000 + 0   + offset2;
            seam[base + 2] = (i+1)*1000 + 999 + offset1;

            seam[base + 4] = (i+1)*1000 + 999 + offset1;
            seam[base + 3] = i*1000 + 0   + offset2;
            seam[base + 5] = (i+1)*1000 + 0   + offset2;
        }
    } else if(which == LEFT) {
        for(int i = 0; i < 999; i++) {
            int base = i*6;
            seam[base + 0] = i*1000 + offset1;
            seam[base + 1] = (i+1)*1000 + 0   + offset1;
            seam[base + 2] = (i)*1000 + 999 + offset2;

            seam[base + 3] = (i+1)*1000 + 0 + offset1;
            seam[base + 4] = (i+1)*1000 + 999 + offset2;
            seam[base + 5] = (i)*1000 + 999 + offset2;
        }
    } else if(which == TOP) {
        for(int i = 0; i < 999; i++) {
            int base = i*6;
            seam[base + 0] = i + 1 + offset1;
            seam[base + 1] = i + offset1;
            seam[base + 2] = i + 1000*999 + offset2;

            seam[base + 3] = i + 1 + offset1;
            seam[base + 4] = i + 1000*999 + offset2;
            seam[base + 5] = i + 1 + 1000*999 + offset2;
        }
    } else { /* BOTTOM */
        for(int i = 0; i < 999; i++) {
            int base = i*6;
//            for(int j = 0; j < 6; j++)
//                seam[base+j] = offset1;

            seam[base + 0] = i + 1 + 1000*999 + offset2;
            seam[base + 1] = i + 1000*999 + offset2;
            seam[base + 2] = i + offset1;

            seam[base + 3] = i + offset1;
            seam[base + 4] = i + 1 + offset1;
            seam[base + 5] = i + 1000*999 + 1 + offset2;
            if(i == 100) {
                printf("%d,%d,%d       \t%d,%d,%d\n", seam[base+0], seam[base+1], seam[base+2], seam[base+3], seam[base+4], seam[base+5]);
            }
        }
    }


}


/*
 * CommandPoolLibrary.cpp
 *
 *  Created on: Aug 10, 2018
 *      Author: ctl
 */

#include <algorithm>
#include <CommandPoolLibrary.h>

typedef boost::unique_lock<boost::mutex> Lock;

CommandPoolLibrary::CommandPoolLibrary(QueueLibrary *q, VkDevice dev) {
    queueLibrary = q;
    device = dev;
}

void CommandPoolLibrary::createCommandPools(uint32_t count, bool createSingleUseBuffers) {

    queueFamilyIndices = queueLibrary->getQueueFamilies();

    int max_family_index = *std::max_element(queueFamilyIndices.begin(), queueFamilyIndices.end());
    if(createSingleUseBuffers) {
        temporaryCommandPoolStatus.resize(max_family_index+1);
        temporaryCommandPools.resize(max_family_index+1);
    }
    commandPoolStatus.resize(max_family_index+1);
    commandPools.resize(max_family_index+1);

    for(int fi = 0; fi < queueFamilyIndices.size(); fi++) {
        uint32_t queueFamilyIndex = queueFamilyIndices[fi];
        if(createSingleUseBuffers) {
            temporaryCommandPoolStatus[queueFamilyIndex].resize(count, true);
            temporaryCommandPools[queueFamilyIndex].resize(count);
        }
        commandPoolStatus[queueFamilyIndex].resize(count, true);
        commandPools[queueFamilyIndex].resize(count);
        for(int i = 0; i < count; i++) {
            if(createSingleUseBuffers) {
                VkCommandPoolCreateInfo tmp_info = {
                        .sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
                        .pNext = nullptr,
                        .flags = VK_COMMAND_POOL_CREATE_TRANSIENT_BIT,
                        .queueFamilyIndex = queueFamilyIndex
                };
                VkCommandPool handle;
                if(auto ret = vkCreateCommandPool(device, &tmp_info, nullptr, &handle) != VK_SUCCESS) {
                    if(ret == VK_ERROR_OUT_OF_HOST_MEMORY) {
                        printf("Can't allocate command pool: out of host memory.\n");
                    }
                    if(ret == VK_ERROR_OUT_OF_DEVICE_MEMORY) {
                        printf("Can't allocate command pool: out of device memory.\n");
                    }
                } else {
                    temporaryCommandPools[queueFamilyIndex][i] = handle;
                }
            }

            VkCommandPoolCreateInfo info = {
                    .sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
                    .pNext = nullptr,
                    .flags = 0,
                    .queueFamilyIndex = queueFamilyIndex
            };
            VkCommandPool handle;
            if(auto ret = vkCreateCommandPool(device, &info, nullptr, &handle) != VK_SUCCESS) {
                if(ret == VK_ERROR_OUT_OF_HOST_MEMORY) {
                    printf("Can't allocate command pool: out of host memory.\n");
                }
                if(ret == VK_ERROR_OUT_OF_DEVICE_MEMORY) {
                    printf("Can't allocate command pool: out of device memory.\n");
                }
            } else {
                commandPools[queueFamilyIndex][i] = handle;
            }
        }
    }


}

VkCommandPool CommandPoolLibrary::borrowCommandPool(uint32_t queueFamilyIndex, bool temporary) {
    Lock lock(mutex);
    VkCommandPool* ret = nullptr;
    while(ret == nullptr) {
        if(temporary) {
            for(int i = 0; i < temporaryCommandPoolStatus[queueFamilyIndex].size(); i++) {
                if(temporaryCommandPoolStatus[queueFamilyIndex][i]) {
                    ret = &temporaryCommandPools[queueFamilyIndex][i];
                    temporaryCommandPoolStatus[queueFamilyIndex][i] = false;
                    goto DONE;
                }
            }
        } else {
            for(int i = 0; i < commandPoolStatus[queueFamilyIndex].size(); i++) {
                if(commandPoolStatus[queueFamilyIndex][i]) {
                    ret = &commandPools[queueFamilyIndex][i];
                    commandPoolStatus[queueFamilyIndex][i] = false;
                    goto DONE;
                }
            }
        }
        if(ret == nullptr)
            cond.wait(lock);
    }
    DONE:

    return *ret;
}

void CommandPoolLibrary::borrowCommandPool(VkCommandPool pool) {
    Lock lock(mutex);

    while(true) {
        for(int queueFamilyIndex = 0; queueFamilyIndex < temporaryCommandPools.size(); queueFamilyIndex++) {
            for(int i = 0; i < temporaryCommandPools[queueFamilyIndex].size(); i++) {
                if(temporaryCommandPools[queueFamilyIndex][i] == pool && temporaryCommandPoolStatus[queueFamilyIndex][i]) {
                    temporaryCommandPoolStatus[queueFamilyIndex][i] = false;
                    return;
                }
            }
        }
        for(int queueFamilyIndex = 0; queueFamilyIndex < commandPools.size(); queueFamilyIndex++) {
            for(int i = 0; i < commandPools[queueFamilyIndex].size(); i++) {
                if(commandPools[queueFamilyIndex][i] == pool && commandPoolStatus[queueFamilyIndex][i]) {
                    commandPoolStatus[queueFamilyIndex][i] = false;
                    return;
                }
            }
        }
        cond.wait(lock);
    }
}

void CommandPoolLibrary::returnCommandPool(VkCommandPool pool) {
    {
        Lock lock(mutex);
        for(int i = 0; i < commandPools.size(); i++) {
            for(int j = 0; j < commandPools[i].size(); j++) {
                if(commandPools[i][j] == pool) {
                    commandPoolStatus[i][j] = true;
                    goto DONE;
                }
            }
        }
        for(int i = 0; i < temporaryCommandPools.size(); i++) {
            for(int j = 0; j < temporaryCommandPools[i].size(); j++) {
                if(temporaryCommandPools[i][j] == pool) {
                    temporaryCommandPoolStatus[i][j] = true;
                    goto DONE;
                }
            }
        }
    }
    DONE:
    cond.notify_all();
}


VkCommandBuffer CommandPoolLibrary::allocateCommandBuffer(VkCommandPool pool, bool singleUse) {
    VkCommandBufferAllocateInfo info = { .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO };
    info.commandPool = pool;
    info.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    info.commandBufferCount = 1;
    VkCommandBuffer command_buffer;
    auto ret = vkAllocateCommandBuffers(device, &info, &command_buffer);
    if(ret == VK_SUCCESS) {
        Lock lock(mutex);
        allocatedCommandBuffers.insert(command_buffer);
    } else {
        if(ret == VK_ERROR_OUT_OF_HOST_MEMORY)
            fprintf(stderr, "Unable to allocate command buffer: out of host memory.\n");
        else if(ret == VK_ERROR_OUT_OF_HOST_MEMORY)
            fprintf(stderr, "Unable to allocate command buffer: out of device memory.\n");
        else
            fprintf(stderr, "Unable to allocate command buffer: unknown cause.\n");
        // TODO: we really should indicate to the user that this failed. Maybe exceptions?
    }
    return command_buffer;
}

VulkanJobGroup::VulkanJobGroup(CommandPoolLibrary& lib, uint32_t type, bool single) : commandPoolLibrary(lib) {
    queue = lib.queueLibrary->getQueue(type);
    queueFamilyIndex = lib.queueLibrary->getQueueFamily(queue);
    singleUse = single;
    fenceValid = false;
    fence = 0; // here to make the compiler happy
}

VulkanJobGroup::~VulkanJobGroup() {
    commandPoolLibrary.queueLibrary->returnQueue(queue);
    if(singleUse)
        freeResources();
}

VulkanJob::VulkanJob(VulkanJobGroup& grp) : jobGroup(grp) {
    commandPool = grp.commandPoolLibrary.borrowCommandPool(grp.queueFamilyIndex, grp.singleUse);
}

VulkanJob::~VulkanJob() {
    if(jobGroup.singleUse && !commandBuffers.empty())
        submitBuffers();
    jobGroup.commandPoolLibrary.returnCommandPool(commandPool);
}

void VulkanJobGroup::freeQueue() {
    commandPoolLibrary.queueLibrary->returnQueue(queue);
}

void VulkanJobGroup::fenceCommands() {

    CommandPoolLoan cploan(commandPoolLibrary, queue, true);
    VkCommandBuffer cbuf = allocateCommandBuffer(cploan.commandPool);

    printf("VulkanJobGroup.fenceCommands: allocated a command buffer.\n");

    Lock lock(mutex);

    VkFenceCreateInfo info = { .sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO };

    vkCreateFence(commandPoolLibrary.device, &info, nullptr, &fence);

    printf("VulkanJobGroup.fenceCommands: created the fence.\n");

    VkSubmitInfo sinfo = {
            .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
    };
    vkQueueSubmit(*queue, 1, &sinfo, fence);
    fenceValid = true;
    printf("VulkanJobGroup.fenceCommands: submitted the fence.\n");
}

void VulkanJobGroup::freeResources() {
    printf("VulkanJobGroup.freeResources()\n");
    Lock lock(mutex);

    /* If we've got a fence, make sure that it's done, then de-allocate it */
    if(fenceValid) {
        printf("There's a valid fence. Waiting for it to be done.\n");
        while(vkWaitForFences(commandPoolLibrary.device, 1, &fence, VK_TRUE, 1000000000L) == VK_NOT_READY)
            ;
        vkDestroyFence(commandPoolLibrary.device, fence, nullptr);
        fenceValid = false;
    }

    printf("VulkanJobGroup.freeResources: freeing resources (%ld command buffers, %ld buffers)\n", allocatedCommandBuffers.size(), allocatedBuffers.size());

    /* Free the command buffers */
    std::unordered_set<VkCommandPool> pools;
    for(auto pool : allocatedCommandBuffers) {
        pools.insert(pool.pool);
    }
    for(auto pool : pools) {
        std::vector<VkCommandBuffer> buffers;
        for(auto buffer : allocatedCommandBuffers) {
            if(buffer.pool == pool)
                buffers.push_back(buffer.buffer);
        }
        commandPoolLibrary.borrowCommandPool(pool);
        vkFreeCommandBuffers(commandPoolLibrary.device, pool, buffers.size(), buffers.data());
        commandPoolLibrary.returnCommandPool(pool);
    }
    allocatedCommandBuffers.clear();

    /* Free the buffers */
    for(auto buffer : allocatedBuffers) {
        vkDestroyBuffer(commandPoolLibrary.device, buffer.buffer, nullptr);
        vkFreeMemory(commandPoolLibrary.device, buffer.memory, nullptr);
    }
    allocatedBuffers.clear();
}

VkCommandBuffer VulkanJobGroup::allocateCommandBuffer(VkCommandPool commandPool) {
    VkCommandBuffer buffer = commandPoolLibrary.allocateCommandBuffer(commandPool, singleUse);

    {
        Lock lock(mutex);
        allocatedCommandBuffers.push_back({ .pool = commandPool, .buffer = buffer});
    }
    VkCommandBufferBeginInfo info = {};
    info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    info.flags = singleUse ? VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT : VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT;
    vkBeginCommandBuffer(buffer, &info);

    return buffer;
}

VkCommandBuffer VulkanJob::allocateCommandBuffer() {
    VkCommandBuffer buffer = jobGroup.allocateCommandBuffer(commandPool);
    commandBuffers.push_back(buffer);
    return buffer;
}

void VulkanJob::submitBuffers() {
    VkSubmitInfo sinfo = { .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO };
    sinfo.commandBufferCount = commandBuffers.size();
    sinfo.pCommandBuffers = commandBuffers.data();

    auto ret = vkQueueSubmit(*jobGroup.queue, 1, &sinfo, VK_NULL_HANDLE);
    if(ret != VK_SUCCESS) {
        printf("Unable to submit to queue");
    }

    commandBuffers.clear();
}


void VulkanJobGroup::createBuffer(VkDeviceSize size, VkBufferUsageFlags usage, VkMemoryPropertyFlags properties, VulkanBuffer& buffer) {
    createVulkanBuffer(commandPoolLibrary.queueLibrary->physicalDevice, commandPoolLibrary.device, size, usage, properties, buffer.buffer, buffer.memory);
    Lock lock(mutex);
    allocatedBuffers.push_back(buffer);
}

void VulkanJob::copyBuffer(VkBuffer srcBuffer, VkBuffer dstBuffer, VkDeviceSize targetOffset, VkDeviceSize size) {
    VkCommandBuffer commandBuffer = allocateCommandBuffer();

    VkBufferCopy copyRegion = { };
    copyRegion.srcOffset = 0;
    copyRegion.dstOffset = targetOffset;
    copyRegion.size = size;
    vkCmdCopyBuffer(commandBuffer, srcBuffer, dstBuffer, 1, &copyRegion);

    vkEndCommandBuffer(commandBuffer);
}


void VulkanJob::stageDataAndCopyInto(void* data, uint32_t size, uint32_t offset, VulkanBuffer& destination) {
    // todo: move creating the buffer to the job group
    VulkanBuffer stagingBuffer;
    jobGroup.createBuffer(size, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, stagingBuffer);
    // allocatedBuffers.add({.buffer=stagingBuffer, .memory = stagingBufferMemory});
    void* mapped_memory;
    vkMapMemory(jobGroup.commandPoolLibrary.device, stagingBuffer.memory, 0, size, 0, &mapped_memory);
    memcpy(mapped_memory, data, (size_t) size);
    vkUnmapMemory(jobGroup.commandPoolLibrary.device, stagingBuffer.memory);

    copyBuffer(stagingBuffer.buffer, destination.buffer, offset, size);
}

VkResult VulkanJobGroup::submitBuffers(VkQueue queue, int count, const VkSubmitInfo* info, VkFence fence) {
    Lock lock(mutex);
    return vkQueueSubmit(queue, count, info, fence);
}
























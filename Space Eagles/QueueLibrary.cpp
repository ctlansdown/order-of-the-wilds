/*
 * QueueLibrary.cpp
 *
 *  Created on: Aug 7, 2018
 *      Author: ctl
 */

#include <algorithm>
#include <QueueLibrary.h>

typedef boost::unique_lock<boost::mutex> Lock;


QueueLibrary::QueueLibrary(VkPhysicalDevice device, VkSurfaceKHR surface) : physicalDevice(device) {
    uint32_t queueFamilyCount = 0;
    vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, nullptr);
    queueFamilyProperties.resize(queueFamilyCount);
    vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, queueFamilyProperties.data());

    /* Identify all of the queue families which can do presentation on the display surface*/
    queueFamilyPresentSupport.resize(queueFamilyProperties.size());
    VkBool32 presentSupport;
    for(int i = 0; i < queueFamilyProperties.size(); i++) {
        vkGetPhysicalDeviceSurfaceSupportKHR(device, i, surface, &presentSupport);
        queueFamilyPresentSupport[i] = presentSupport;
    }
}

QueueLibrary::QueueLibrary(VkPhysicalDevice device) : physicalDevice(device) {
    uint32_t queueFamilyCount = 0;
    vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, nullptr);
    queueFamilyProperties.resize(queueFamilyCount);
    vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, queueFamilyProperties.data());

    queueFamilyPresentSupport.resize(queueFamilyProperties.size());
    for(int i = 0; i < queueFamilyProperties.size(); i++)
        queueFamilyPresentSupport[i] = false;
}

std::vector<VkDeviceQueueCreateInfo> QueueLibrary::createQueueInfos(uint32_t maxQueues, uint32_t capabilities, bool needPresentSupport) {
    std::vector<VkDeviceQueueCreateInfo> infos;
    queuesCreatedByFamily.resize(queueFamilyProperties.size(), 0);

    for(int familyIndex = 0; familyIndex < queueFamilyProperties.size(); familyIndex++) {
        VkQueueFamilyProperties& properties = queueFamilyProperties[familyIndex];
        bool skip = true;
        for(int i = 0; i < 4; i++) {
            uint32_t mask = 1 << i;
            if(((capabilities & mask) == mask  && (properties.queueFlags & mask) == mask) ||
                    (needPresentSupport && queueFamilyPresentSupport[familyIndex]))
                skip = false;
        }

        uint32_t count = std::min(maxQueues, properties.queueCount);
        float queuePriority[count];
        for(int i = 0; i < count; i++)
            queuePriority[i] = 1.0f;
        VkDeviceQueueCreateInfo queueCreateInfo = { };
        queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
        queueCreateInfo.queueFamilyIndex = familyIndex;
        queueCreateInfo.queueCount = count;
        queueCreateInfo.pQueuePriorities = queuePriority;
        infos.push_back(queueCreateInfo);
        queuesCreatedByFamily[familyIndex] = count;
    }

    return infos;
}


void QueueLibrary::addCreatedQueues(VkDevice& device) {
    for(int i = 0; i < queuesCreatedByFamily.size(); i++) {
        if(queuesCreatedByFamily[i] > 0) {
            addQueues(device, i, queuesCreatedByFamily[i]);
        }
    }
    // TODO: add something into queueFamiliesAdded since we're not populating it
}


void QueueLibrary::addQueues(VkDevice& device, uint32_t familyIndex, uint32_t count) {

    uint32_t type = queueFamilyProperties[familyIndex].queueFlags;
    if(type >= 16) {
        printf("Invalid queue type specifier: %x\n", type);
        return;
    }
    {
        Lock lock(mutex);
        queues[type].resize(count);
        status[type].resize(count);
        presentSupport[type].resize(count);
        queueFamily[type].resize(count);
        queueFamiliesAdded.push_back(familyIndex);
        for(int i = 0; i < count; i++) {
            vkGetDeviceQueue(device, familyIndex, i, &queues[type][i]);
            status[type][i] = true;
            presentSupport[type][i] = queueFamilyPresentSupport[familyIndex];
            queueFamily[type][i] = familyIndex;
            queueToFamily[&queues[type][i]] = familyIndex;
        }
    }
    cond.notify_all();
}

VkQueue* QueueLibrary::getQueue(uint32_t types) {
    Lock lock(mutex);
    VkQueue* ret = nullptr;

    while(ret == nullptr) {
        for(int i = 0; i < 16; i++) {
            if((i & types) == types) {
                for(int j = 0; j < status[i].size(); j++) {
                    if(status[i][j]) {
                        ret = &queues[i][j];
                        status[i][j] = false;
                        goto DONE;
                    }
                }
            }
        }
        DONE:
        if(ret == nullptr)
            cond.wait(lock);
    }

    return ret;
}

VkQueue* QueueLibrary::getPresentQueue() {
    Lock lock(mutex);
    VkQueue* ret = nullptr;

    while(ret == nullptr) {
        for(int i = 0; i < 16; i++) {
            for(int j = 0; j < presentSupport[i].size(); j++) {
                if(presentSupport[i][j] && status[i][j]) {
                    ret = &queues[i][j];
                    status[i][j] = false;
                    goto DONE;
                }
            }
        }
        DONE:
        if(ret == nullptr)
            cond.wait(lock);
    }

    return ret;
}

VkQueue* QueueLibrary::getQueueFromFamily(uint32_t familyIndex) {
    Lock lock(mutex);
    VkQueue* ret = nullptr;

    while(ret == nullptr) {
        for(int i = 0; i < 16; i++) {
            for(int j = 0; j < queueFamily[i].size(); j++) {
                if(queueFamily[i][j] == familyIndex && status[i][j]) {
                    ret = &queues[i][j];
                    status[i][j] = false;
                    goto DONE;
                }
            }
        }
        DONE:
        if(ret == nullptr)
            cond.wait(lock);
    }


    return ret;
}

void QueueLibrary::returnQueue(VkQueue* queue) {
    {
        Lock lock(mutex);
        for(int i = 0; i < 16; i++) {
            for(int j = 0; j < queues[i].size(); j++) {
                if(&queues[i][j] == queue) {
                    status[i][j] = true;
                    goto DONE;
                }
            }
        }
    }
    DONE:
    cond.notify_all();
}

uint32_t QueueLibrary::findQueueFamily(VkQueueFlags flags) {
    for(int i = 0; i < queueFamilyProperties.size(); i++)
        if((queueFamilyProperties[i].queueFlags & flags) == flags)
            return i;
    fprintf(stderr, "Unable to find a queue with the properties %x\n", flags);
    return 0;
}

uint32_t QueueLibrary::getQueueFamily(VkQueue* queue) {
    return queueToFamily[queue];
}

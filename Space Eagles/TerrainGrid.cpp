/*
 * TerrainGrid.cpp
 *
 *  Created on: Jul 18, 2018
 *      Author: ctl
 */

#include "TerrainGrid.h"

TerrainGrid terrainGrid;

int TerrainGrid::pointToGridIndex(point p) {
    for (int i = 0; i < terrainGrid.count; i++) {
        if (p == terrainGrid.tiles[i].lowerLeft) {
            return i;
        }
    }
    printf("ERROR: couldn't find tile for (%ld,%ld)\n", p.x, p.y);
    return -1;
}

int TerrainGrid::pointToGridIndex(int64_t x, int64_t y) {
    point p(x, y);
    for (int i = 0; i < terrainGrid.count; i++) {
        if (p == terrainGrid.tiles[i].lowerLeft) {
            return i;
        }
    }
    printf("ERROR: couldn't find tile for (%ld,%ld)\n", p.x, p.y);
    return -1;
}

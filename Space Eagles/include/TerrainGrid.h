/*
 * TerrainGrid.h
 *
 *  Created on: Jul 18, 2018
 *      Author: ctl
 */

#ifndef TERRAINGRID_H_
#define TERRAINGRID_H_

#include "Terrain.h"

/**
 * A structure for keeping track of what is loaded into the terrain buffer
 * we keep track of whether the grid element is allocated by whether its length is 0
 */
struct TerrainGridElement {
    point lowerLeft;
    /** For vertex buffers only, the offset to apply to the indices */
    uint32_t vertexOffset = 0;
    /** The offset within the buffer (i.e. the place to copy data to in order to overwrite the grid element */
    uint32_t bufferOffset = 0;
    /** The length of the data within the buffer */
    uint32_t length = 0;
};

/**
 * A structure for keeping track of what is loaded into the terrain buffer
 */
struct TerrainGrid {
    /** The distance away from the current tile we have in the grid */
    int32_t distance;
    /** The total number of tiles we have in the grid */
    uint32_t count;
    /** the total number fo seams we have in the grid */
    uint32_t seamCount;
    /** The total number of indices to render */
    uint32_t indicesCount;
    /** the grid elements for the tiles */
    vector<TerrainGridElement> tiles;
    /** the grid elements for the tile indices */
    vector<TerrainGridElement> indices;
    /** The grid element for the seam indices */
    vector<TerrainGridElement> seams;

    int pointToGridIndex(point p);
    int pointToGridIndex(int64_t x, int64_t y);
};

extern TerrainGrid terrainGrid;
#endif /* TERRAINGRID_H_ */

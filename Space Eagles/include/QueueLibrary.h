/*
 * QueueLibrary.h
 *
 *  Created on: Aug 7, 2018
 *      Author: ctl
 */

#ifndef INCLUDE_QUEUELIBRARY_H_
#define INCLUDE_QUEUELIBRARY_H_

#include <vulkan/vulkan.h>
#include <stdint.h>

#include <boost/thread/mutex.hpp>
#include <boost/thread/condition_variable.hpp>
#include <vector>
#include <unordered_map>

/**
 * A thread-safe library for checking out and waiting for queues.
 * Contains the queue objects. Once you've created the queues that you want on the device, you should add them, family-by-family, with the addQueues method.
 *
 * Alternatively, you can use the createQueueInfos and addCreatedQueues methods
 *
 */
class QueueLibrary {

public:
    /**
     * Called with the physical device and display surface so that we can get the information we need about the queue families
     */
    QueueLibrary(VkPhysicalDevice device, VkSurfaceKHR surface);

    /**
     * Called with the physical device so that we can get the information we need about the queue families.
     * This constructor is for programs which do not use a display surface (e.g. pure compute)
     */
    QueueLibrary(VkPhysicalDevice device);


    /**
     * Adds the specified number of queues from the specified queue family.
     * NOTE: the queues must already have been created.
     */
    void addQueues(VkDevice& device, uint32_t familyIndex, uint32_t count);

    /**
     * Gets the VkDeviceQueueCreateInfo structs to pass to logical device creation
     * @param maxQueues the maximum number of queues per family
     * @param capabilities the bitwise-or of the capability bytes (VK_QUEUE_*_BIT)
     * @param needPresentSupport whether or not present support is required for at least one queue
     */
    std::vector<VkDeviceQueueCreateInfo> createQueueInfos(uint32_t maxQueues, uint32_t capabilities, bool needPresentSupport);


    /**
     * Adds the queues created with the info returned by {@createQueueInfos}. Once the VkDevice has been created, this should be called.
     */
    void addCreatedQueues(VkDevice& device);


    /**
     * Gets a queue which supports the specified types.
     *
     * You probably want to use one of the QueueLoan RAII classes rather than directly using this, since then you're responsible for
     * calling {@link returnQueue} yourself.
     *
     * @param types types as given by the VK_QUEUE_*_BIT enums from {@link VkQueueFlagBits} (you can specify more than one)
     */
    VkQueue* getQueue(uint32_t types);


    /**
     * Gets a queue from the specified queue family.
     *
     * This is meant for CommandPoolLibrary's use and would be of limited and inconvenient general use.
     */
    VkQueue* getQueueFromFamily(uint32_t familyIndex);

    /**
     * Gets a queue with present support (i.e. one that can be used with {@link vkQueuePresentKHR})
     * You probably want to use the {@link PresentQueueLoan} RAII classes rather than directly using this, since then you're responsible
     * for calling {@link returnQueue} yourself.
     */
    VkQueue* getPresentQueue();

    /**
     * Returns a queue to the library (i.e. indicates that the caller is done with it).
     * You probably want to use one of the QueueLoan RAII classes rather than this directly.
     */
    void returnQueue(VkQueue* queue);

    /**
     * Finds a queue family which supports the capabilities specified in flags
     * @return the queue family index of the queue family found
     */
    uint32_t findQueueFamily(VkQueueFlags flags);

    /**
     * Returns an array of the queue families we have. (This is primarily useful for creating command pools.)
     */
    std::vector<uint32_t> getQueueFamilies() {
        return queueFamiliesAdded;
    }

    /**
     * Gets the queue family index that the specified queue is from
     */
    uint32_t getQueueFamily(VkQueue*);

    /**
     * The physical device on which the queues are created
     */
    VkPhysicalDevice physicalDevice;


private:

    boost::mutex mutex;

    boost::condition_variable cond;

    /**
     * An array of queues. Each index corresponds to the VkQueueFlagBits value, i.e. indicates which capabilities are supported by the queues in the array
     */
    std::vector<VkQueue> queues[16];

    /**
     * An array of statuses. Each index in a status vector corresponds to the queue in {@link queues}. True means that it's available, false that it's checked out.
     * (the indexing is identical to queues)
     */
    std::vector<bool> status[16];

    /**
     * For each queue, whether the queue has present support (i.e. can be used as the presentation queue for {@link vkQueuePresentKHR}
     */
    std::vector<bool> presentSupport[16];

    /**
     * A map of the queue to the queue family index it is from
     */
    std::vector<uint32_t> queueFamily[16];

    /**
     * A map of the queues to the queue family they're from
     */
    std::unordered_map<VkQueue*, uint32_t> queueToFamily;

    /**
     * Present support for each queue family, where the indexes match
     */
    std::vector<bool> queueFamilyPresentSupport;

    /**
     * A list of the queue families which have been added. This is used for command pool creation.
     */
    std::vector<uint32_t> queueFamiliesAdded;


    std::vector<VkQueueFamilyProperties> queueFamilyProperties;

    /**
     * If {@link createQueueInfos} is called, this gets filled so that {@link addCreatedQueues} knows what to create. Otherwise it is empty
     */
    std::vector<uint32_t> queuesCreatedByFamily;
};






struct QueuePresentFlagClass {
};

/**
 * A convenience class to use RAII to ensure we return the queue
 */
struct QueueLoan {
public:
    QueueLoan(QueueLibrary* lib, uint32_t type) : library(*lib) {
        queue = library.getQueue(type);
    }

    QueueLoan(QueueLibrary* lib, QueuePresentFlagClass present) : library(*lib) {
        queue = library.getPresentQueue();
    }

    ~QueueLoan() {
        library.returnQueue(queue);
    }

    VkQueue* queue;
protected:
    QueueLibrary& library;
};

/**
 * A convenience class to use RAII to ensure we return the queue. Gets a queue that supports graphics operations
 */
struct GraphicsQueueLoan : QueueLoan {
    GraphicsQueueLoan(QueueLibrary* lib) : QueueLoan(lib, VK_QUEUE_GRAPHICS_BIT) {
    }
};

/**
 * A convenience class to use RAII to ensure we return the queue. Gets a queue that supports graphics operations
 */
struct TransferQueueLoan : QueueLoan {
    TransferQueueLoan(QueueLibrary* lib) : QueueLoan(lib, VK_QUEUE_TRANSFER_BIT) {
    }
};

/**
 * A convenience class to use RAII to ensure we return the queue. Gets a queue that supports graphics operations
 */
struct ComputeQueueLoan : QueueLoan {
    ComputeQueueLoan(QueueLibrary* lib) : QueueLoan(lib, VK_QUEUE_COMPUTE_BIT) {
    }
};

/**
 * A convenience class to use RAII to ensure we return the queue. Gets a queue that supports graphics operations
 */
struct SparseQueueLoan : QueueLoan {
    SparseQueueLoan(QueueLibrary* lib) : QueueLoan(lib, VK_QUEUE_SPARSE_BINDING_BIT) {
    }
};

struct PresentQueueLoan : QueueLoan {
    PresentQueueLoan(QueueLibrary* lib) : QueueLoan(lib, QueuePresentFlagClass()) {
    }
};


#endif /* INCLUDE_QUEUELIBRARY_H_ */

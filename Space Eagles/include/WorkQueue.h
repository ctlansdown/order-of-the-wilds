/*
 * WorkQueue.h
 *
 *  Created on: Jul 20, 2018
 *      Author: ctl
 */

#ifndef INCLUDE_WORKQUEUE_H_
#define INCLUDE_WORKQUEUE_H_

#include <boost/thread/mutex.hpp>
#include <boost/thread/condition_variable.hpp>
#include <atomic>
#include <vector>
#include <deque>
#include <functional>

class Job {

public:
    Job(std::vector<std::function<void()>>& tasks);

    /**
     * Waits for completion of all tasks in the job
     */
    void waitForCompletion();

    int getTaskCount();

    /**
     * Iterates over the functions we have and
     */
    struct iterable {
        int count;
        Job& parent;

        iterable(Job& j) : parent(j) {
            count = 0;
        }

        iterable begin() {
            return *this;
        }

        struct sentinel {
            // just a sentinel
        };

        sentinel end() {
            return sentinel();
        }

        bool equals(const sentinel& o) {
            return count < parent.functions.size();
        }


        iterable& operator++() {
            ++count;
            return *this;
        }

        std::function<void()> operator*() {
            int index = count;
            Job& job = parent;
            return [index,&job] {
                job.functions[index]();
                if(--job.completionCount == 0) {
                    boost::unique_lock<boost::mutex> lock(job.mut);
                    job.cond.notify_all();
                }
            };
        }

        friend bool operator!=(const Job::iterable& lh, const Job::iterable::sentinel& rh);

    };

    Job& operator=(const Job& other) {
        if(this != &other) {
            functions = other.functions;
            completionCount = (unsigned int)other.completionCount;
        }
        return *this;
    }

    /**
     * Gets an iterator for the functions to stick them into the work queue
     */
    iterable getFunctions() {
        iterable i(*this);
        return i;
    }

    friend bool operator!=(const Job::iterable& lh, const Job::iterable::sentinel& rh);


private:
    std::vector<std::function<void()>>& functions;
    boost::mutex mut;
    boost::condition_variable cond;
    /**
     * The number of tasks remaining to complete. It starts at the total number of tasks and works down to zero
     */
    std::atomic_uint completionCount;
};

inline bool operator!=(const Job::iterable& lh, const Job::iterable::sentinel& rh) {
    return lh.count < lh.parent.functions.size();
}


/**
 *  The Work Queue
 */
class WorkQueue {
public:
    /**
     * Initializes the work queue with 1 less than the total number of threads supported by the CPU
     */
    WorkQueue();
    /**
     * Initializes the work queue with the specified thread count
     */
    WorkQueue(int thread_count);

    void submit(Job& job);

    void submit(std::function<void()> f);

    template<typename T>
    void submit(T tasks) {
        {
            boost::unique_lock<boost::mutex> lock(mutex);
            for(std::function<void()> f : tasks) {
                queue.push_back(f);
            }
        }
        cond.notify_all();
    }

    int getThreadCount();

protected:

    int threadCount;
    boost::mutex mutex;
    boost::condition_variable cond;
    std::deque<std::function<void()>> queue;

    struct QueueThread {
        WorkQueue& workQueue;

        QueueThread(WorkQueue& wq) : workQueue(wq) {}

        void operator()();
    };


};

#endif /* INCLUDE_WORKQUEUE_H_ */

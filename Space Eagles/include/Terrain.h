#ifndef TERRAIN_H
#define TERRAIN_H
/**
* The class which gives an implementation of terrain generation
*/

#include <cstdint>
#include <vector>
#include <unordered_map>
#include <stdlib.h>
#include <random>

#include <boost/thread.hpp>

#include "Vertex.h"

#define TYPE_PLAINS 0
#define TYPE_ROCKY 1
#define TYPE_MOUNTAINOUS 2

const float TYPE_HEIGHTS[] = {0, 100, 1000};
const float TYPE_VARIANCE[] = {0.2, 1.0, 0.5};

using namespace std;

struct point {
    int64_t x, y;
    point(){
        x = 0;
        y = 0;
    }
    point(int64_t ix, int64_t iy) {
        x = ix;
        y = iy;
    }
};
inline bool operator==(const point& lhs, const point& rhs) {
    return lhs.x == rhs.x && lhs.y == rhs.y;
}
namespace std {
    template<> struct hash<point> {
        size_t operator()(point const& p) const noexcept {
            size_t const h1 (std::hash<int64_t>{}(p.x));
            size_t const h2 (std::hash<int64_t>{}(p.y));
            return h1 ^ (h2 << 1);
        }
    };
}

struct terrain_description{
    /** A constant which determines the type (mountainous, rocky, flat, etc) */
    int type;
    /** The average height in meters */
    float averageHeight;
    /** the dominance relative to neighbors; in the range (0.0,1.0). Controls behavior near the borders */
    float dominance;
    /** The variance of the terrain (a scale from 0.0 to 1.0 to tamp down the randomness, 0.0 being complete uniform) */
    float variance;
};

/**
 * This is intended to be a singleton class which contains all of the terrain descriptions
 */
class Terrain
{
    public:
        Terrain(uint64_t seed) {
            globalSeed = seed;
        }

        vector<Vertex>* getVertexData(int64_t x, int64_t y);
        vector<uint32_t>* getIndexData(int64_t x, int64_t y);
        /**
         * Generates the seam data for the specified tile.
         * NOTE: the indices are for the current vertex buffer (with multiple tiles init) since the indices have to into that buffer
         */
        void generateSeamData(int64_t x1, int64_t y1, uint32_t offset1, int64_t x2, int64_t y2, uint32_t offset2, vector<uint32_t>& seam);

        /**
        * Generates the terrain for the square kilometer grid covering the specified point
        */
        void generateTerrain(double x, double y) {
            generateTerrain(x, y, calculateSeed(x, y));
        }

        float getHeight(double x_pos, double y_pos) {
            int64_t x = (int64_t)x_pos;
            int64_t y = (int64_t)y_pos;
            int64_t base_x = x >= 0 ? 1000*(x/1000) : 1000*((x-999)/1000);
            int64_t base_y = y >= 0 ? 1000*(y/1000) : 1000*((y - 999)/1000);
            point pos(base_x, base_y);
            int ix = x - base_x;
            int iy = y - base_y;
            auto& map = *getHeightmapVector(pos);
            return map[iy * 1000 + ix];
        }

        /**
         * Returns the number of index buffer elements required for the triangles for one tile
         */
        int getTileIndexBufferElementCount() {
            return 999*999*2*3;
        }

        /**
         * returns the number of index buffer elements required for the triangles for one seam
         */
        int getTileIndexBufferSeamElementCount() {
            return 999*2*3;
        }

        /**
         * Converts a location (in coordinates) to the point which identifies the terrain tile the point is on
         */
        static point locationToTilePoint(double x, double y) {
            return point((int64_t)floor(x/1000)*1000, (int64_t)floor(y/1000)*1000);
        }

    protected:

        /**
        * Generates the descriptions
        */
        void generateDescriptions(double x, double y, uint64_t seed);

        uint64_t calculateSeed(double x, double y) {
            return (int64_t)(globalSeed + x + 113*y);
        }

        vector<unordered_map<point, terrain_description>>& getDescriptions(point p);

        vector<float>* getHeightmapVector(point p);
        vector<Vertex>* getVertexVector(point p);
        vector<uint32_t>* getIndicesVector(point p);

        bool haveHeightmapData(point p);
        bool haveVertexData(point p);
        bool haveIndexData(point p);

        boost::mutex description_mutex;
        boost::mutex heightmap_mutex;
        boost::mutex vertex_mutex;
        boost::mutex index_mutex;

    private:
        uint64_t globalSeed;

        /**
        * The terrain descriptions. The point is the lower-left point of the grid. the index is the level.
        * Level 0 is a 10x10 meter grid. Each layer up is 10x zoomed out. So level 1 is 100x100. Level 2 is 1000x1000. etc.
        */
        std::unordered_map<point, vector<unordered_map<point, terrain_description>>*> descriptions;

        // TODO: refactor heightmap, vertexmap, and vertexIndicesMap to use pointers and allocate their data using new, so they can be worked on independently

        /**
        * This is a map of the actual terrain heights. The key is a point in the lower-left hand corner of the heightmap.
        * The value is a 1000x1000 array representing a square kilometer.
        */
        unordered_map<point, vector<float>*> heightmap;

        /**
         * This is a map of the vertices generated from the herrain heights. The key is a point in the lower-left hand corner of the vertex map.
         * The value is an array represending a square kilometer
         */
        unordered_map<point, vector<Vertex>*> vertexmap;

        /**
         * The indices which form the triangles that use the vertices
         */
        unordered_map<point, vector<uint32_t>*> vertexIndicesMap;


        void generateTerrain(double x, double y, uint64_t seed);
        void generate_zoom_level(double x, double y, int level, std::uniform_real_distribution<double>& uniform, std::mt19937& mt,
                vector<unordered_map<point, terrain_description>>& my_descriptions, terrain_description& my_description);
        void generateVertices(int64_t x, int64_t y, vector<Vertex>& vertices, vector<uint32_t>& indices);
};

/**
 * The variable which holds the chunks. The point is at the bottom-left, and currently the chunks are 1000m by 1000m, so the points must align on these
 */
extern unordered_map<point, Terrain*> terrain_chunks;

Terrain* get_terrain(int64_t x, int64_t y);

#endif // TERRAIN_H

/*
 * CommandPoolLibrary.h
 *
 *  Created on: Aug 10, 2018
 *      Author: ctl
 */

#ifndef COMMANDPOOLLIBRARY_H_
#define COMMANDPOOLLIBRARY_H_

#include <vulkan/vulkan.h>

#include <boost/thread/mutex.hpp>
#include <boost/thread/condition_variable.hpp>

#include <stdint.h>
#include <vector>
#include <unordered_set>

#include <QueueLibrary.h>
#include <VulkanUtils.h>

struct CommandPoolLoan;

struct AllocatedCommandBuffer {
    VkCommandPool pool;
    VkCommandBuffer buffer;
};

/**
 * A library for managing commands and command pools.
 * NOTE: this class is designed to work with {@link QueueLibrary} and cannot be used without it.
 */
class CommandPoolLibrary {
    friend class CommandPoolLoan;
    friend class VulkanJobGroup;
    friend class VulkanJob;
public:

    CommandPoolLibrary(QueueLibrary *q, VkDevice dev);

    /**
     * Creates the command pools for the queue families in the QueueLibrary.
     *
     * @param count the number of command pools to create for each queue family. This controls the maximum number of
     * commands which can be recorded simultaneously. You'll probably want to set this to std::thread::hardware_concurrency()
     * @param createShortLivedBuffers create command pools for short lived command buffers as well as regular command pools
     */
    void createCommandPools(uint32_t count, bool createShortLivedBuffers);

    /**
     * checks out a command pool on the specified queue family
     */
    VkCommandPool borrowCommandPool(uint32_t queueFamilyIndex, bool temporary);

    /**
     * Borrows the specified command pool so that it can be used.
     */
    void borrowCommandPool(VkCommandPool pool);

    /**
     * Returns a command pool
     */
    void returnCommandPool(VkCommandPool pool);

    /**
     * Allocates a command pool on the specified buffer. Keeps track of the command buffer so that it can be reaped using general methods.
     * NOTE: this method assumes that the command pool is already locked, since it will need to be by whatever code is recording commands in the allocated buffer
     */
    VkCommandBuffer allocateCommandBuffer(VkCommandPool pool, bool singleUse);


    VkDevice device;

    QueueLibrary* queueLibrary;


private:


    boost::mutex mutex;

    boost::condition_variable cond;

    std::vector<uint32_t> queueFamilyIndices;

    std::vector<std::vector<VkCommandPool>> commandPools;

    std::vector<std::vector<bool>> commandPoolStatus;

    std::vector<std::vector<VkCommandPool>> temporaryCommandPools;

    std::vector<std::vector<bool>> temporaryCommandPoolStatus;

    std::unordered_set<VkCommandBuffer> allocatedCommandBuffers;
};




/**
 * A class used to allocate a command buffer on a command pool
 */
struct CommandPoolLoan {
    CommandPoolLoan(CommandPoolLibrary& lib, VkQueueFlags queueProperties, bool temporary) : commandLibrary(lib) {
        queueFamilyIndex = commandLibrary.queueLibrary->findQueueFamily(queueProperties);
        commandPool = commandLibrary.borrowCommandPool(queueFamilyIndex, temporary);
    }

    CommandPoolLoan(CommandPoolLibrary& lib, VkQueue* q, bool temporary) : commandLibrary(lib) {
        queueFamilyIndex = commandLibrary.queueLibrary->getQueueFamily(q);
        commandPool = commandLibrary.borrowCommandPool(queueFamilyIndex, temporary);
    }

    ~CommandPoolLoan() {
        commandLibrary.returnCommandPool(commandPool);
    }

    VkCommandPool commandPool;
private:
    CommandPoolLibrary& commandLibrary;
    uint32_t queueFamilyIndex;
};



/**
 * A representation of a collection of jobs which all perform related tasks.
 *
 * The idea is that these jobs will use one queue and a collection of command pools on that queue in order to do the work in parallel. Once the jobs are submitted,
 * a fence will be put on the queue so that it can be used to tell when the jobs have completed and the associated resources can be cleaned up.
 *
 *
 */
struct VulkanJobGroup {
    friend class VulkanJob;

    /*
TODO:
 * create a fence
 * be able to wait on it
 * be able to test to see if it's done (for opportunistic cleanup)
 * have some way of cleaning up resources
 * possibly a list of buffers to destroy
 * definitely a list of command buffers to free
 * possibly a list of cleanup functions to execute as a catch-all for resources we don't forsee
 *

    */

    /**
     * Grabs a queue to execute the jobs on
     * @param lib the command pool library to use for command buffer allocations
     * @param type the capabilities required for the queue we're going to check out (bitwise OR of VK_QUEUE_*_BIT)
     * @param single whether the command buffers that will be recorded should be single-use buffers (true) or multi-use (false)
     */
    VulkanJobGroup(CommandPoolLibrary& lib, uint32_t type, bool single);

    ~VulkanJobGroup();

    /**
     * Allocates command buffers. Also initializes them.
     * NOTE: if the job group is not single-use, the VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT will be set on the command buffer.
     */
    VkCommandBuffer allocateCommandBuffer(VkCommandPool cp);

    void createBuffer(VkDeviceSize size, VkBufferUsageFlags usage, VkMemoryPropertyFlags properties, VulkanBuffer& buffer);

    /**
     * Creates and submits a fence on the queue
     */
    void fenceCommands();

    /**
     * Frees buffers allocates by the VulkanJobs that are part of this job group.
     * NOTE: if we've allocated a fence, this will wait for it to complete.
     */
    void freeResources();

    /**
     * Returns the queue to the queue library. Used for jobs which set up long-term command buffers so that the queue can be used by other code.
     * Note: the queue will have to be checked out before the commands can be re-submitted.
     */
    void freeQueue();

protected:
    VkResult submitBuffers(VkQueue queue, int count, const VkSubmitInfo* info, VkFence);

private:
    boost::mutex mutex;
    CommandPoolLibrary& commandPoolLibrary;
    VkQueue* queue;
    uint32_t queueFamilyIndex;
    bool singleUse;
    std::vector<AllocatedCommandBuffer> allocatedCommandBuffers;
    std::vector<VulkanBuffer> allocatedBuffers;
    bool fenceValid = false;
    VkFence fence;
};


/**
 *
 *
 * NOTE: if the VulkanJobGroup is single-use, a VulkanJob will automatically submit its command buffers in its destructor
 */
struct VulkanJob {
    /**
     * Borrows a command pool
     */
    VulkanJob(VulkanJobGroup& grp);

    ~VulkanJob();

    /**
     * Allocates command buffers. Also initializes them.
     * NOTE: if the job group is not single-use, the VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT will be set on the command buffer.
     *
     * NOTE: the command buffer allocation will be recorded in the job group, and freed when the job group's resources are
     */
    VkCommandBuffer allocateCommandBuffer();

    /**
     * Creates the command buffer filled out to copy the buffer.
     *
     * NOTE: you must call submitBuffers() for it to be submitted.
     */
    void copyBuffer(VkBuffer srcBuffer, VkBuffer dstBuffer, VkDeviceSize targetOffset, VkDeviceSize size);

    /**
     * Stages the data and creates the command buffer filled out to copy to the buffer.
     *
     * note: you must call submitBuffers() for the command buffer to be submitted
     */
    void stageDataAndCopyInto(void* data, uint32_t size, uint32_t offset, VulkanBuffer& destination);

    /**
     * Submits the command buffers we've recorded to the queue
     */
    void submitBuffers();

    VkCommandPool commandPool;
private:
    VulkanJobGroup& jobGroup;
    std::vector<VkCommandBuffer> commandBuffers;
};







#endif /* COMMANDPOOLLIBRARY_H_ */

/*
 * VulkanBuffer.h
 *
 *  Created on: Jun 23, 2018
 *      Author: ctl
 */

#ifndef VULKANBUFFER_H_
#define VULKANBUFFER_H_

#include <vulkan/vulkan.h>

struct VulkanBuffer {
    VkBuffer buffer;
    VkDeviceMemory memory;
};

#endif /* VULKANBUFFER_H_ */

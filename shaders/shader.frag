#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec3 fragColor;
layout(location = 1) in vec3 vertexNormal;
layout(location = 2) in vec3 modelPosition;

layout(location = 0) out vec4 outColor;

void main() {
     vec3 lightDir = normalize(vec3(0,0,10000)-modelPosition);
     float cosAngIncidence = dot(normalize(vertexNormal), lightDir);
     outColor = vec4(fragColor*cosAngIncidence + 0.2*fragColor, 1.0);
//      outColor = vec4(fragColor, 1.0);
}

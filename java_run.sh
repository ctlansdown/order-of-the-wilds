#! /bin/sh
#LWJGL=~/lwjgl
LWJGL=~/lwjgl-nightly
CLASSPATH="bin/"
for file in `ls -1 $LWJGL/*.jar lib/*.jar`:
do
	CLASSPATH="$CLASSPATH:$file"
done
# LD_LIBRARY_PATH="/home/ctl/VulkanSDK/1.0.65.0/source/lib:$LD_LIBRARY_PATH" \
LWJGL_DEBUG="-Dorg.lwjgl.util.Debug=true"
# VALIDATION="-Dvulkan.validation=true"
java $LWJGL_DEBUG $VALIDATION  -cp $CLASSPATH HelloWorld
